﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Dulwa.v1.Migrations
{
    public partial class dulwainfoupdated : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "DulwaInformationViewModel");

            migrationBuilder.AddColumn<string>(
                name: "ShortDescription",
                table: "Dulwainfo",
                nullable: false,
                defaultValue: "");

            migrationBuilder.CreateTable(
                name: "DulwaInformation",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    Created_Date = table.Column<DateTime>(nullable: false),
                    TollFreeNo = table.Column<string>(nullable: true),
                    Updated_Date = table.Column<DateTime>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_DulwaInformation", x => x.Id);
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "DulwaInformation");

            migrationBuilder.DropColumn(
                name: "ShortDescription",
                table: "Dulwainfo");

            migrationBuilder.CreateTable(
                name: "DulwaInformationViewModel",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    TollFreeNo = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_DulwaInformationViewModel", x => x.Id);
                });
        }
    }
}
