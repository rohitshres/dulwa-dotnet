﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Dulwa.v1.Migrations
{
    public partial class test : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_DulwaStory_Dulwainfo_DulwainfoId",
                table: "DulwaStory");

            migrationBuilder.AlterColumn<string>(
                name: "Link",
                table: "DulwaStory",
                nullable: true,
                oldClrType: typeof(string));

            migrationBuilder.AlterColumn<string>(
                name: "ImageUrl",
                table: "DulwaStory",
                nullable: true,
                oldClrType: typeof(string));

            migrationBuilder.AlterColumn<Guid>(
                name: "DulwainfoId",
                table: "DulwaStory",
                nullable: true,
                oldClrType: typeof(Guid));

            migrationBuilder.AlterColumn<string>(
                name: "Description",
                table: "DulwaStory",
                nullable: true,
                oldClrType: typeof(string));

            migrationBuilder.AddForeignKey(
                name: "FK_DulwaStory_Dulwainfo_DulwainfoId",
                table: "DulwaStory",
                column: "DulwainfoId",
                principalTable: "Dulwainfo",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_DulwaStory_Dulwainfo_DulwainfoId",
                table: "DulwaStory");

            migrationBuilder.AlterColumn<string>(
                name: "Link",
                table: "DulwaStory",
                nullable: false,
                oldClrType: typeof(string),
                oldNullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "ImageUrl",
                table: "DulwaStory",
                nullable: false,
                oldClrType: typeof(string),
                oldNullable: true);

            migrationBuilder.AlterColumn<Guid>(
                name: "DulwainfoId",
                table: "DulwaStory",
                nullable: false,
                oldClrType: typeof(Guid),
                oldNullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "Description",
                table: "DulwaStory",
                nullable: false,
                oldClrType: typeof(string),
                oldNullable: true);

            migrationBuilder.AddForeignKey(
                name: "FK_DulwaStory_Dulwainfo_DulwainfoId",
                table: "DulwaStory",
                column: "DulwainfoId",
                principalTable: "Dulwainfo",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
