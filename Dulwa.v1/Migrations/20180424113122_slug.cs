﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Dulwa.v1.Migrations
{
    public partial class slug : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "slug",
                table: "WishList",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "slug",
                table: "UserPurchaseItem",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "slug",
                table: "Tradition_Culture",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "slug",
                table: "StoryCategory",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "slug",
                table: "StoreDetail",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "slug",
                table: "StoreCategory",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "slug",
                table: "Store",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "slug",
                table: "ShoppingCartItem",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "slug",
                table: "ShoppingCart",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "slug",
                table: "Route",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "slug",
                table: "Review",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "slug",
                table: "PackageDate",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "slug",
                table: "Package",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "slug",
                table: "Keys",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "slug",
                table: "KeyCategory",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "slug",
                table: "GalleryVideo",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "slug",
                table: "GalleryImage",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "slug",
                table: "Gallery",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "slug",
                table: "Event",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "slug",
                table: "DulwaStory",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "slug",
                table: "DulwaInformation",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "slug",
                table: "Dulwainfo",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "slug",
                table: "DestinationStops",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "slug",
                table: "CustomPackage",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "slug",
                table: "CurriculumRedirect",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "slug",
                table: "Curriculum",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "slug",
                table: "Cuisine",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "slug",
                table: "ContestUserLike",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "slug",
                table: "ContestUser",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "slug",
                table: "ContestImage",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "slug",
                table: "ContestDetail",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "slug",
                table: "ContestComment",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "slug",
                table: "Contest",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "slug",
                table: "Company",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "slug",
                table: "Advertisement",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "slug",
                table: "WishList");

            migrationBuilder.DropColumn(
                name: "slug",
                table: "UserPurchaseItem");

            migrationBuilder.DropColumn(
                name: "slug",
                table: "Tradition_Culture");

            migrationBuilder.DropColumn(
                name: "slug",
                table: "StoryCategory");

            migrationBuilder.DropColumn(
                name: "slug",
                table: "StoreDetail");

            migrationBuilder.DropColumn(
                name: "slug",
                table: "StoreCategory");

            migrationBuilder.DropColumn(
                name: "slug",
                table: "Store");

            migrationBuilder.DropColumn(
                name: "slug",
                table: "ShoppingCartItem");

            migrationBuilder.DropColumn(
                name: "slug",
                table: "ShoppingCart");

            migrationBuilder.DropColumn(
                name: "slug",
                table: "Route");

            migrationBuilder.DropColumn(
                name: "slug",
                table: "Review");

            migrationBuilder.DropColumn(
                name: "slug",
                table: "PackageDate");

            migrationBuilder.DropColumn(
                name: "slug",
                table: "Package");

            migrationBuilder.DropColumn(
                name: "slug",
                table: "Keys");

            migrationBuilder.DropColumn(
                name: "slug",
                table: "KeyCategory");

            migrationBuilder.DropColumn(
                name: "slug",
                table: "GalleryVideo");

            migrationBuilder.DropColumn(
                name: "slug",
                table: "GalleryImage");

            migrationBuilder.DropColumn(
                name: "slug",
                table: "Gallery");

            migrationBuilder.DropColumn(
                name: "slug",
                table: "Event");

            migrationBuilder.DropColumn(
                name: "slug",
                table: "DulwaStory");

            migrationBuilder.DropColumn(
                name: "slug",
                table: "DulwaInformation");

            migrationBuilder.DropColumn(
                name: "slug",
                table: "Dulwainfo");

            migrationBuilder.DropColumn(
                name: "slug",
                table: "DestinationStops");

            migrationBuilder.DropColumn(
                name: "slug",
                table: "CustomPackage");

            migrationBuilder.DropColumn(
                name: "slug",
                table: "CurriculumRedirect");

            migrationBuilder.DropColumn(
                name: "slug",
                table: "Curriculum");

            migrationBuilder.DropColumn(
                name: "slug",
                table: "Cuisine");

            migrationBuilder.DropColumn(
                name: "slug",
                table: "ContestUserLike");

            migrationBuilder.DropColumn(
                name: "slug",
                table: "ContestUser");

            migrationBuilder.DropColumn(
                name: "slug",
                table: "ContestImage");

            migrationBuilder.DropColumn(
                name: "slug",
                table: "ContestDetail");

            migrationBuilder.DropColumn(
                name: "slug",
                table: "ContestComment");

            migrationBuilder.DropColumn(
                name: "slug",
                table: "Contest");

            migrationBuilder.DropColumn(
                name: "slug",
                table: "Company");

            migrationBuilder.DropColumn(
                name: "slug",
                table: "Advertisement");
        }
    }
}
