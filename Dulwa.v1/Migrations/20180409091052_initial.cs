﻿using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Dulwa.v1.Migrations
{
    public partial class initial : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Advertisement",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    Created_Date = table.Column<DateTime>(nullable: false),
                    Updated_Date = table.Column<DateTime>(nullable: false),
                    imageUrl = table.Column<string>(nullable: false),
                    name = table.Column<string>(nullable: false),
                    redirectUrl = table.Column<string>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Advertisement", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Company",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    ContactPersonDesignation = table.Column<string>(nullable: false),
                    ContactPersonName = table.Column<string>(nullable: false),
                    Created_Date = table.Column<DateTime>(nullable: false),
                    Updated_Date = table.Column<DateTime>(nullable: false),
                    address = table.Column<string>(nullable: false),
                    contact = table.Column<string>(nullable: false),
                    fb_Link = table.Column<string>(nullable: true),
                    google_Link = table.Column<string>(nullable: true),
                    image = table.Column<string>(nullable: false),
                    instragram_Link = table.Column<string>(nullable: true),
                    name = table.Column<string>(nullable: false),
                    pan_vat_number = table.Column<string>(nullable: true),
                    pinterest_Link = table.Column<string>(nullable: true),
                    twitter_Link = table.Column<string>(nullable: true),
                    website_Link = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Company", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "ContestComment",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    Created_Date = table.Column<DateTime>(nullable: false),
                    Updated_Date = table.Column<DateTime>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ContestComment", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "ContestDetail",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    Created_Date = table.Column<DateTime>(nullable: false),
                    Description = table.Column<string>(nullable: false),
                    EndDate = table.Column<DateTime>(nullable: false),
                    StartDate = table.Column<DateTime>(nullable: false),
                    Status = table.Column<bool>(nullable: false),
                    Title = table.Column<string>(nullable: false),
                    Updated_Date = table.Column<DateTime>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ContestDetail", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "CurriculumRedirect",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    Browser = table.Column<string>(nullable: false),
                    Created_Date = table.Column<DateTime>(nullable: false),
                    Updated_Date = table.Column<DateTime>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_CurriculumRedirect", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "CustomPackage",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    Created_Date = table.Column<DateTime>(nullable: false),
                    Description = table.Column<string>(nullable: false),
                    ImageUrl = table.Column<string>(nullable: false),
                    Name = table.Column<string>(nullable: false),
                    Updated_Date = table.Column<DateTime>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_CustomPackage", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Dulwainfo",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    Created_Date = table.Column<DateTime>(nullable: false),
                    Description = table.Column<string>(nullable: false),
                    Image = table.Column<string>(nullable: false),
                    Lat = table.Column<string>(nullable: false),
                    Name = table.Column<string>(nullable: false),
                    Popular = table.Column<string>(nullable: false),
                    Rating = table.Column<decimal>(nullable: false),
                    Season = table.Column<string>(nullable: false),
                    Type = table.Column<string>(nullable: false),
                    Updated_Date = table.Column<DateTime>(nullable: false),
                    lon = table.Column<string>(nullable: false),
                    trending = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Dulwainfo", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "KeyCategory",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    Created_Date = table.Column<DateTime>(nullable: false),
                    Name = table.Column<string>(nullable: true),
                    Updated_Date = table.Column<DateTime>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_KeyCategory", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Roles",
                columns: table => new
                {
                    Id = table.Column<string>(nullable: false),
                    ConcurrencyStamp = table.Column<string>(nullable: true),
                    Name = table.Column<string>(maxLength: 256, nullable: true),
                    NormalizedName = table.Column<string>(maxLength: 256, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Roles", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "ShoppingCart",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    CartId = table.Column<string>(nullable: false),
                    Created_Date = table.Column<DateTime>(nullable: false),
                    OrderId = table.Column<string>(nullable: true),
                    Payment = table.Column<bool>(nullable: false),
                    Updated_Date = table.Column<DateTime>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ShoppingCart", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "StoreCategory",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    Created_Date = table.Column<DateTime>(nullable: false),
                    ImageUrl = table.Column<string>(nullable: false),
                    Name = table.Column<string>(nullable: false),
                    Updated_Date = table.Column<DateTime>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_StoreCategory", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "StoryCategory",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    Created_Date = table.Column<DateTime>(nullable: false),
                    Name = table.Column<string>(nullable: false),
                    Updated_Date = table.Column<DateTime>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_StoryCategory", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Users",
                columns: table => new
                {
                    Id = table.Column<string>(nullable: false),
                    AccessFailedCount = table.Column<int>(nullable: false),
                    ConcurrencyStamp = table.Column<string>(nullable: true),
                    Email = table.Column<string>(maxLength: 256, nullable: true),
                    EmailConfirmed = table.Column<bool>(nullable: false),
                    FullName = table.Column<string>(nullable: true),
                    LockoutEnabled = table.Column<bool>(nullable: false),
                    LockoutEnd = table.Column<DateTimeOffset>(nullable: true),
                    NormalizedEmail = table.Column<string>(maxLength: 256, nullable: true),
                    NormalizedUserName = table.Column<string>(maxLength: 256, nullable: true),
                    PasswordHash = table.Column<string>(nullable: true),
                    PhoneNumber = table.Column<string>(nullable: true),
                    PhoneNumberConfirmed = table.Column<bool>(nullable: false),
                    SecurityStamp = table.Column<string>(nullable: true),
                    TwoFactorEnabled = table.Column<bool>(nullable: false),
                    UserName = table.Column<string>(maxLength: 256, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Users", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Contest",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    AutherName = table.Column<string>(nullable: false),
                    ContestDetailId = table.Column<Guid>(nullable: false),
                    Created_Date = table.Column<DateTime>(nullable: false),
                    Description = table.Column<string>(nullable: false),
                    ImageUrl = table.Column<string>(nullable: false),
                    Updated_Date = table.Column<DateTime>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Contest", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Contest_ContestDetail_ContestDetailId",
                        column: x => x.ContestDetailId,
                        principalTable: "ContestDetail",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "ContestUser",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    ContestDetailId = table.Column<Guid>(nullable: false),
                    Created_Date = table.Column<DateTime>(nullable: false),
                    ImageStatus = table.Column<bool>(nullable: false),
                    Updated_Date = table.Column<DateTime>(nullable: false),
                    UserId = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ContestUser", x => x.Id);
                    table.ForeignKey(
                        name: "FK_ContestUser_ContestDetail_ContestDetailId",
                        column: x => x.ContestDetailId,
                        principalTable: "ContestDetail",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Cuisine",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    Caption = table.Column<string>(nullable: true),
                    Community = table.Column<string>(nullable: true),
                    Created_Date = table.Column<DateTime>(nullable: false),
                    Details = table.Column<string>(nullable: true),
                    DulwainfoId = table.Column<Guid>(nullable: false),
                    Festival = table.Column<string>(nullable: true),
                    Image = table.Column<string>(nullable: true),
                    Name = table.Column<string>(nullable: true),
                    Price = table.Column<string>(nullable: true),
                    Recipe = table.Column<string>(nullable: true),
                    Updated_Date = table.Column<DateTime>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Cuisine", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Cuisine_Dulwainfo_DulwainfoId",
                        column: x => x.DulwainfoId,
                        principalTable: "Dulwainfo",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Curriculum",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    Created_Date = table.Column<DateTime>(nullable: false),
                    Description = table.Column<string>(nullable: false),
                    DulwainfoId = table.Column<Guid>(nullable: false),
                    ImageUrl = table.Column<string>(nullable: false),
                    IsLocal = table.Column<bool>(nullable: false),
                    Name = table.Column<string>(nullable: false),
                    Price = table.Column<string>(nullable: true),
                    Updated_Date = table.Column<DateTime>(nullable: false),
                    redirectCount = table.Column<int>(nullable: false),
                    redirectLink = table.Column<string>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Curriculum", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Curriculum_Dulwainfo_DulwainfoId",
                        column: x => x.DulwainfoId,
                        principalTable: "Dulwainfo",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "DestinationStops",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    Created_Date = table.Column<DateTime>(nullable: false),
                    DulwainfoId = table.Column<Guid>(nullable: false),
                    Icon = table.Column<string>(nullable: false),
                    Name = table.Column<string>(nullable: false),
                    Updated_Date = table.Column<DateTime>(nullable: false),
                    expensive = table.Column<string>(nullable: true),
                    famousFor = table.Column<string>(nullable: false),
                    imageUrl = table.Column<string>(nullable: true),
                    lat = table.Column<string>(nullable: false),
                    lon = table.Column<string>(nullable: false),
                    rating = table.Column<decimal>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_DestinationStops", x => x.Id);
                    table.ForeignKey(
                        name: "FK_DestinationStops_Dulwainfo_DulwainfoId",
                        column: x => x.DulwainfoId,
                        principalTable: "Dulwainfo",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Event",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    Caption = table.Column<string>(nullable: true),
                    Created_Date = table.Column<DateTime>(nullable: false),
                    Date = table.Column<DateTime>(nullable: false),
                    Description = table.Column<string>(nullable: true),
                    DulwainfoId = table.Column<Guid>(nullable: false),
                    Image = table.Column<string>(nullable: true),
                    Location = table.Column<string>(nullable: true),
                    Name = table.Column<string>(nullable: true),
                    Organizers = table.Column<string>(nullable: true),
                    Ticket = table.Column<bool>(nullable: false),
                    Type = table.Column<string>(nullable: true),
                    Updated_Date = table.Column<DateTime>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Event", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Event_Dulwainfo_DulwainfoId",
                        column: x => x.DulwainfoId,
                        principalTable: "Dulwainfo",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Gallery",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    Created_Date = table.Column<DateTime>(nullable: false),
                    DulwainfoId = table.Column<Guid>(nullable: false),
                    Name = table.Column<string>(nullable: false),
                    Updated_Date = table.Column<DateTime>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Gallery", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Gallery_Dulwainfo_DulwainfoId",
                        column: x => x.DulwainfoId,
                        principalTable: "Dulwainfo",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Package",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    CompanyId = table.Column<Guid>(nullable: false),
                    Created_Date = table.Column<DateTime>(nullable: false),
                    Description = table.Column<string>(nullable: false),
                    Difficulty = table.Column<int>(nullable: false),
                    DulwainfoId = table.Column<Guid>(nullable: false),
                    Duration = table.Column<string>(nullable: false),
                    EndDate = table.Column<DateTime>(nullable: false),
                    Name = table.Column<string>(nullable: false),
                    Price = table.Column<decimal>(nullable: false),
                    StartDate = table.Column<DateTime>(nullable: false),
                    StoreCategory = table.Column<string>(nullable: true),
                    Types = table.Column<string>(nullable: false),
                    Updated_Date = table.Column<DateTime>(nullable: false),
                    image = table.Column<string>(nullable: true),
                    lat = table.Column<string>(nullable: true),
                    lon = table.Column<string>(nullable: true),
                    rating = table.Column<decimal>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Package", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Package_Company_CompanyId",
                        column: x => x.CompanyId,
                        principalTable: "Company",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_Package_Dulwainfo_DulwainfoId",
                        column: x => x.DulwainfoId,
                        principalTable: "Dulwainfo",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Tradition_Culture",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    Caption = table.Column<string>(nullable: true),
                    Community = table.Column<string>(nullable: true),
                    Created_Date = table.Column<DateTime>(nullable: false),
                    Description = table.Column<string>(nullable: true),
                    DulwainfoId = table.Column<Guid>(nullable: false),
                    Image = table.Column<string>(nullable: true),
                    Name = table.Column<string>(nullable: true),
                    NepaliDate = table.Column<string>(nullable: true),
                    Purpose = table.Column<string>(nullable: true),
                    Updated_Date = table.Column<DateTime>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Tradition_Culture", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Tradition_Culture_Dulwainfo_DulwainfoId",
                        column: x => x.DulwainfoId,
                        principalTable: "Dulwainfo",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Keys",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    Created_Date = table.Column<DateTime>(nullable: false),
                    KeyCategoryId = table.Column<Guid>(nullable: false),
                    Name = table.Column<string>(nullable: true),
                    Updated_Date = table.Column<DateTime>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Keys", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Keys_KeyCategory_KeyCategoryId",
                        column: x => x.KeyCategoryId,
                        principalTable: "KeyCategory",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "AspNetRoleClaims",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    ClaimType = table.Column<string>(nullable: true),
                    ClaimValue = table.Column<string>(nullable: true),
                    RoleId = table.Column<string>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AspNetRoleClaims", x => x.Id);
                    table.ForeignKey(
                        name: "FK_AspNetRoleClaims_Roles_RoleId",
                        column: x => x.RoleId,
                        principalTable: "Roles",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Store",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    Created_Date = table.Column<DateTime>(nullable: false),
                    Description = table.Column<string>(nullable: false),
                    ImageUrl = table.Column<string>(nullable: false),
                    Name = table.Column<string>(nullable: false),
                    ProductId = table.Column<string>(nullable: false),
                    StoreCategoryId = table.Column<Guid>(nullable: false),
                    Updated_Date = table.Column<DateTime>(nullable: false),
                    price = table.Column<decimal>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Store", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Store_StoreCategory_StoreCategoryId",
                        column: x => x.StoreCategoryId,
                        principalTable: "StoreCategory",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "AspNetUserTokens",
                columns: table => new
                {
                    UserId = table.Column<string>(nullable: false),
                    LoginProvider = table.Column<string>(nullable: false),
                    Name = table.Column<string>(nullable: false),
                    Value = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AspNetUserTokens", x => new { x.UserId, x.LoginProvider, x.Name });
                    table.ForeignKey(
                        name: "FK_AspNetUserTokens_Users_UserId",
                        column: x => x.UserId,
                        principalTable: "Users",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "DulwaStory",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    Approval = table.Column<bool>(nullable: false),
                    Author = table.Column<string>(nullable: false),
                    Created_Date = table.Column<DateTime>(nullable: false),
                    Date = table.Column<DateTime>(nullable: false),
                    Description = table.Column<string>(nullable: false),
                    DulwainfoId = table.Column<Guid>(nullable: false),
                    ImageUrl = table.Column<string>(nullable: false),
                    Link = table.Column<string>(nullable: false),
                    Name = table.Column<string>(nullable: false),
                    Rating = table.Column<decimal>(nullable: false),
                    StoryCategoryId = table.Column<Guid>(nullable: false),
                    Updated_Date = table.Column<DateTime>(nullable: false),
                    UserId = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_DulwaStory", x => x.Id);
                    table.ForeignKey(
                        name: "FK_DulwaStory_Dulwainfo_DulwainfoId",
                        column: x => x.DulwainfoId,
                        principalTable: "Dulwainfo",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_DulwaStory_StoryCategory_StoryCategoryId",
                        column: x => x.StoryCategoryId,
                        principalTable: "StoryCategory",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_DulwaStory_Users_UserId",
                        column: x => x.UserId,
                        principalTable: "Users",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "UserClaims",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    ClaimType = table.Column<string>(nullable: true),
                    ClaimValue = table.Column<string>(nullable: true),
                    UserId = table.Column<string>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_UserClaims", x => x.Id);
                    table.ForeignKey(
                        name: "FK_UserClaims_Users_UserId",
                        column: x => x.UserId,
                        principalTable: "Users",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "UserLogins",
                columns: table => new
                {
                    LoginProvider = table.Column<string>(nullable: false),
                    ProviderKey = table.Column<string>(nullable: false),
                    ProviderDisplayName = table.Column<string>(nullable: true),
                    UserId = table.Column<string>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_UserLogins", x => new { x.LoginProvider, x.ProviderKey });
                    table.ForeignKey(
                        name: "FK_UserLogins_Users_UserId",
                        column: x => x.UserId,
                        principalTable: "Users",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "UserRoles",
                columns: table => new
                {
                    UserId = table.Column<string>(nullable: false),
                    RoleId = table.Column<string>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_UserRoles", x => new { x.UserId, x.RoleId });
                    table.ForeignKey(
                        name: "FK_UserRoles_Roles_RoleId",
                        column: x => x.RoleId,
                        principalTable: "Roles",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_UserRoles_Users_UserId",
                        column: x => x.UserId,
                        principalTable: "Users",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "ContestImage",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    ContestId = table.Column<Guid>(nullable: false),
                    Created_Date = table.Column<DateTime>(nullable: false),
                    Updated_Date = table.Column<DateTime>(nullable: false),
                    imageUrl = table.Column<string>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ContestImage", x => x.Id);
                    table.ForeignKey(
                        name: "FK_ContestImage_Contest_ContestId",
                        column: x => x.ContestId,
                        principalTable: "Contest",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "ContestUserLike",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    ContestId = table.Column<Guid>(nullable: false),
                    Created_Date = table.Column<DateTime>(nullable: false),
                    Like = table.Column<bool>(nullable: false),
                    Updated_Date = table.Column<DateTime>(nullable: false),
                    UserId = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ContestUserLike", x => x.Id);
                    table.ForeignKey(
                        name: "FK_ContestUserLike_Contest_ContestId",
                        column: x => x.ContestId,
                        principalTable: "Contest",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "GalleryImage",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    Created_Date = table.Column<DateTime>(nullable: false),
                    Description = table.Column<string>(nullable: true),
                    GalleryId = table.Column<Guid>(nullable: false),
                    ImageUrl = table.Column<string>(nullable: true),
                    Title = table.Column<string>(nullable: true),
                    Updated_Date = table.Column<DateTime>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_GalleryImage", x => x.Id);
                    table.ForeignKey(
                        name: "FK_GalleryImage_Gallery_GalleryId",
                        column: x => x.GalleryId,
                        principalTable: "Gallery",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "GalleryVideo",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    Created_Date = table.Column<DateTime>(nullable: false),
                    Description = table.Column<string>(nullable: true),
                    GalleryId = table.Column<Guid>(nullable: false),
                    Title = table.Column<string>(nullable: false),
                    Updated_Date = table.Column<DateTime>(nullable: false),
                    VideoUrl = table.Column<string>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_GalleryVideo", x => x.Id);
                    table.ForeignKey(
                        name: "FK_GalleryVideo_Gallery_GalleryId",
                        column: x => x.GalleryId,
                        principalTable: "Gallery",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "PackageDate",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    Created_Date = table.Column<DateTime>(nullable: false),
                    Date = table.Column<DateTime>(nullable: false),
                    PackageId = table.Column<Guid>(nullable: false),
                    Updated_Date = table.Column<DateTime>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_PackageDate", x => x.Id);
                    table.ForeignKey(
                        name: "FK_PackageDate_Package_PackageId",
                        column: x => x.PackageId,
                        principalTable: "Package",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Review",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    Created_Date = table.Column<DateTime>(nullable: false),
                    DulwainfoId = table.Column<Guid>(nullable: true),
                    Message = table.Column<string>(nullable: true),
                    Name = table.Column<string>(nullable: false),
                    PackageId = table.Column<Guid>(nullable: true),
                    Updated_Date = table.Column<DateTime>(nullable: false),
                    UserId = table.Column<string>(nullable: true),
                    score = table.Column<decimal>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Review", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Review_Dulwainfo_DulwainfoId",
                        column: x => x.DulwainfoId,
                        principalTable: "Dulwainfo",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Review_Package_PackageId",
                        column: x => x.PackageId,
                        principalTable: "Package",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Review_Users_UserId",
                        column: x => x.UserId,
                        principalTable: "Users",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "ShoppingCartItem",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    CartId = table.Column<Guid>(nullable: false),
                    Created_Date = table.Column<DateTime>(nullable: false),
                    Quantity = table.Column<int>(nullable: false),
                    StoreId = table.Column<Guid>(nullable: false),
                    Updated_Date = table.Column<DateTime>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ShoppingCartItem", x => x.Id);
                    table.ForeignKey(
                        name: "FK_ShoppingCartItem_ShoppingCart_CartId",
                        column: x => x.CartId,
                        principalTable: "ShoppingCart",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_ShoppingCartItem_Store_StoreId",
                        column: x => x.StoreId,
                        principalTable: "Store",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "StoreDetail",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    Created_Date = table.Column<DateTime>(nullable: false),
                    StoreId = table.Column<Guid>(nullable: false),
                    Updated_Date = table.Column<DateTime>(nullable: false),
                    color = table.Column<string>(nullable: false),
                    description = table.Column<string>(nullable: false),
                    imageUrl = table.Column<string>(nullable: false),
                    size = table.Column<string>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_StoreDetail", x => x.Id);
                    table.ForeignKey(
                        name: "FK_StoreDetail_Store_StoreId",
                        column: x => x.StoreId,
                        principalTable: "Store",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "UserPurchaseItem",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    Created_Date = table.Column<DateTime>(nullable: false),
                    StoreId = table.Column<Guid>(nullable: false),
                    Updated_Date = table.Column<DateTime>(nullable: false),
                    UserId = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_UserPurchaseItem", x => x.Id);
                    table.ForeignKey(
                        name: "FK_UserPurchaseItem_Store_StoreId",
                        column: x => x.StoreId,
                        principalTable: "Store",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_UserPurchaseItem_Users_UserId",
                        column: x => x.UserId,
                        principalTable: "Users",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "WishList",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    Created_Date = table.Column<DateTime>(nullable: false),
                    StoreId = table.Column<Guid>(nullable: false),
                    Updated_Date = table.Column<DateTime>(nullable: false),
                    UserId = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_WishList", x => x.Id);
                    table.ForeignKey(
                        name: "FK_WishList_Store_StoreId",
                        column: x => x.StoreId,
                        principalTable: "Store",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_WishList_Users_UserId",
                        column: x => x.UserId,
                        principalTable: "Users",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Route",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    Created_Date = table.Column<DateTime>(nullable: false),
                    CustomPackageId = table.Column<Guid>(nullable: true),
                    DulwaInfoId = table.Column<Guid>(nullable: true),
                    DulwaStoryId = table.Column<Guid>(nullable: true),
                    MapType = table.Column<string>(nullable: false),
                    PackageId = table.Column<Guid>(nullable: true),
                    Updated_Date = table.Column<DateTime>(nullable: false),
                    lat = table.Column<string>(nullable: false),
                    lon = table.Column<string>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Route", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Route_CustomPackage_CustomPackageId",
                        column: x => x.CustomPackageId,
                        principalTable: "CustomPackage",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Route_Dulwainfo_DulwaInfoId",
                        column: x => x.DulwaInfoId,
                        principalTable: "Dulwainfo",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Route_DulwaStory_DulwaStoryId",
                        column: x => x.DulwaStoryId,
                        principalTable: "DulwaStory",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Route_Package_PackageId",
                        column: x => x.PackageId,
                        principalTable: "Package",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_AspNetRoleClaims_RoleId",
                table: "AspNetRoleClaims",
                column: "RoleId");

            migrationBuilder.CreateIndex(
                name: "IX_Contest_ContestDetailId",
                table: "Contest",
                column: "ContestDetailId");

            migrationBuilder.CreateIndex(
                name: "IX_ContestImage_ContestId",
                table: "ContestImage",
                column: "ContestId");

            migrationBuilder.CreateIndex(
                name: "IX_ContestUser_ContestDetailId",
                table: "ContestUser",
                column: "ContestDetailId");

            migrationBuilder.CreateIndex(
                name: "IX_ContestUserLike_ContestId",
                table: "ContestUserLike",
                column: "ContestId");

            migrationBuilder.CreateIndex(
                name: "IX_Cuisine_DulwainfoId",
                table: "Cuisine",
                column: "DulwainfoId");

            migrationBuilder.CreateIndex(
                name: "IX_Curriculum_DulwainfoId",
                table: "Curriculum",
                column: "DulwainfoId");

            migrationBuilder.CreateIndex(
                name: "IX_DestinationStops_DulwainfoId",
                table: "DestinationStops",
                column: "DulwainfoId");

            migrationBuilder.CreateIndex(
                name: "IX_DulwaStory_DulwainfoId",
                table: "DulwaStory",
                column: "DulwainfoId");

            migrationBuilder.CreateIndex(
                name: "IX_DulwaStory_StoryCategoryId",
                table: "DulwaStory",
                column: "StoryCategoryId");

            migrationBuilder.CreateIndex(
                name: "IX_DulwaStory_UserId",
                table: "DulwaStory",
                column: "UserId");

            migrationBuilder.CreateIndex(
                name: "IX_Event_DulwainfoId",
                table: "Event",
                column: "DulwainfoId");

            migrationBuilder.CreateIndex(
                name: "IX_Gallery_DulwainfoId",
                table: "Gallery",
                column: "DulwainfoId",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_GalleryImage_GalleryId",
                table: "GalleryImage",
                column: "GalleryId");

            migrationBuilder.CreateIndex(
                name: "IX_GalleryVideo_GalleryId",
                table: "GalleryVideo",
                column: "GalleryId");

            migrationBuilder.CreateIndex(
                name: "IX_Keys_KeyCategoryId",
                table: "Keys",
                column: "KeyCategoryId");

            migrationBuilder.CreateIndex(
                name: "IX_Package_CompanyId",
                table: "Package",
                column: "CompanyId");

            migrationBuilder.CreateIndex(
                name: "IX_Package_DulwainfoId",
                table: "Package",
                column: "DulwainfoId");

            migrationBuilder.CreateIndex(
                name: "IX_PackageDate_PackageId",
                table: "PackageDate",
                column: "PackageId");

            migrationBuilder.CreateIndex(
                name: "IX_Review_DulwainfoId",
                table: "Review",
                column: "DulwainfoId");

            migrationBuilder.CreateIndex(
                name: "IX_Review_PackageId",
                table: "Review",
                column: "PackageId");

            migrationBuilder.CreateIndex(
                name: "IX_Review_UserId",
                table: "Review",
                column: "UserId");

            migrationBuilder.CreateIndex(
                name: "RoleNameIndex",
                table: "Roles",
                column: "NormalizedName",
                unique: true,
                filter: "[NormalizedName] IS NOT NULL");

            migrationBuilder.CreateIndex(
                name: "IX_Route_CustomPackageId",
                table: "Route",
                column: "CustomPackageId");

            migrationBuilder.CreateIndex(
                name: "IX_Route_DulwaInfoId",
                table: "Route",
                column: "DulwaInfoId");

            migrationBuilder.CreateIndex(
                name: "IX_Route_DulwaStoryId",
                table: "Route",
                column: "DulwaStoryId");

            migrationBuilder.CreateIndex(
                name: "IX_Route_PackageId",
                table: "Route",
                column: "PackageId");

            migrationBuilder.CreateIndex(
                name: "IX_ShoppingCartItem_CartId",
                table: "ShoppingCartItem",
                column: "CartId");

            migrationBuilder.CreateIndex(
                name: "IX_ShoppingCartItem_StoreId",
                table: "ShoppingCartItem",
                column: "StoreId");

            migrationBuilder.CreateIndex(
                name: "IX_Store_StoreCategoryId",
                table: "Store",
                column: "StoreCategoryId");

            migrationBuilder.CreateIndex(
                name: "IX_StoreDetail_StoreId",
                table: "StoreDetail",
                column: "StoreId",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_Tradition_Culture_DulwainfoId",
                table: "Tradition_Culture",
                column: "DulwainfoId");

            migrationBuilder.CreateIndex(
                name: "IX_UserClaims_UserId",
                table: "UserClaims",
                column: "UserId");

            migrationBuilder.CreateIndex(
                name: "IX_UserLogins_UserId",
                table: "UserLogins",
                column: "UserId");

            migrationBuilder.CreateIndex(
                name: "IX_UserPurchaseItem_StoreId",
                table: "UserPurchaseItem",
                column: "StoreId");

            migrationBuilder.CreateIndex(
                name: "IX_UserPurchaseItem_UserId",
                table: "UserPurchaseItem",
                column: "UserId");

            migrationBuilder.CreateIndex(
                name: "IX_UserRoles_RoleId",
                table: "UserRoles",
                column: "RoleId");

            migrationBuilder.CreateIndex(
                name: "EmailIndex",
                table: "Users",
                column: "NormalizedEmail");

            migrationBuilder.CreateIndex(
                name: "UserNameIndex",
                table: "Users",
                column: "NormalizedUserName",
                unique: true,
                filter: "[NormalizedUserName] IS NOT NULL");

            migrationBuilder.CreateIndex(
                name: "IX_WishList_StoreId",
                table: "WishList",
                column: "StoreId");

            migrationBuilder.CreateIndex(
                name: "IX_WishList_UserId",
                table: "WishList",
                column: "UserId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Advertisement");

            migrationBuilder.DropTable(
                name: "AspNetRoleClaims");

            migrationBuilder.DropTable(
                name: "AspNetUserTokens");

            migrationBuilder.DropTable(
                name: "ContestComment");

            migrationBuilder.DropTable(
                name: "ContestImage");

            migrationBuilder.DropTable(
                name: "ContestUser");

            migrationBuilder.DropTable(
                name: "ContestUserLike");

            migrationBuilder.DropTable(
                name: "Cuisine");

            migrationBuilder.DropTable(
                name: "Curriculum");

            migrationBuilder.DropTable(
                name: "CurriculumRedirect");

            migrationBuilder.DropTable(
                name: "DestinationStops");

            migrationBuilder.DropTable(
                name: "Event");

            migrationBuilder.DropTable(
                name: "GalleryImage");

            migrationBuilder.DropTable(
                name: "GalleryVideo");

            migrationBuilder.DropTable(
                name: "Keys");

            migrationBuilder.DropTable(
                name: "PackageDate");

            migrationBuilder.DropTable(
                name: "Review");

            migrationBuilder.DropTable(
                name: "Route");

            migrationBuilder.DropTable(
                name: "ShoppingCartItem");

            migrationBuilder.DropTable(
                name: "StoreDetail");

            migrationBuilder.DropTable(
                name: "Tradition_Culture");

            migrationBuilder.DropTable(
                name: "UserClaims");

            migrationBuilder.DropTable(
                name: "UserLogins");

            migrationBuilder.DropTable(
                name: "UserPurchaseItem");

            migrationBuilder.DropTable(
                name: "UserRoles");

            migrationBuilder.DropTable(
                name: "WishList");

            migrationBuilder.DropTable(
                name: "Contest");

            migrationBuilder.DropTable(
                name: "Gallery");

            migrationBuilder.DropTable(
                name: "KeyCategory");

            migrationBuilder.DropTable(
                name: "CustomPackage");

            migrationBuilder.DropTable(
                name: "DulwaStory");

            migrationBuilder.DropTable(
                name: "Package");

            migrationBuilder.DropTable(
                name: "ShoppingCart");

            migrationBuilder.DropTable(
                name: "Roles");

            migrationBuilder.DropTable(
                name: "Store");

            migrationBuilder.DropTable(
                name: "ContestDetail");

            migrationBuilder.DropTable(
                name: "StoryCategory");

            migrationBuilder.DropTable(
                name: "Users");

            migrationBuilder.DropTable(
                name: "Company");

            migrationBuilder.DropTable(
                name: "Dulwainfo");

            migrationBuilder.DropTable(
                name: "StoreCategory");
        }
    }
}
