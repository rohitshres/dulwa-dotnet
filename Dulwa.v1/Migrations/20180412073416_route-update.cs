﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Dulwa.v1.Migrations
{
    public partial class routeupdate : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Route_Package_PackageId",
                table: "Route");

            migrationBuilder.AddForeignKey(
                name: "FK_Route_Package_PackageId",
                table: "Route",
                column: "PackageId",
                principalTable: "Package",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Route_Package_PackageId",
                table: "Route");

            migrationBuilder.AddForeignKey(
                name: "FK_Route_Package_PackageId",
                table: "Route",
                column: "PackageId",
                principalTable: "Package",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
