﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Dulwa.v1.Migrations
{
    public partial class testsfe : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Route_Dulwainfo_DulwaInfoId",
                table: "Route");

            migrationBuilder.AddForeignKey(
                name: "FK_Route_Dulwainfo_DulwaInfoId",
                table: "Route",
                column: "DulwaInfoId",
                principalTable: "Dulwainfo",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Route_Dulwainfo_DulwaInfoId",
                table: "Route");

            migrationBuilder.AddForeignKey(
                name: "FK_Route_Dulwainfo_DulwaInfoId",
                table: "Route",
                column: "DulwaInfoId",
                principalTable: "Dulwainfo",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
