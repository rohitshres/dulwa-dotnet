using System;
using System.ComponentModel.DataAnnotations;

namespace Dulwa.v1.Models
{
    public class ErrorViewModel
    {
        [Display(Name = "Request ID")]
        public string RequestId { get; set; }

        public bool ShowRequestId => !string.IsNullOrEmpty(RequestId);
    }
}