﻿using Dulwa.v1.Service.Advertisements;
using Dulwa.v1.Service.Packages;
using Dulwa.v1.Service.Storys;
using Dulwa.v1.ViewModel;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Dulwa.v1.ViewComponents
{
    public class SideBarViewComponent:ViewComponent
    {
        private IStoryService _storyservice;
        private IPackageService _packageservice;
        private IAdvertisementService _advertisementservice;

        public SideBarViewComponent(IStoryService storyservice, IPackageService packageservice, IAdvertisementService advertisementservice)
        {
            _storyservice = storyservice;
            _packageservice = packageservice;
            _advertisementservice = advertisementservice;
        }
        public async Task<IViewComponentResult> InvokeAsync()
        {
            var SideBar = new SideBarListViewModel();
            SideBar.Story =await _storyservice.GetTopStory(4);
            SideBar.Package = await _packageservice.GetTopPackage(4);
            SideBar.Advertisement =await _advertisementservice.GetAdvertisementList(2);
            return View(SideBar);
        }
    }
}
