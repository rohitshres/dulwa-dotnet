﻿angular.module('startapp', ['commom.services'])
    .service('sharedProperties', function ($rootScope) {
        var Total = {
            total: 0,
            items: 0
        };        
        return {
            setTotalItems: function (total) {
                Total = total;
                $rootScope.$broadcast('LoadTotalCart');

            },
            getTotalItems: function () {
                return Total;
            }
        };
    });
  