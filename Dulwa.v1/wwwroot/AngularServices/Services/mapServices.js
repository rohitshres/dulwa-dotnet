﻿angular.module('commom.services')
    .factory("MapServices", ["$resource", "appSettings", "$http", MapServices]);

function MapServices($resource, appSettings, $http) {
    $http.defaults.headers.post['Content-Type'] = 'application/json; charset=utf-8';
    return $resource(appSettings.serverPath + "/Transaction", null,
    {
        DulwaInfoTop: {
            method: 'GET',
            isArray: true,
            url: appSettings.serverPath + "/Rest/DulwaInfoList",
            responseType: 'json'
        },
        NearDistance: {
            method: 'GET',
            isArray: true,
            params: { lat: '@lat',lon:'@lon' },
            url: appSettings.serverPath + "/Rest/NearDistance",
            responseType: 'json'
        },
        NearAttraction: {
            method: 'GET',
            isArray: true,
            params: { Id: '@Id' },
            url: appSettings.serverPath + "/Rest/NearAttraction",
            responseType: 'json'
        },
        RouteType: {
            method: 'GET',
            isArray: true,
            params: { id: '@id', type: '@type' },
            url: appSettings.serverPath + "/Rest/Route",
            responseType: 'json'
        },
        Dulwastops: {
            method: 'GET',
            isArray: true,
            params: { id: '@id' },
            url: appSettings.serverPath + "/Rest/RouteStops",
            responseType: 'json'
        },        
        SearchDulwainfo: {
            method: 'GET',
            isArray: true,
            params: { key:'@key'},
            url: appSettings.serverPath + "/Rest/SerachDulwainfo",
            responseType: 'json'
        },
        DulwaPackage: {
            method: 'GET',
            isArray: true,
            params: { id: '@id' },
            url: appSettings.serverPath + "/Rest/PackageDetail",
            responseType: 'json'
        },
        PackageRoute: {
            method: 'GET',
            isArray: true,
            params: { id: '@id' },
            url: appSettings.serverPath + "/Rest/PackageRoute",
            responseType: 'json'
        },
        StoryList:{
            method: 'GET',
            isArray: true,
            params: { take: '@take',id:'@id'},
            url: appSettings.serverPath + "/Rest/StoryList",
            responseType: 'json'
        },
        GalleryList: {
            method: 'GET',
            isArray: true,
            params: { skip: '@take', take: '@take' },
            url: appSettings.serverPath + "/Rest/GalleryList",
            responseType: 'json'
        },
        GallerySpecific: {
            method: 'GET',
            isArray: true,
            params: { skip: '@take', take: '@take',type:"@type" },
            url: appSettings.serverPath + "/Rest/GallerySpecific",
            responseType: 'json'
        },
        WeatherService: {
            method: 'GET',
            isArray: true,
            params: { lat: '@lat', lon: '@lon'},
            url: "https://api.darksky.net/forecast/b25642a88ddc49e1c681d9edd79873a9/",
            responseType: 'json'
        },
        StoreList: {
            method: 'GET',
            isArray: true,
            params: { id: '@id' },
            url: appSettings.serverPath + "/Rest/StoreList",
            responseType: 'json'
        },
        ReviewMode: {
            method: 'POST',
            isArray: false,
            url: appSettings.serverPath + "/Rest/Reviews",
            responseType: 'json'
        },
        AddtoCart: {
            method: 'GET',
            isArray: false,
            params: { id: '@id', Quantity:'@quantity' },
            url: appSettings.serverPath + "/Store/addtocart",
            responseType: 'json'
        },
        CartItem: {
            method: 'GET',
            isArray: false,
            url: appSettings.serverPath + "/Store/cartitemdetail",
            responseType: 'json'
        },
        WishListAdd: {
            method: 'GET',
            isArray: false,
            params: {id:'@id'},
            url: appSettings.serverPath + "/WishList/AddWishList",
            responseType: 'json'
        },
        GetCalendarData: {
            method: 'GET',
            isArray: true,
            url: appSettings.serverPath + "/Rest/Calendar",
            responseType: 'json'
        },
        GetStuffData: {
            method: 'GET',
            isArray: true,
            url: appSettings.serverPath + "/Rest/StuffYouMightNeed",
            params: { id: '@id' },
            responseType: 'json'
        },
        GetCuisineData: {
            method: 'GET',
            isArray: true,
            url: appSettings.serverPath + "/Rest/Cuisine",
            params: { id: '@id' },
            responseType: 'json'
        },
        GetEventData: {
            method: 'GET',
            isArray: true,
            url: appSettings.serverPath + "/Rest/Event",
            params: { id: '@id' },
            responseType: 'json'
        },
        GetTraditionCultureData: {
            method: 'GET',
            isArray: true,
            url: appSettings.serverPath + "/Rest/TraditionCulture",
            params: { id: '@id' },
            responseType: 'json'
        },
        GetReviewData: {
            method: 'GET',
            isArray: true,
            url: appSettings.serverPath + "/Rest/ReviewList",
            params: { id: '@id', type:'@type' },
            responseType: 'json'
        },
        GetDulwaInformation: {
            method: 'GET',
            isArray: false,
            url: appSettings.serverPath + "/Rest/DulwaInformation",
            responseType: 'json'
        }

    });
}
