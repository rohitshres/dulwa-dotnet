﻿(function () {
    'use strict';

    angular
        .module('startapp')
        .controller('MapFullController', function MapFullController($scope, MapFull, MapServices,$rootScope) {
            $scope.loadingicon = false;
            $scope.latlon = [];
            $scope.LoadMap = function () {
                MapFull.init();
                MapFull.marker();
            }

            $scope.RouteMap = function () {
                MapFull.init();
                MapFull.routeMarker($scope.latlon);  
            }
            $scope.LoadMapData = function () {               
                    $scope.mapLat = $rootScope.pointerMap.lat;
                $scope.mapLong = $rootScope.pointerMap.long;
               
                
            };

            $scope.LoadDirection = function(){               
                $scope.direction = $rootScope.direction.latlng;
            }
        })
})();
