﻿(function () {
    'use strict';

    angular
        .module('startapp')
        .controller('StoreController', function StoreController($scope, MapServices, sharedProperties) {
            $scope.total = sharedProperties.getTotalItems();
            $scope.categorydata = function (id, name) {
                $scope.Id = id;
                $scope.Name = name;
                MapServices.StoreList({ id: id }, function (res) {
                    $scope.categorydatalist = res;
                });
            }


            $scope.addtocart = function (id, quantity) {                
                MapServices.AddtoCart({ id: id, quantity: quantity }, function (res) {
                    MapServices.CartItem(function (res) {
                        $scope.total.items = res.items;
                        $scope.total.total = res.total;
                        sharedProperties.setTotalItems($scope.total);
                    });
                });
            }

            $scope.wishlistadd = function (id) {
    
                MapServices.WishListAdd({ id: id }, function (res) {

                });
            };

        })

})();
