﻿(function () {
    'use strict';

    angular
        .module('startapp')
        .controller('GalleryController', function addCategoryController($scope, MapServices) {
            var gallerylists = [];
            $scope.galleryList;
            var skip = 0;
            var take = 5;
            var type = "all";
            var result = 1;
            $scope.show = false;
            $scope.button = true;
            var count = 0;
            MapServices.GalleryList({ skip: 0, take: 5 }, function (res) {
                gallerylists = res;
                $scope.gallerylist = gallerylists;
                angular.forEach($scope.gallerylist, function (tranlist) {
                    count++;
                    if (count === $scope.gallerylist.length) {
                        style(0);
                    }
                });
                //angular.forEach(gallerylists, function (result, key) {
                //    //ContraList[key].contraAcctNo = result.contraAcctNo;
                //    console.log(result);
                //});
            });

            $scope.arrangeGallery = function (index) {
                if (index > $scope.gallerylistlength - 2) {
                    var ancestor = document.getElementById('gallerylist');
                    var tabcontents = ancestor.getElementsByClassName("col-md-3");
                    if (tabcontents.length === $scope.gallerylist.length) {
                        for (var i = 0; i < tabcontents.length; i++) {
                            var id = "#" + tabcontents[i].id;
                            $(id).removeClass("col-md-3");
                            $(id).addClass("col-d-6").css('transition', 'ease-in-out 1s');
                        }
                    }
                }
            };
          
            $scope.loadmore = function () {
                console.log(gallerylists);
                skip = skip + take;
                MapServices.GallerySpecific({ skip: skip, take: 5, type: type }, function (res) {
                    if (res.length === 0) {
                        $scope.button = false;
                    }
                    res.forEach(element => {
                        gallerylists.push(element);
                    })
                    take = take + 5;
                    style(6);                   
                });                
            }

            $scope.GallerySpecific = function (value) {
                type = value;
                $scope.button = true;
                gallerylists.length = 0;
                skip = 0;
                take = 5;
                count = 0;
                MapServices.GallerySpecific({ skip: skip, take: take, type: value }, function (res) {
                    console.log(res);
                    gallerylists = res;
                    $scope.gallerylist = gallerylists;
                    angular.forEach($scope.gallerylist, function (tranlist) {
                        count++;
                        if (count === $scope.gallerylist.length) {
                            style(0);
                        }
                    });
                });
            }

            function style(initial) {
                setTimeout(function () {
                    var j = 0;
                    for (var i = initial; i < $scope.gallerylist.length; i++) {
                        j = i;
                        if (i >= 6) {
                            j = j - 6;
                        }
                        if (j === 0 || j === 5) {
                            var id = "#galleryItem" + i; //+ tabcontents[i].id;
                            $(id).removeClass("col-md-3");
                            $(id).addClass("col-md-6").css('transition', 'ease-in-out .5s');
                        }
                    }
                    var $lg = $('#gallerylist');
                    $lg.lightGallery();
                    $lg.data('lightGallery').destroy(true);
                    $lg.lightGallery();
                }, 1000)
            }
        })
        .directive('lightgallery', function () {
            return {
                restrict: 'A',
                link: function (scope, element, attrs) {
                    if (scope.$last) {
                        // ng-repeat is completed
                        element.parent().lightGallery({
                            loadYoutubeThumbnail: true,
                            youtubeThumbSize: 'default',
                            loadVimeoThumbnail: true,
                            vimeoThumbSize: 'thumbnail_medium',
                            download: false
                        });
                    }
                }
            };
        });
})();
