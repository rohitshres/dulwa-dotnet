﻿(function () {
    'use strict';

    angular
        .module('startapp')
        .controller('MapController', function addCategoryController($scope, Map, MapServices,$window, $timeout) {
            $scope.loadingicon = false;
            $scope.DulwaInfoTop = function () {
                MapServices.DulwaInfoTop(function (res) {                    
                    for (var i = 0; i < res.length; i++) {
                        Map.addMarker(res[i]);
                    }
                    Map.makecluster(); 
                });               
            }
            $scope.LoadMap = function () {
                Map.init(7);
            }     
            $scope.neardistance = function (lat,lon) {
                MapServices.NearDistance({ lat: lat, lon: lon }, function (res) {                   
                    $scope.neardistanceplace = res;
                });
            }
            
            $scope.search = function (text) {
                $scope.loadingicon = true;
                if (text.length > 0) {
                    MapServices.SearchDulwainfo({ key: text }, function (res) {                      
                    Map.Clearmap(); 

                        $scope.dulwinfotop = res;
                        for (var i = 0; i < res.length; i++) {
                            Map.addMarker(res[i]);
                        }
                        Map.makecluster(); 
                        $scope.loadingicon = false;
                    });

                }
                else {
                    Map.Clearmap(); 
                    $scope.DulwaInfoTop();                        
                }

            }
            //new google.maps.LatLng(28.3949, 84.1240)
            window.addEventListener("resize", function () {
                var width = $('body').width() * 0.60;
                $("#map-canvas").width(width);
                $("#map-canvas").height($('body').height());
                Map.recenter(width);
            });


        })
})();
