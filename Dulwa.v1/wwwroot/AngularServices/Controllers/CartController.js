﻿(function () {
    'use strict';

    angular
        .module('startapp')
        .controller('CartController', function CartController($scope, MapServices, sharedProperties) {
            $scope.total = sharedProperties.getTotalItems();
            MapServices.CartItem(function (res) {
                $scope.total.items = res.items;
                $scope.total.total = res.total;
                sharedProperties.setTotalItems($scope.total);
            });
        })

})();
