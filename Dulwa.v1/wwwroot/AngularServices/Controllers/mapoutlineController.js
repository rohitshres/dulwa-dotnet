﻿(function () {
    'use strict';

    angular
        .module('startapp')
        .controller('mapoutlineController', function addCategoryController($scope, $http, Mapoutline, MapServices) {
            $scope.eventdata = [];
            $scope.owlOptionsTestimonials = {
                autoPlay: 4000,
                stopOnHover: true,
                slideSpeed: 300,
                paginationSpeed: 600,
                items: 2
            }
            $scope.searchicon = false;            
            $scope.LoadMap = function (lat,lon) {
                Mapoutline.init("27.692278","85.310296");
            }

            $scope.Weather = function (lat, lon) {
                $http.get("https://api.darksky.net/forecast/25ef86197ee965d8474b1a677b8be21e/"+lat+","+lon+"")
                    .then(function (response) {
                        $scope.Weatherdata=response.data.currently;
                    });
            }

            $scope.neardistance = function (lat,lon) {
                MapServices.NearDistance({ lat: lat, lon: lon }, function (res) {
                   
                    $scope.neardistanceplace = res;

                });
            }
            $scope.Route = function (id, type) { 
                MapServices.RouteType({ id: id, type }, function (res) {
                    if (type == "polygon") {
                        $scope.routeview = true;
                        $scope.placeview = false;
                        Mapoutline.polygon(res);
                        MapServices.Dulwastops({ id: id }, function (res) {
                            res.forEach(element => {
                                Mapoutline.addMarker(element);
                            })
                        });
                        
                    }
                    else if (type == "route") {
                        $scope.routeview = false;
                        $scope.placeview = true;
                        Mapoutline.route(res,"#fff");
                    }
                });
                
            }
            //new google.maps.LatLng(28.3949, 84.1240)
            $scope.nearAttraction = function (Id) {
                MapServices.NearAttraction({ Id: Id }, function (res) {
                    $scope.nearattraction = res;
                    console.log($scope.nearattraction);
                });
            }
            $scope.package = function (id) {
                MapServices.DulwaPackage({ id: id }, function (res) {
                    $scope.package = res;                    
                });
            }

            $scope.cuisine = function(Id){
                MapServices.GetCuisineData({ id: Id }, function (res) {
                    $scope.cuisinedata = res;                    
                });
            }
            $scope.event = function (Id) {
                MapServices.GetEventData({ id: Id }, function (res) {
                    $scope.eventdata = res;                    
                });
            }
            $scope.traditionculture = function (Id) {
                MapServices.GetTraditionCultureData({ id: Id }, function (res) {
                    $scope.traditionculturedata = res;                   
                });
            }

            $scope.PackageRoute = function (id) {  
                console.log(id);
                MapServices.PackageRoute({ id: id }, function (res) {
                    console.log(res);   
                    Mapoutline.route(res, "#000");
                });
            }

            $scope.Story = function (id) {
                MapServices.StoryList({ take: 4, id: id }, function (res) {
                    console.log(res);
                });
            }

            $scope.submitReviewForm = function (Id) { 
                $scope.message = true;
                MapServices.ReviewMode($scope.review, function (res) {
                    if (res.documents[0].score <= 0.05) {
                        $scope.sad = true;
                        $scope.medium = false;
                        $scope.happy = false;
                    }
                    else if (res.documents[0].score <= 0.50) {
                        $scope.sad = false;
                        $scope.medium = true;
                        $scope.happy = false;

                    }
                    else if (res.documents[0].score >= 0.70) {
                        $scope.sad = false;
                        $scope.medium = false;
                        $scope.happy = true;
                    }
                    $scope.message = false;
                    LoadReviewData($scope.review.DulwainfoId, "DulwaInfo")
                    $scope.review = {};
                    //$('#myModalReview').modal("hide");
                    $("#myModalReview").removeClass("in");
                    $(".modal-backdrop").remove();
                    $('body').removeClass('modal-open');
                    $('body').css('padding-right', '');
                    $("#myModalReview").hide();
                    $("#reviewsuccess").modal("show");
                })
            }

            $scope.LoadReview = function (Id, Type) {
                LoadReviewData(Id, Type);
            }

            function LoadReviewData(Id, Type) {
                MapServices.GetReviewData({ id: Id, type: Type }, function (res) {
                    $scope.ReviewData = res;
                });
            }
            $scope.LoadNearAttractionOnMap = function (id, near) {
                MapServices.RouteType({ id: id, type: "Polygon" }, function (res) { 
                        
                        Mapoutline.polygon(res);                      
                        Mapoutline.addMarker(near);                                           
                });
            }

            //$('#calendar').fullCalendar({              
               
            //    eventLimit: true, // allow "more" link when too many events
            //    events: [
            //        {
            //            title: 'All Day Event',
            //            start: '2018-03-01'
            //        }
            //    ]
            //});

            $scope.CalendarData = function () {
                MapServices.GetCalendarData(function (data) {
                    angular.forEach(data, function (value) {
                        $scope.eventdata.push({
                            Title: "test",
                            start: value.date
                        })
                    });
                    $('#calendar').fullCalendar({

                        eventLimit: true, // allow "more" link when too many events
                        events: $scope.eventdata                          
                       
                    });
                    console.log($scope.eventdata);


                });
                 
                
            }

            $scope.LoadDulwaInformation = function () {
                MapServices.GetDulwaInformation(function (res) {
                    $scope.dulwainformation = res;
                });
            }

            $scope.finished = function () {
                console.log("test");
                $('.owl-carousel').owlCarousel({
                    loop: false,
                    margin: 10,
                    lazyload: true,
                    nav: true,
                    responsive: {
                        0: {
                            items: 1
                        },
                        600: {
                            items: 2
                        },
                        1000: {
                            items: 4
                        }
                    }
                });
            };
        

            $scope.StuffData = function (Id) {
                MapServices.GetStuffData({ id: Id }, function (res) {
                    $scope.stuffdata = res;
                })
            }

            $scope.traditionculturemodal = function (data) {
                $scope.traditionculturemodaldata = data;
            }

            $scope.cuisinemodal = function (data) {
                $scope.cuisinemodadata = data;
            }
        })
})();
