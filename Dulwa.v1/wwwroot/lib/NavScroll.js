﻿$(document).ready(function () {
   
    $(window).scroll(function () { // check if scroll event happened    
        
        if ($(this).scrollTop() > 50) { // check if user scrolled more than 50 from top of the browser window
            $("header").removeClass("nav-transparent");
            $("header").addClass("nav-colored");
            $("#showScrollDest").slideDown('slow');
        } else {
            $("header").removeClass("nav-colored");
            $("header").addClass("nav-transparent");
            $("#showScrollDest").slideUp('slow');
            }
        });
    });
