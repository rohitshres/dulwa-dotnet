//Filtering Portfolio

$(function() {
    var selectedClass = "";
    $(".fil-cat").click(function(){
        selectedClass = $(this).attr("data-rel");
        $("#portfolioWork").fadeTo(100, 0.1);
        $("#portfolioWork div").not("."+selectedClass).fadeOut().removeClass('scale-anm');
        setTimeout(function() {
            $("."+selectedClass).fadeIn().addClass('scale-anm');
            $("#portfolioWork").fadeTo(300, 1);
        }, 300);

    });
});
//Ends==========

$(".toolbar button").off().on("click", function () {
    $(".toolbar button").removeClass("active-work");
    $(this).addClass("active-work");

});

//Nearest Destination
$('.explore-near-desti').off().on('click', function () {
    $('.nearest-desti-list').slideToggle(400);
})

//Skill bar js
jQuery('.skillbar').each(function(){
    jQuery(this).find('.skillbar-bar').animate({
        width:jQuery(this).attr('data-percent')
    },2000);
});

//Comment
$('.writeReview').off().on('click', function () {
  $('.sheded-layer').fadeIn(400);
})
$('.close-layer').off().on('click', function () {
    $('.sheded-layer').fadeOut(400);
});


//On body click hide prop =====

$("body").click(function(){
    $('.nearest-desti-list').fadeOut(200);
});

$(".nearest-desti-list").click(function(e){
    e.stopPropagation();
});

$(".nearest-desti").click(function(e){
    e.stopPropagation();
});


