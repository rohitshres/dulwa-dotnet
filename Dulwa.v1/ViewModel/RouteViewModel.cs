﻿using Dulwa.v1.Entity;
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Dulwa.v1.ViewModel
{
    public class RouteViewModel
    {
        [Display(Name ="Latitude")]
        public string lat { get; set; }       
        [Display(Name = "Longitude")]
        public string lon { get; set; }       
        [Display(Name = "Custom Package")]
        public Guid CustomPackageId { get; set; }
        public Guid? PackageId { get; set; }
        [Display(Name = "Map Type")]
        public string MapType { get; set; }
        public string LatLong { get; set; }
        [Display(Name = "Dulwa Info")]
        [ForeignKey(nameof(Dulwainfo))]
        public Guid? DulwaInfoId { get; set; }
        public Dulwainfo Dulwainfo { get; set; }
    }
}
