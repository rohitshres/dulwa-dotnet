﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Dulwa.v1.ViewModel
{
    public class CustomPackageViewModel
    {
        public Guid Id { get; set; }
        [Required]
        public string Name { get; set; }
        [Required]
        public string Description { get; set; }
        [Required]
        [Display(Name="Image URL")]
        public string ImageUrl { get; set; }
        public List<RouteViewModel> Route { get; set; }
    }
}
