﻿using Microsoft.AspNetCore.Http;
using System;
using System.ComponentModel.DataAnnotations;

namespace Dulwa.v1.ViewModel
{
    public class DestinationStopsViewModel
    {
        public Guid Id { get; set; }
        [Required]
        public string Name { get; set; }
        [Required]
        public string Icon { get; set; }
        [Required]
        [Display(Name ="Latitude")]
        public string lat { get; set; }
        [Required]
        [Display(Name = "Longitude")]
        public string lon { get; set; }       
        [Display(Name = "Image URL")]
        public string imageUrl { get; set; }
       
        [Display(Name = "Famous for")]
        public string famousFor { get; set; }
        [Display(Name = "Famous For")]
        public string[] popular { get; set; }
        [Display(Name = "Expensive")]
        public string expensive { get; set; }
        [Display(Name = "“Destination Associated")]
        public Guid DulwainfoId { get; set; }
        public DulwaInfoViewModel DulwaInfo { get; set; }
        public IFormFile File { get; set; }

    }
}
