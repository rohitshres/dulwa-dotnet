﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Dulwa.v1.ViewModel
{
    public class KeysViewModel
    {
        public Guid Id { get; set; }
        [Required]
        public string Name { get; set; }
        public Guid KeyCategoryId { get; set; }

    }
}
