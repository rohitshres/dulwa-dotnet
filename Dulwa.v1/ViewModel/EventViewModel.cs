﻿using Microsoft.AspNetCore.Http;
using System;

namespace Dulwa.v1.ViewModel
{
    public class EventViewModel
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public string Organizers { get; set; }
        public DateTime Date { get; set; }
        public string Caption { get; set; }
        public string Location { get; set; }
        public string Image { get; set; }
        public string Description { get; set; }
        public bool Ticket { get; set; }
        public string Link { get; set; }
        public string Type { get; set; }
        public IFormFile File { get; set; }
        public Guid DulwainfoId { get; set; }
    }
}
