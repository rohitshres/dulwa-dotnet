﻿using AutoMapper;
using Dulwa.v1.Areas.ViewModel;
using Dulwa.v1.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Dulwa.v1.ViewModel.Mapper
{
    public class ViewModelMapper:Profile
    {
        public ViewModelMapper()
        {
            CreateMap<Dulwainfo, DulwaInfoViewModel>().ReverseMap();
            CreateMap<Route, RouteViewModel>().ReverseMap();
            CreateMap<Package, PackageViewModel>().ReverseMap();
            CreateMap<DulwaStory, StoryViewModel>().ReverseMap();
            CreateMap<Gallery, GalleryViewModel>().ReverseMap();
            CreateMap<GalleryImage, GalleryImageViewModel>().ReverseMap();
            CreateMap<GalleryVideo, GalleryVideoViewModel>().ReverseMap();
            CreateMap<GalleryDetailViewModel, GalleryImage>().ReverseMap();
            CreateMap<GalleryDetailViewModel, GalleryVideo>().ReverseMap();
            CreateMap<StoreViewModel, Store>().ReverseMap();
            CreateMap<StoreCategoryViewModel, StoreCategory>().ReverseMap();
            CreateMap<CartItemViewModel, CartItem>().ReverseMap();
            CreateMap<CartViewModel, Cart>().ReverseMap();
            CreateMap<CompanyVM, Company>().ReverseMap();
            CreateMap<WishListViewModel, Wishlist>().ReverseMap();
            CreateMap<Review, ReviewViewModel>().ReverseMap();
            CreateMap<AdvertisementViewModel, Advertisement>().ReverseMap();
            CreateMap<PackageDateViewModel, PackageDate>().ReverseMap();
            CreateMap<CurriculumViewModel, Curriculum>().ReverseMap();
            CreateMap<ContestDetailViewModel, ContestDetail>().ReverseMap();
            CreateMap<ContestViewModel, Contest>().ReverseMap();
            CreateMap<PackageDate, PackageDateViewModel>().ReverseMap();
            CreateMap<DestinationStops, DestinationStopsViewModel>().ReverseMap();
            CreateMap<StoryCategory, StoryCategoryViewModel>().ReverseMap();
            CreateMap<Keys, KeysViewModel>().ReverseMap();
            CreateMap<Cuisine, CuisinesViewModel>().ReverseMap();
            CreateMap<Event, EventViewModel>().ReverseMap();
            CreateMap<Tradition_Culture, TraditionCultureViewModel>().ReverseMap();
            CreateMap<DulwaInformationViewModel, DulwaInformation>().ReverseMap();

        }
    }
}
