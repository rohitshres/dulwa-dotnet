﻿using System.Collections.Generic;

namespace Dulwa.v1.ViewModel
{
    public class ReviewResultViewModel
    {
        public List<Document> Documents { get; set; }
    }

    public class Document
    {
        public int id { get; set; }
        public string score { get; set; }
    }
}
