﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Dulwa.v1.ViewModel
{
    public class ReviewViewModel
    {
        public Guid Id { get; set; }
        [Required]
        public string Name { get; set; }
        public string UserProfile { get; set; }
        [Required]
        public string Message { get; set; }
        [Required]
        [Display(Name="Dulwa Info")]
        public Guid? DulwainfoId { get; set; }
        [Required]
        [Display(Name = "Package")]
        public Guid? PackageId { get; set; }        
        [Display(Name = "User ID")]
        public string UserId { get; set; }
        [Display(Name = "Score")]
        public decimal score { get; set; }

    }
}
