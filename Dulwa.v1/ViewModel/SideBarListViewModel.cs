﻿using Dulwa.v1.Areas.ViewModel;
using System.Collections.Generic;

namespace Dulwa.v1.ViewModel
{
    public class SideBarListViewModel
    {
        public IEnumerable<StoryViewModel> Story { get; set; }
        public IEnumerable<AdvertisementViewModel> Advertisement { get; set; }
        public IEnumerable<PackageViewModel> Package { get; set; }
    }
}
