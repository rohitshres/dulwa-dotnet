﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Dulwa.v1.ViewModel
{
    public class LatLongViewModel
    {
        public string Lat { get; set; }
        public string lon { get; set; }
    }
}
