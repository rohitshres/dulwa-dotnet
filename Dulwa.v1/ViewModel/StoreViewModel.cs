﻿using Microsoft.AspNetCore.Http;
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Dulwa.v1.ViewModel
{
    public class StoreViewModel
    {
        [Required]
        public Guid Id { get; set; }
        public string slug { get; set; }
        [Required]
        public string Name { get; set; }
        [Required]
        public string Description { get; set; }
        [Display(Name = "Image URL")]
        public string ImageUrl { get; set; }
        [Required]
        [Display(Name = "Price")]
        public decimal price { get; set; }
        [Required]
        [Display(Name="Product Code")]
        public string ProductId { get; set; }
        [Required]
        [Display(Name = "Store Category")]
        public Guid StoreCategoryId { get; set; }
        [NotMapped]
        public IFormFile File { get; set; }
        public StoreDetailViewModel StoreDetail { get; set; }
    }
}
