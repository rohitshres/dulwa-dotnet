﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Dulwa.v1.ViewModel
{
    public class GalleryDetailViewModel
    {
        public Guid Id { get; set; }
        [Display(Name ="Image URL")]
        public string ImageUrl { get; set; }
        [Display(Name = "Video URL")]
        public string VideoUrl { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        [Required]
        [Display(Name = "Created Date")]
        public DateTime Created_Date { get; set; }

    }
}
