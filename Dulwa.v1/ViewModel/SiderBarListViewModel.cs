﻿using Dulwa.v1.Areas.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Dulwa.v1.ViewModel
{
    public class SiderBarListViewModel
    {
        public IEnumerable<StoryViewModel> Story { get; set; }
        public IEnumerable<AdvertisementViewModel> Advertisement { get; set; }
        public IEnumerable<PackageViewModel> Package { get; set; }
    }
}
