﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Dulwa.v1.ViewModel
{
    public class GalleryVideoViewModel
    {
        public Guid Id { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        [Display(Name = "Video URL")]
        public string VideoUrl { get; set; }
        public Guid GalleryId { get; set; }
    }
}
