﻿using Dulwa.v1.Entity;
using Dulwa.v1.Models;
using Microsoft.AspNetCore.Http;
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Dulwa.v1.ViewModel
{
    public class StoryViewModel
    {
        public Guid Id { get; set; }
        public string slug { get; set; }

        [Required]
        [Display(Name="Story Title")]
        public string Name { get; set; }
        [Required]
        public string Author { get; set; }
        [Required]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:MM/dd/yyyy}")]
        public DateTime Date { get; set; }
        public bool Approval { get; set; }
        [Required]
        public string Description { get; set; }
        [Required]
        [Display(Name="YouTube Link")]
        public string Link { get; set; }      
        [Display(Name = "Rating")]
        public decimal Rating { get; set; }       
        [Display(Name = "Destination")]
        [ForeignKey(nameof(Dulwainfo))]
        public Guid? DulwainfoId { get; set; }
        public Dulwainfo Dulwainfo { get; set; }       
        [Display(Name = "Image URL")]
        public string ImageUrl { get; set; }
        public StoryCategoryViewModel StoryCategory { get; set; }
        [Display(Name="Story Category")]
        public Guid StoryCategoryId { get; set; }
        public string UserProfile { get; set; }
        [Display(Name = "Cover Image")]
        public IFormFile CoverImage { get; set; }     
        [Display(Name = "User")]
        [ForeignKey(nameof(User))]      
        public string UserId { get; set; }
        public ApplicationUser User { get; set; }

    }
}
