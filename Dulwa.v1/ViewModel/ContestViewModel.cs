﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Dulwa.v1.ViewModel
{
    public class ContestViewModel
    {
        public Guid Id { get; set; }
        [Required]
        [Display(Name = "End Date")]
        public string ImageUrl { get; set; }
        [Required]
        [Display(Name = "Author Name")]
        public string AutherName { get; set; }
        [Required]
        public string Description { get; set; }
        [Required]
        [Display(Name = "Created Date")]
        public DateTime Created_Date { get; set; }
        [Display(Name = "Contest Detail ID")]
        public Guid ContestDetailId { get; set; }
    }
}
