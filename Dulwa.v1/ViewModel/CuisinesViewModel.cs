﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Dulwa.v1.ViewModel
{
    public class CuisinesViewModel
    {
        public Guid Id { get; set; }
        public string slug { get; set; }
        public string Name { get; set; }
        public string Image { get; set; }
        [Display(Name= "Community")]
        public string Community { get; set; }
        public string Caption { get; set; }
        public string Festival { get; set; }
        public string Price { get; set; }
        public string Details { get; set; }
        public string Recipe { get; set; }
        [Display(Name= "YouTube link")]
        public string Videolink { get; set; }
        public string[] Festivals { get; set; }
        public string[] Communitys { get; set; }
        public IFormFile File { get; set; }
        [Display(Name= "Destination Associated")]
        public Guid DulwainfoId { get; set; }
    }
}
