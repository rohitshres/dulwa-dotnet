﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Dulwa.v1.ViewModel
{
    public class StoryCategoryViewModel
    {
        public Guid Id { get; set; }
        [Required]
        public string Name { get; set; }
    }
}
