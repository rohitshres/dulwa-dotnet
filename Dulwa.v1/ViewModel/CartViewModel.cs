﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Dulwa.v1.ViewModel
{
    public class CartViewModel
    {
        public Guid Id { get; set; }
        [Required]
        [Display(Name = "Cart ID")]
        public string CartId { get; set; }
        public List<CartItemViewModel> CartItem { get; set; }

    }
}
