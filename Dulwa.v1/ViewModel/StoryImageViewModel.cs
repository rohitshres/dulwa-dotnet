﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Dulwa.v1.ViewModel
{
    public class StoryImageViewModel
    {
        [Required]
        [Display(Name ="Link")]
        public string link { get; set; }

        [Required]
        [Display(Name = "Dulwa Story")]
        public Guid DulwastoryId { get; set; }
    }
}
