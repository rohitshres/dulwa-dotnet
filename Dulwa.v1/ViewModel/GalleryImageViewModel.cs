﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Dulwa.v1.ViewModel
{
    public class GalleryImageViewModel
    {
        public Guid Id { get; set; }
        [Display(Name = "Image URL")]
        public string ImageUrl { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public Guid GalleryId { get; set; }

        public IFormFile File { get; set; }
        public List<IFormFile> Files { get; set; }
    }
}
