﻿using Dulwa.v1.Entity;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Dulwa.v1.ViewModel
{
    public class DulwaInfoViewModel
    {
        public Guid Id { get; set; }
        [Required]
        public string Name { get; set; }
        
        public string slug { get; set; }
        [Required]
        [Display(Name= "Type of Destination")]
        public string[] Types { get; set; }
        [Required]
        [Display(Name= "Famous For")]
        public string[] populars { get; set; }
        [Required]
        [Display(Name = "Favorable Seasons to Travel")]
        public string[] seasons { get; set; }
        
        public string Type { get; set; }
        public string Popular { get; set; }
        public string Season { get; set; }
        public float Rating { get; set; }
        [Required]
        public string ShortDescription { get; set; }
        public string Description { get; set; }    
        public string Image { get; set; }
        [Required]
        [Display(Name = "Latitude")]
        public string Lat { get; set; }
        [Required]
        [Display(Name = "Longitude")]
        public string lon { get; set; }
        
        public string Distance { get; set; }
        public IFormFile File { get; set; }
        public List<StoryViewModel> DulwaStory { get; set; }
        public List<GalleryDetailViewModel> GalleryDetailModel { get; set; }
        public GalleryViewModel Gallery { get; set; }
    }
}
