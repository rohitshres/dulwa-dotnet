﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Dulwa.v1.ViewModel
{
    public class StoreDetailViewModel
    {
        public Guid Id { get; set; }
        [Required]
        [Display(Name ="Size")]
        public List<string> size { get; set; }
        [Required]
        [Display(Name = "Color")]
        public List<string> color { get; set; }
        [Required]
        [Display(Name = "Image URL")]
        public List<string> imageUrl { get; set; }
        [Required]
        [Display(Name = "Description")]
        public string description { get; set; }
        [Required]
        [Display(Name = "Store")]
        public Guid StoreId { get; set; }
        public StoreViewModel Store { get; set; }
    }
}
