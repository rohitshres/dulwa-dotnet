﻿using Dulwa.v1.Entity;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace Dulwa.v1.ViewModel
{
    public class PackageViewModel
    {
     
        public Guid Id { get; set; }
        [Required]
        public string Name { get; set; }
        [Required]
        public string Types { get; set; }
        [Required]
        public int Difficulty { get; set; }
        [Required]
        public string Duration { get; set; }
        [Required]
        public decimal Price { get; set; }
        [Required]
        [Display(Name="Start Date")]
        public DateTime StartDate { get; set; }
        [Required]
        [Display(Name = "End Date")]
        public DateTime EndDate { get; set; }
        [Required]
        public string Description { get; set; }
        [Required]
        [Display(Name = "Latitude")]
        public string lat { get; set; }
        [Required]
        [Display(Name = "Longitude")]
        public string lon { get; set; }   
        public decimal rating { get; set; }
       
        [Display(Name = "Image")]
        public string image { get; set; }
        [Required]
        [Display(Name = "Dulwa Info")]
        public Guid DulwainfoId { get; set; }
        [Required]
        [Display(Name = "Company")]
        public Guid CompanyId { get; set; }
        public string[] StoreCategorys { get; set; }
        public string StoreCategory { get; set; }
        public Dulwainfo Dulwainfo { get; set; }
        public Company company { get; set; }
        public IFormFile File { get; set; }
    }
}
