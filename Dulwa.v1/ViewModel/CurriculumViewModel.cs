﻿using Microsoft.AspNetCore.Http;
using System;
using System.ComponentModel.DataAnnotations;

namespace Dulwa.v1.ViewModel
{
    public class CurriculumViewModel
    {
        public Guid Id { get; set; }    
        [Display(Name = "Image")]
        public string ImageUrl { get; set; }
        [Required]
        public string Name { get; set; }
        [Required]
        public string Description { get; set; }       
        [Display(Name = "Redirect Link")]
        public string redirectLink { get; set; }
        [Display(Name = "Redirect Count")]
        public int redirectCount { get; set; }
        [Display(Name = "Aestination Associated")]
        public Guid DulwainfoId { get; set; }
        public IFormFile File { get; set; }
    }
}
