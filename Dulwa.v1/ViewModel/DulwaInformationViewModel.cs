﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Dulwa.v1.ViewModel
{
    public class DulwaInformationViewModel
    {
        public Guid Id { get; set; }
        public string TollFreeNo { get; set; }

    }
}
