﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Dulwa.v1.ViewModel
{
    public class GalleryViewModel
    {
        public Guid Id { get; set; }
        [Required]
        public string Name { get; set; }
        [Required]
        [Display(Name="Dulw Info")]
        public Guid DulwainfoId { get; set; }
        public List<GalleryImageViewModel> GalleryImage { get; set; }
        public List<GalleryVideoViewModel> GalleryVideo { get; set; }
        public List<IFormFile> ImageFiles { get; set; }
        public List<IFormFile> VideoFiles { get; set; }
    }
}
