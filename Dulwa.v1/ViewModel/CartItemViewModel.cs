﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Dulwa.v1.ViewModel
{
    public class CartItemViewModel
    {
        public Guid Id { get; set; }
        [Required]
        [Display(Name ="Cart ID")]
        public string CartId { get; set; }
        [Required]
        public int Quantity { get; set; }
        [Required]
        [Display(Name = "Store ID")]
        public Guid StoreId { get; set; }
        public decimal Total { get; set; }
        public int Items { get; set; }
        public StoreViewModel Store { get; set; }
    }
}
