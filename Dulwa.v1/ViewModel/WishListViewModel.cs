﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Dulwa.v1.ViewModel
{
    public class WishListViewModel
    {
        [Required]
        public Guid Id { get; set; }
        [Required]
        [Display(Name ="Store ID")]
        public Guid StoreId { get; set; }
        [Required]
        [Display(Name = "User ID")]
        public string UserId { get; set; }
        public StoreViewModel Store { get; set; }
    }
}
