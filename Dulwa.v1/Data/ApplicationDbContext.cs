﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using Dulwa.v1.Models;
using Dulwa.v1.Entity;
using Microsoft.AspNetCore.Identity;
using Dulwa.v1.ViewModel;

namespace Dulwa.v1.Data
{
    public class ApplicationDbContext : IdentityDbContext<ApplicationUser>
    {
        public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options)
            : base(options)
        {
        }
        public DbSet<Dulwainfo> Dulwainfo { get; set; }
        public DbSet<Route> Route { get; set; }
        public DbSet<Company> Company { get; set; }
        public DbSet<Package> Package { get; set; }
        public DbSet<StoryCategory> StoryCategory { get; set; }
        public DbSet<DulwaStory> DulwaStory { get; set; }
        public DbSet<DestinationStops> DestinationStops { get; set; }
        public DbSet<Gallery> Gallery { get; set; }
        public DbSet<GalleryImage> GalleryImage { get; set; }
        public DbSet<GalleryVideo> GalleryVideo { get; set; }
        public DbSet<StoreCategory> StoreCategory { get; set; }
        public DbSet<Store> Store { get; set; }
        public DbSet<UserPurchaseItem> UserPurchaseItem { get; set; }
        public DbSet<Cart> ShoppingCart { get; set; }
        public DbSet<CartItem> ShoppingCartItem { get; set; }
        public DbSet<Wishlist> WishList { get; set; }
        public DbSet<StoreDetail> StoreDetail { get; set; }
        public DbSet<Review> Review { get; set; }
        public DbSet<Advertisement> Advertisement { get; set; }
        public DbSet<Contest> Contest { get; set; }
        public DbSet<ContestComment> ContestComment { get; set; }
        public DbSet<ContestImage> ContestImage { get; set; }
        public DbSet<CustomPackage> CustomPackage { get; set; }
        public DbSet<Curriculum> Curriculum { get; set; }
        public DbSet<CurriculumRedirect> CurriculumRedirect { get; set; }
        public DbSet<PackageDate> PackageDate { get; set; }
        public DbSet<ContestDetail> ContestDetail { get; set; }
        public DbSet<ContestUser> ContestUser { get; set; }
        public DbSet<ContestUserLike> ContestUserLike { get; set; }
        public DbSet<Event> Event { get; set; }
        public DbSet<Cuisine> Cuisine { get; set; }
        public DbSet<Tradition_Culture> Tradition_Culture { get; set; }
        public DbSet<KeyCategory> KeyCategory { get; set; }
        public DbSet<Keys> Keys { get; set; }
        public DbSet<DulwaInformation> DulwaInformation { get; set; }
        public DbSet<TermsCondition> TermsCondition { get; set; }
        public DbSet<PrivacyPolicy> PrivacyPolicy { get; set; }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            base.OnModelCreating(builder);
            builder.Entity<ApplicationUser>().ToTable("Users");
            builder.Entity<IdentityUserRole<string>>().ToTable("UserRoles");
            builder.Entity<IdentityUserLogin<string>>().ToTable("UserLogins");
            builder.Entity<IdentityUserClaim<string>>().ToTable("UserClaims");
            builder.Entity<IdentityRole>().ToTable("Roles");
            builder.Entity<Store>()
                .HasOne(a => a.StoreDetail)
                .WithOne(b => b.Store)
                .HasForeignKey<StoreDetail>(b => b.StoreId);
            builder.Entity<Route>()
                .HasOne(a => a.Package)
                .WithMany(b => b.Route)
                .OnDelete(DeleteBehavior.Cascade);
            builder.Entity<Package>()                
                .HasOne(a => a.company)
                .WithMany(b => b.Package)                
                .OnDelete(DeleteBehavior.Cascade);
            builder.Entity<Review>()
                .HasOne(a => a.Package)
                .WithMany(b => b.Reviews)                
                .OnDelete(DeleteBehavior.Cascade);
            builder.Entity<Review>()
                .HasOne(a => a.Dulwainfo)
                .WithMany(b => b.Reviews)
                .OnDelete(DeleteBehavior.Cascade);
        }            
       
       
    }
}
