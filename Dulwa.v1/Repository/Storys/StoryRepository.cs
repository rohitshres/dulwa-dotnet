﻿using Dulwa.v1.Data;
using Dulwa.v1.Entity;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Dulwa.v1.Repository.Storys
{
    public class StoryRepository:Repository<DulwaStory>,IStoryRepository
    {
        public StoryRepository(ApplicationDbContext context):base(context)
        {

        }

        public async Task<IEnumerable<DulwaStory>> GetStoryByDulwainfoId(Guid id, int take)
        {
            return await _context.DulwaStory.Include(m => m.StoryCategory).Where(m => m.DulwainfoId == id).Where(m=>m.Approval).Take(take).ToListAsync();
        }

       

        public async Task<DulwaStory> GetDulwaStoryById(string Id)
        {
            return await _context.DulwaStory.Include(m => m.StoryCategory).Include(m=>m.User).Where(m => m.slug == Id).Where(m => m.Approval).FirstOrDefaultAsync();
        }

        public async Task<IEnumerable<DulwaStory>> GetDulwaStoryByUserId(string Id)
        {
            return await _context.DulwaStory.Include(m=>m.StoryCategory).Where(m => m.UserId == Id).Where(m => m.Approval).ToListAsync();
        }

        public async Task<IEnumerable<DulwaStory>> GetTopStory(int take)
        {
            return await _context.DulwaStory.Include(m => m.StoryCategory).Where(m => m.Rating > 4).Where(m => m.Approval).OrderBy(m => m.Created_Date).Take(take).ToListAsync();
        }
        public async Task<IEnumerable<StoryCategory>> GetAllStoryCategory()
        {
            return await _context.StoryCategory.ToListAsync();
        }

        public async Task<IEnumerable<DulwaStory>> GetAllList()
        {
            return await _context.DulwaStory.Include(m => m.Dulwainfo).Include(m => m.User).ToListAsync();
        }

        public async Task<IEnumerable<DulwaStory>> GetAllPendingStory()
        {
            return await _context.DulwaStory.Where(m => m.Approval == false).ToListAsync();
        }
    }
}
