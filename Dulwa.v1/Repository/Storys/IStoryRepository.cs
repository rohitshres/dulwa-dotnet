﻿using Dulwa.v1.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Dulwa.v1.Repository.Storys
{
    public interface IStoryRepository:IAsyncRepository<DulwaStory>,IRepository<DulwaStory>
    {
        Task<IEnumerable<DulwaStory>> GetStoryByDulwainfoId(Guid id, int take);
        Task<DulwaStory> GetDulwaStoryById(string Id);
        Task<IEnumerable<DulwaStory>> GetTopStory(int take);
        Task<IEnumerable<DulwaStory>> GetDulwaStoryByUserId(string Id);
        Task<IEnumerable<StoryCategory>> GetAllStoryCategory();
        Task<IEnumerable<DulwaStory>> GetAllList();
        Task<IEnumerable<DulwaStory>> GetAllPendingStory();
    }
}
