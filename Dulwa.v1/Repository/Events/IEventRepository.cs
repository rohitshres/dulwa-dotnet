﻿using Dulwa.v1.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Dulwa.v1.Repository.Events
{
    public interface IEventRepository:IAsyncRepository<Event>,IRepository<Event>
    {
    }
}
