﻿using Dulwa.v1.Data;
using Dulwa.v1.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Dulwa.v1.Repository.Events
{
    public class EventRepository:Repository<Event>,IEventRepository
    {
        public EventRepository(ApplicationDbContext context):base(context)
        {

        }
    }
}
