﻿using Dulwa.v1.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Dulwa.v1.Repository.Tradition_Cultures
{
   public interface ITradition_CultureRepository:IAsyncRepository<Tradition_Culture>,IRepository<Tradition_Culture>
    {
        Task<IEnumerable<Tradition_Culture>> GetTraditionDataWithDulwaId(Guid Id);
    }
}
