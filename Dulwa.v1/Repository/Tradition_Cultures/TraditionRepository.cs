﻿using Dulwa.v1.Data;
using Dulwa.v1.Entity;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Dulwa.v1.Repository.Tradition_Cultures
{
    public class TraditionRepository : Repository<Tradition_Culture>,ITradition_CultureRepository
    {
        public TraditionRepository(ApplicationDbContext context):base(context)
        {

        }


        public async  Task<IEnumerable<Tradition_Culture>> GetTraditionDataWithDulwaId(Guid Id)
        {
            return await _context.Tradition_Culture.Include(m => m.Dulwainfo).Where(m => m.DulwainfoId == Id).OrderBy(m => m.Created_Date).ToListAsync();

        }
    }
}
