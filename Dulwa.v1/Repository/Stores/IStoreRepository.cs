﻿using Dulwa.v1.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Dulwa.v1.Repository.Stores
{
   public interface IStoreRepository:IRepository<Store>,IAsyncRepository<Store>
    {
        Task<IEnumerable<StoreCategory>> GetStoreCategory();
        Task<IEnumerable<Store>> GetStoreList(Guid Id, int take, int skip);
        Task CreateStoreCategory(StoreCategory model);
        Task<Store> GetStoreById(Guid Id);
        Task CreateStoreDetail(StoreDetail model);
        Task<StoreDetail> GetStoreDetail(Guid Id);
    }
}
