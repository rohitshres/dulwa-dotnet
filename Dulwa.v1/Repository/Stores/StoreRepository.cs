﻿using Dulwa.v1.Data;
using Dulwa.v1.Entity;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Dulwa.v1.Repository.Stores
{
    public class StoreRepository:Repository<Store>,IStoreRepository
    {
        public StoreRepository(ApplicationDbContext context):base(context)
        {

        }
        public async Task<IEnumerable<StoreCategory>> GetStoreCategory()
        {
            return  await _context.StoreCategory.OrderBy(m=>m.Name).ToListAsync();
        }

        public async Task<IEnumerable<Store>> GetStoreList(Guid Id, int take,int skip)
        {
            return await _context.Store.Where(m => m.StoreCategoryId == Id).Take(take).Skip(skip).ToListAsync();
        }
        public async Task CreateStoreCategory(StoreCategory model)
        {
            await _context.StoreCategory.AddAsync(model);
            await _context.SaveChangesAsync();

        }

        public async Task<Store> GetStoreById(Guid Id)
        {
            return await _context.Store.Where(m => m.Id == Id).FirstOrDefaultAsync();
            
        }

        public async Task CreateStoreDetail(StoreDetail model)
        {
            await _context.StoreDetail.AddAsync(model);
            await _context.SaveChangesAsync();
        }

        public async Task<StoreDetail> GetStoreDetail(Guid Id)
        {
            return await _context.StoreDetail.Where(m => m.StoreId == Id).FirstOrDefaultAsync();
        }
    }
}
