﻿using Dulwa.v1.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Dulwa.v1.Repository.KeyCategories
{
    public interface IKeyCategoryRepository:IAsyncRepository<KeyCategory>,IRepository<KeyCategory>
    {
        Task<IEnumerable<Keys>> KeysList(string name);
    }
}
