﻿using Dulwa.v1.Data;
using Dulwa.v1.Entity;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Dulwa.v1.Repository.KeyCategories
{
    public class KeyCategoryRepository:Repository<KeyCategory>,IKeyCategoryRepository
    {
        public KeyCategoryRepository(ApplicationDbContext context):base(context)
        {
        }

        public async Task<IEnumerable<Keys>> KeysList(string name)
        {
            return  await _context.Keys.Where(m => m.KeyCategory.Name == name).ToListAsync();
        }
    }
}
