﻿using Dulwa.v1.Data;
using Dulwa.v1.Entity;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Dulwa.v1.Repository.Cuisines
{
    public class CuisineRepository : Repository<Cuisine>, ICuisinesRepository
    {
        public CuisineRepository(ApplicationDbContext context) : base(context)
        {

        }

        public async Task<IEnumerable<Cuisine>> GetallList()
        {
            return await _context.Cuisine.Include(m => m.Dulwainfo).ToListAsync();
        }      
        
        public async Task<IEnumerable<Cuisine>> GetCuisineByDulwaId(Guid Id)
        {
            return await _context.Cuisine.Include(m => m.Dulwainfo).Where(m => m.DulwainfoId == Id).OrderBy(m => m.Created_Date).ToListAsync();
        }
    }
}
