﻿using Dulwa.v1.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Dulwa.v1.Repository.Cuisines
{
    public interface ICuisinesRepository:IAsyncRepository<Cuisine>,IRepository<Cuisine>
    {
        Task<IEnumerable<Cuisine>> GetallList();
        Task<IEnumerable<Cuisine>> GetCuisineByDulwaId(Guid Id);


    }
}
