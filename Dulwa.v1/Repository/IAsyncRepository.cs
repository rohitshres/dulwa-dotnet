﻿using Dulwa.v1.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Dulwa.v1.Repository
{
   public interface IAsyncRepository<T>where T:BaseEntity
    {
        Task<T> GetByIdAsync(Guid? id);
        Task<List<T>> ListAllAsync();
        Task<T> AddAsync(T entity);
        Task UpdateAsync(T entity);
        Task DeleteAsync(T entity);
        Task DeleteRangeAsync(List<T> entity);
        Task AddRangeAsync(List<T> entity);
        Task<bool> CheckData(Guid Id);
        Task<T> GetFirstOrDefaultData();
    }
}
