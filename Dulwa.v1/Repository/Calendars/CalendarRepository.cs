﻿using Dulwa.v1.Data;
using Dulwa.v1.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Dulwa.v1.Repository.Calendars
{
    public class CalendarRepository:Repository<PackageDate>,ICalendarRepository
    {
        public CalendarRepository(ApplicationDbContext context):base(context)
        {

        }
    }
}
