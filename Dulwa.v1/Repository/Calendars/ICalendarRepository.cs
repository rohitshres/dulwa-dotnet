﻿using Dulwa.v1.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Dulwa.v1.Repository.Calendars
{
   public interface ICalendarRepository:IAsyncRepository<PackageDate>,IRepository<PackageDate>
    {
    }
}
