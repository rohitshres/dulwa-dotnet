﻿using Dulwa.v1.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Dulwa.v1.Repository.Advertisements
{
    public interface IAdvertisementRepository:IAsyncRepository<Advertisement>,IRepository<Advertisement>
    {
        Task<IEnumerable<Advertisement>> GetAdvertisementList(int take);
    }
}
