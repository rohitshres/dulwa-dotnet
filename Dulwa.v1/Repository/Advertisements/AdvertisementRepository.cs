﻿using Dulwa.v1.Data;
using Dulwa.v1.Entity;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Dulwa.v1.Repository.Advertisements
{
    public class AdvertisementRepository:Repository<Advertisement>,IAdvertisementRepository
    {
        public AdvertisementRepository(ApplicationDbContext context):base(context)
        {

        }

        public async Task<IEnumerable<Advertisement>> GetAdvertisementList(int take)
        {
            return await _context.Advertisement.Take(take).OrderByDescending(m => m.Created_Date).ToListAsync();
        }
    }
}
