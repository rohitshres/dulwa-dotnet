﻿using Dulwa.v1.Data;
using Dulwa.v1.Entity;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Dulwa.v1.Repository.DestinationStop
{
    public class DestinationStopsRepository:Repository<DestinationStops>,IDestinationStopsRepository
    {
        public DestinationStopsRepository(ApplicationDbContext context):base(context)
        {

        }

        public async Task<IEnumerable<DestinationStops>> GetStopsList(Guid Id)
        {
           return  await _context.DestinationStops.Where(m => m.DulwainfoId == Id).ToListAsync();
        }

        public async Task<IEnumerable<DestinationStops>> GetallData()
        {
            return await _context.DestinationStops.Include(m => m.DulwaInfo).ToListAsync();
        }
    }
}
