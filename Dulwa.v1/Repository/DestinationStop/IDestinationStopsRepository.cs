﻿using Dulwa.v1.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Dulwa.v1.Repository.DestinationStop
{
   public interface IDestinationStopsRepository:IAsyncRepository<DestinationStops>,IRepository<DestinationStops>
    {
        Task<IEnumerable<DestinationStops>> GetStopsList(Guid Id);
        Task<IEnumerable<DestinationStops>> GetallData();
    }
}
