﻿using Dulwa.v1.Data;
using Dulwa.v1.Entity;
using Dulwa.v1.ViewModel;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Dulwa.v1.Repository
{
    public class Repository<T>:IRepository<T>,IAsyncRepository<T> where T:BaseEntity
    {
        protected readonly ApplicationDbContext _context;

        public Repository(ApplicationDbContext context)
        {
            _context = context;
        }

        public T Add(T entity)
        {
            _context.Set<T>().Add(entity);
            _context.SaveChanges();
            return entity;
        }

        public async Task<T> AddAsync(T entity)
        {
            _context.Set<T>().Add(entity);
            await _context.SaveChangesAsync();
            return entity;
        }

        public async Task<bool> CheckData(Guid Id)
        {
          return await _context.Set<T>().AnyAsync(m=>m.Id==Id);
        }

        public void Delete(T entity)
        {
            _context.Set<T>().Remove(entity);
            _context.SaveChanges();
        }

        public async Task DeleteAsync(T entity)
        {
            _context.Set<T>().Remove(entity);
          await _context.SaveChangesAsync();
        }
        public async Task DeleteRangeAsync(List<T> entity)
        {
            _context.Set<T>().RemoveRange(entity);
            await _context.SaveChangesAsync();
        }

        public T GetById(Guid id)
        {
            return _context.Set<T>().Find(id);
        }

        public async Task<T> GetByIdAsync(Guid? id)
        {
            return await _context.Set<T>().Where(m=>m.Id==id).AsNoTracking().FirstOrDefaultAsync();
        }

        public IEnumerable<T> ListAll()
        {
            return _context.Set<T>().ToList();
        }

        public async Task<List<T>> ListAllAsync()
        {
            return await _context.Set<T>().ToListAsync();
        }

        public void Update(T entity)
        {
            _context.Entry(entity).State = EntityState.Modified;
            _context.SaveChanges();
        }

        public async Task UpdateAsync(T entity)
        {
            _context.Entry(entity).State = EntityState.Modified;
           await _context.SaveChangesAsync();
        }

        public async Task AddRangeAsync(List<T> entity)
        {
           await _context.Set<T>().AddRangeAsync(entity);
            await _context.SaveChangesAsync();
        }

        public async Task<T> GetFirstOrDefaultData()
        {
            return _context.Set<T>().FirstOrDefault();
        }

        public string[] Keys(string serializestring)
        {
            var deserialize = JsonConvert.DeserializeObject<KeyMapper>(serializestring);
            string[] keys = new string[deserialize.Id.Length];
            for (var i = 0; i < deserialize.Id.Length; i++)
            {               
                keys[i] = _context.Keys.Where(m => m.Id == deserialize.Id[i]).FirstOrDefault().Name;
            }
            return keys;
        }
    }
}
