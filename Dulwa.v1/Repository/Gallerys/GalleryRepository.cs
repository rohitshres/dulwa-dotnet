﻿using Dulwa.v1.Data;
using Dulwa.v1.Entity;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Dulwa.v1.Repository.Gallerys
{
    public class GalleryRepository:Repository<Gallery>,IGalleryRepository
    {
        public GalleryRepository(ApplicationDbContext context):base(context)
        {

        }
        public async Task<IEnumerable<Dulwainfo>> GetAllGallery()
        {
            return await _context.Dulwainfo.Include(m => m.Gallery).ThenInclude(m => m.GalleryImage).Include(m => m.Gallery).ThenInclude(m => m.GalleryVideo).ToListAsync();
        }

        public async Task<Gallery> GetAllGalleryWithGalleryId(Guid Id)
        {
            return await _context.Gallery.Include(m => m.GalleryImage).Include(m => m.GalleryVideo).Where(m => m.Id == Id).FirstOrDefaultAsync();
        }
        public async Task<IEnumerable<Gallery>> GetGallery(int skip, int take)
        {
            return await _context.Gallery.Include(m => m.GalleryImage).Include(m => m.GalleryVideo).OrderByDescending(m => m.Created_Date).ToListAsync();
        }
        public async Task<List<GalleryImage>> GetGalleryImage(int skip,int take)
        {
            return await _context.GalleryImage.Skip(skip).Take(take).OrderByDescending(m=>m.Created_Date).ToListAsync();
        }
        public async Task<List<GalleryVideo>> GetGalleryVideo(int skip, int take)
        {
            return await _context.GalleryVideo.Skip(skip).Take(take).OrderByDescending(m => m.Created_Date).ToListAsync();
        }

        public async Task<List<GalleryImage>> GetGalleryImagewithdulwainfoId(int skip, int take,Guid Id)
        {
            return await _context.GalleryImage.Where(m=>m.GalleryId==Id).Skip(skip).Take(take).OrderByDescending(m => m.Created_Date).ToListAsync();
        }
        public async Task<List<GalleryVideo>> GetGalleryVideowithdulwainfoId(int skip, int take,Guid Id)
        {
            return await _context.GalleryVideo.Where(m=>m.GalleryId==Id).Skip(skip).Take(take).OrderByDescending(m => m.Created_Date).ToListAsync();
        }
        public async Task<Gallery> GetGalleryByDulwainfoId(Guid Id)
        {
            return await _context.Gallery.Where(m => m.DulwainfoId == Id).FirstOrDefaultAsync();
        }
        public async Task CreateGalleryImage(GalleryImage model)
        {
           await _context.GalleryImage.AddAsync(model);
            await _context.SaveChangesAsync();
        }

        public async Task<GalleryImage> GetImageDetail(Guid? Id)
        {
            return await _context.GalleryImage.Where(m => m.Id == Id).FirstOrDefaultAsync();
        }

        public async Task DeleteGallerImage(Guid Id)
        {
            var gallery = await _context.GalleryImage.Where(m => m.Id == Id).FirstOrDefaultAsync();
            _context.GalleryImage.Remove(gallery);
            await _context.SaveChangesAsync();
        }
        public async Task UpdateImageDetail(GalleryImage model)
        {
            _context.GalleryImage.Update(model);
            await _context.SaveChangesAsync();
        }
        public async Task CreateGalleryVideo(GalleryVideo model)
        {
            await _context.GalleryVideo.AddAsync(model);
            await _context.SaveChangesAsync();
        }

        public async Task<GalleryVideo> GetVideoDetail(Guid? Id)
        {
            return await _context.GalleryVideo.Where(m => m.Id == Id).FirstOrDefaultAsync();
        }

        public async Task DeleteGallerVideo(Guid Id)
        {
            var gallery = await _context.GalleryVideo.Where(m => m.Id == Id).FirstOrDefaultAsync();
            _context.GalleryVideo.Remove(gallery);
            await _context.SaveChangesAsync();
        }
        public async Task UpdateVideoDetail(GalleryVideo model)
        {
            _context.GalleryVideo.Update(model);
            await _context.SaveChangesAsync();
        }

        
    }
}
