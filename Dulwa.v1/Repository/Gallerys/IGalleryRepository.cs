﻿using Dulwa.v1.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Dulwa.v1.Repository.Gallerys
{
    public interface IGalleryRepository:IRepository<Gallery>,IAsyncRepository<Gallery>
    {
        Task<IEnumerable<Dulwainfo>> GetAllGallery();
        Task<IEnumerable<Gallery>> GetGallery(int skip, int take);
        Task<List<GalleryImage>> GetGalleryImage(int skip, int take);
        Task<List<GalleryVideo>> GetGalleryVideo(int skip, int take);
        Task CreateGalleryVideo(GalleryVideo model);
        Task<Gallery> GetGalleryByDulwainfoId(Guid Id);
        Task<Gallery> GetAllGalleryWithGalleryId(Guid Id);
        Task CreateGalleryImage(GalleryImage model);
        Task<GalleryImage> GetImageDetail(Guid? Id);
        Task UpdateImageDetail(GalleryImage model);
        Task DeleteGallerImage(Guid Id);  
        Task<GalleryVideo> GetVideoDetail(Guid? Id);
        Task UpdateVideoDetail(GalleryVideo model);
        Task DeleteGallerVideo(Guid Id);
    }
}
