﻿using Dulwa.v1.Entity;
using Dulwa.v1.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Dulwa.v1.Repository.DulwaInfos
{
   public interface IDulwaInfoRepository:IRepository<Dulwainfo>,IAsyncRepository<Dulwainfo>
    {
        Task<IEnumerable<DulwaInfoViewModel>> NearDistance(string lat, string lon);
        Task<IEnumerable<Dulwainfo>> GetDulwaSearchInfo(string key);
        Task<Dulwainfo> GetDulwainfoWithStory(string id);
        Task<IEnumerable<Dulwainfo>> GetDulwainfoWithStory();
        Task<IEnumerable<DestinationStops>> NearAttraction(Guid Id);
        Task<Dulwainfo> GetByIdNullable(Guid? Id);
    }
}
