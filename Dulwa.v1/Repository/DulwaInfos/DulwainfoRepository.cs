﻿using Dulwa.v1.Data;
using Dulwa.v1.Entity;
using Dulwa.v1.ViewModel;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Z.EntityFramework.Plus;

namespace Dulwa.v1.Repository.DulwaInfos
{
    public class DulwainfoRepository : Repository<Dulwainfo>, IDulwaInfoRepository
    {
        public DulwainfoRepository(ApplicationDbContext context) : base(context)
        {

        }

        public async Task<Dulwainfo> GetDulwainfoWithStory(string id)
        {
            var dulwainfo = _context.Dulwainfo.Include(m=>m.Gallery).ThenInclude(m=>m.GalleryImage).Where(m => m.slug == id).OrderByDescending(m => m.Created_Date).FirstOrDefault();        
            dulwainfo.DulwaStory=_context.DulwaStory.Where(m=>m.Approval).Include(m=>m.StoryCategory).OrderByDescending(m => m.Created_Date).Where(m => m.DulwainfoId == dulwainfo.Id).Take(4).ToList();
            return dulwainfo;

        }

        public async Task<IEnumerable<Dulwainfo>> GetDulwainfoWithStory()
        {

            // return await _context.Dulwainfo.Include(m => m.DulwaStory).OrderByDescending(m => m.Created_Date).Take(4).ToListAsync();
            var dulwainfolist = new List<Dulwainfo>();
            var dulwainfo = _context.Dulwainfo.OrderByDescending(m=>m.Created_Date).ToList();
            foreach(var dulwa in dulwainfo)
            {
                var dulwainfodata = new Dulwainfo();
                dulwainfodata = dulwa;
                dulwainfodata.DulwaStory=_context.DulwaStory.OrderByDescending(m=>m.Created_Date).Where(m => m.DulwainfoId == dulwa.Id).Take(5).ToList();
                dulwainfolist.Add(dulwainfodata);                
            }

            return dulwainfolist;
        }

        public async Task<IEnumerable<Dulwainfo>> GetDulwaSearchInfo(string key)
        {
            var keywords = await _context.Dulwainfo.FromSql("Select * from Dulwainfo where CONTAINS(Description,'"+key+"')").ToListAsync();
            var name=await _context.Dulwainfo.Where(m => (m.Name.ToLower().StartsWith(key.ToLower()))).ToListAsync();
            foreach(var data in name)
            {
                if (!keywords.Contains(data))
                {
                    keywords.Add(data);
                }
            }
            return keywords;
        }

        public async Task<IEnumerable<DestinationStops>> NearAttraction(Guid Id)
        {
            return await _context.DestinationStops.Where(m => m.DulwainfoId == Id).Where(m=>m.rating>3).ToListAsync();
        }
        public async Task<Dulwainfo> GetByIdNullable(Guid? Id)
        {            
            return await _context.Dulwainfo.Where(m => m.Id == Id).FirstOrDefaultAsync();
        }
        public async Task<IEnumerable<DulwaInfoViewModel>> NearDistance(string lat, string lon)
        {
          string sql = "SELECT id,Name,lat,lon,Image,LEFT(CONVERT(VARCHAR,(geography::Point('" + lat + "', '" + lon + "', 4326).STDistance(geography::Point(ISNULL(LAT,0), ISNULL(LON, 0), 4326)))/ 1000),5) as Description from Dulwainfo WHERE (geography::Point('" + lat + "', '" + lon + "', 4326).STDistance(geography::Point(ISNULL(LAT, 0), ISNULL(LON, 0), 4326))) / 1000 > 0 and (geography::Point('" + lat + "', '" + lon + "', 4326).STDistance(geography::Point(ISNULL(LAT, 0), ISNULL(LON, 0), 4326))) / 1000 < 7";
          return await _context.Dulwainfo.FromSql(sql).Select(m=> new DulwaInfoViewModel() {
                Id=m.Id,
                Name=m.Name,
                Lat=m.Lat,
                lon=m.lon,
                Image=m.Image,
                Distance=m.Description
          }) .ToListAsync();
        }
    }
}
