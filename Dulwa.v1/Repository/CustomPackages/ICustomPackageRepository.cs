﻿using Dulwa.v1.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Dulwa.v1.Repository.CustomPackages
{
    public interface ICustomPackageRepository:IAsyncRepository<CustomPackage>,IRepository<CustomPackage>
    {
    }
}
