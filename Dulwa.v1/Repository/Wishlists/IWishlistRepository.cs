﻿using Dulwa.v1.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Dulwa.v1.Repository.Wishlists
{
    public interface IWishlistRepository:IRepository<Wishlist>,IAsyncRepository<Wishlist>
    {
        Task<Wishlist> GetWishlistByStoreId(Guid storeId, string userId);
        Task<List<Wishlist>> GetAllWishList(string userId);
    }
}
