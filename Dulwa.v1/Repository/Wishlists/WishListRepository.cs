﻿using Dulwa.v1.Data;
using Dulwa.v1.Entity;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Dulwa.v1.Repository.Wishlists
{
    public class WishListRepository:Repository<Wishlist>,IWishlistRepository
    {
        public WishListRepository(ApplicationDbContext context):base(context)
        {
        }

        public async Task<Wishlist> GetWishlistByStoreId(Guid storeId,string userId)
        {
            return await _context.WishList.Where(m => m.StoreId == storeId && m.UserId==userId).FirstOrDefaultAsync();
        }

        public async Task<List<Wishlist>> GetAllWishList(string userId)
        {
            return await _context.WishList.Include(m=>m.Store).Where(m => m.UserId == userId).ToListAsync();
        }
    }
}
