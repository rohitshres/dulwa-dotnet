﻿using System;
using System.Threading.Tasks;
using Dulwa.v1.Entity;
using System.Collections.Generic;

namespace Dulwa.v1.Repository.Routes
{
    public interface IRouteRepository : IRepository<Route>, IAsyncRepository<Route>
    {
        Task<IEnumerable<Route>> GetDulwainfoRoute(Guid id, string type);
        Task<IEnumerable<Route>> GetPackageRoute(Guid id, string type);
    }
}