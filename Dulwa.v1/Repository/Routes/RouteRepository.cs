﻿using Dulwa.v1.Data;
using Dulwa.v1.Entity;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Dulwa.v1.Repository.Routes
{
    public class RouteRepository:Repository<Route>,IRouteRepository
    {
        public RouteRepository(ApplicationDbContext context) : base(context)
        {
        }
        public async Task<IEnumerable<Route>> GetDulwainfoRoute(Guid id, string type)
        {
            return await _context.Route.Where(m => m.DulwaInfoId == id).Where(m => m.MapType == type).ToListAsync();
        }
        public async Task<IEnumerable<Route>> GetPackageRoute(Guid id, string type)
        {
            return await _context.Route.Where(m => m.PackageId == id).Where(m => m.MapType == type).ToListAsync();
        }
    }
}
