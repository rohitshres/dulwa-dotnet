﻿using Dulwa.v1.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Dulwa.v1.Repository.Curriculums
{
   public interface ICurriculumRepository:IAsyncRepository<Curriculum>,IRepository<Curriculum>
    {
        Task<IEnumerable<Curriculum>> GetCurriculumById(Guid Id);
    }
}
