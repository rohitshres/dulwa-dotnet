﻿using Dulwa.v1.Data;
using Dulwa.v1.Entity;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Dulwa.v1.Repository.Curriculums
{
    public class CurriculumRepository:Repository<Curriculum>,ICurriculumRepository
    {
        public CurriculumRepository(ApplicationDbContext context):base(context)
        {

        }
        public async Task<IEnumerable<Curriculum>> GetCurriculumById(Guid Id)
        {
            return await _context.Curriculum.Where(m => m.DulwainfoId == Id).Take(10).OrderByDescending(m=>m.Created_Date).ToListAsync();
        }
    }
}
