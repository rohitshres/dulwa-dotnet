﻿using Dulwa.v1.Data;
using Dulwa.v1.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Dulwa.v1.Repository.DulwaInformations
{
    public class DulwainfomationRepository:Repository<DulwaInformation>,IDulwainfomationRepository
    {
        public DulwainfomationRepository(ApplicationDbContext context):base(context)
        {

        }
    }
}
