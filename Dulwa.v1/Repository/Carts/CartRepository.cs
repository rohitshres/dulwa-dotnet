﻿using Dulwa.v1.Data;
using Dulwa.v1.Entity;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Dulwa.v1.ViewModel;

namespace Dulwa.v1.Repository.Carts
{
    public class CartRepository:Repository<CartItem>,ICartRepository
    {
        public const string CartSessionKey = "CartId";
        public CartRepository(ApplicationDbContext context):base(context)
        {

        }
        public async Task AddToCart(Guid id,int quantity,string shoppingcartId)
        {
            // Retrieve the product from the database.           

            var cart = _context.ShoppingCart.Where(m => m.CartId == shoppingcartId).FirstOrDefault();
            if (cart == null)
            {
               var carts = new Cart
                {
                    Id = Guid.NewGuid(),
                    CartId=shoppingcartId
                };

              var cartdata=_context.ShoppingCart.Add(carts);

                var cartitems = new CartItem
                {
                    Id = Guid.NewGuid(),
                    StoreId = id,
                    Quantity = quantity,
                    CartId = cartdata.Entity.Id
                };

                _context.ShoppingCartItem.Add(cartitems);
            }
            else
            {
                var cartItem = _context.ShoppingCartItem.Where(m => m.CartId == cart.Id && m.StoreId == id).FirstOrDefault();
                if (cartItem == null)
                {
                    var cartitems = new CartItem()
                    {
                        Id = Guid.NewGuid(),
                        Quantity = quantity,
                        StoreId = id,
                        CartId=cart.Id

                    };
                    _context.ShoppingCartItem.Add(cartitems);

                }
                else
                {
                    if (quantity > 1)
                    {
                        cartItem.Quantity = cartItem.Quantity + quantity;
                    }
                    else
                    {
                        cartItem.Quantity++;

                    }
                }
            }
          await  _context.SaveChangesAsync();
        }

        public Cart GetCartItems(string cardId)
        {
            return _context.ShoppingCart.Include(p => p.CartItem).ThenInclude(p => p.Store).Where(
                c => c.CartId == cardId).FirstOrDefault();
        }
        //public void UpdateItem(string updateCartID, int updateProductID, int quantity)
        //{
        //    using (var _db = new WingtipToys.Models.ProductContext())
        //    {
        //        try
        //        {
        //            var myItem = (from c in _db.ShoppingCartItems where c.CartId == updateCartID && c.Product.ProductID == updateProductID select c).FirstOrDefault();
        //            if (myItem != null)
        //            {
        //                myItem.Quantity = quantity;
        //                _db.SaveChanges();
        //            }
        //        }
        //        catch (Exception exp)
        //        {
        //            throw new Exception("ERROR: Unable to Update Cart Item - " + exp.Message.ToString(), exp);
        //        }
        //    }
        //}
        public decimal GetTotal(string cardId)
        {
            // Multiply product price by quantity of that product to get        
            // the current price for each of those products in the cart.  
            // Sum all product price totals to get the cart total.  
            var cart = _context.ShoppingCart.Where(m => m.CartId == cardId).FirstOrDefault();
            decimal? total = decimal.Zero;
            if (cart != null)
            {
                total = (decimal?)(from cartItems in _context.ShoppingCartItem
                                   where cardId == cart.CartId
                                   select (int?)cartItems.Quantity *
                                   cartItems.Store.price).Sum();
            }
            return total ?? decimal.Zero;
        }
        public void EmptyCart(string cardId)
        {
            var cart = _context.ShoppingCart.Where(m => m.CartId == cardId).FirstOrDefault();
            var cartItems = _context.ShoppingCartItem.Where(
                c => c.CartId == cart.Id);
            _context.ShoppingCartItem.RemoveRange(cartItems);
            // Save changes.    
            _context.ShoppingCart.Remove(cart);
            _context.SaveChanges();
        }

        public int GetCount(string cardId)
        {
            var cart = _context.ShoppingCart.Where(m => m.CartId == cardId).FirstOrDefault();
            int? count=0;
            // Get the count of each item in the cart and sum them up  
            if (cart != null)
            {
                count = (from cartItems in _context.ShoppingCartItem
                              where cartItems.CartId == cart.Id
                              select (int?)cartItems.Quantity).Sum();
            }
            // Return 0 if all entries are null         
            return count ?? 0;
        }
    }
}
