﻿using Dulwa.v1.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Dulwa.v1.Repository.Carts
{
    public interface ICartRepository:IRepository<CartItem>,IAsyncRepository<CartItem>
    {
        Task AddToCart(Guid id, int quantity,string shoppingcartId);
        Cart GetCartItems(string cardId);
        decimal GetTotal(string cardId);
        void EmptyCart(string cardId);
        int GetCount(string cardId);
    }
}
