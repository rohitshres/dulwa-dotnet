﻿using Dulwa.v1.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Dulwa.v1.Repository.Packages
{
    public interface IPackageRepository:IAsyncRepository<Package>,IRepository<Package>
    {
        Task<IEnumerable<Package>> GetTopPackage(int take);
        Task CreatePackageData(PackageDate model);
        Task<Package> GetPackageByIdNullable(Guid? Id);
    }
}
