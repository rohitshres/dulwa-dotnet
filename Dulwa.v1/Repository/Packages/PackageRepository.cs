﻿using Dulwa.v1.Data;
using Dulwa.v1.Entity;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Dulwa.v1.Repository.Packages
{
    public class PackageRepository:Repository<Package>,IPackageRepository
    {
        public PackageRepository(ApplicationDbContext context):base(context)
        {

        }              
        public async Task<IEnumerable<Package>> GetTopPackage(int take)
        {
            return await _context.Package.Where(m => m.rating > 4).OrderByDescending(m => m.Created_Date).Take(take).ToListAsync();
        }
        public async Task CreatePackageData(PackageDate model)
        {
            await _context.PackageDate.AddAsync(model);
            await _context.SaveChangesAsync();
        }

        public async Task<Package> GetPackageByIdNullable(Guid? Id)
        {
            return await _context.Package.Where(m => m.Id == Id).FirstOrDefaultAsync();
        }
    }
}
