﻿using Dulwa.v1.Data;
using Dulwa.v1.Entity;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;


namespace Dulwa.v1.Repository.Contests
{
    public class ContestRepository:Repository<ContestDetail>,IContestRepository
    {
        public ContestRepository(ApplicationDbContext context):base(context)
        {

        }

        public async Task<ContestDetail> GetSingleContest()
        {
            return await _context.ContestDetail.Where(m => m.Status == true).FirstOrDefaultAsync();
        }
        public async Task<IEnumerable<Contest>> GetContestByContestDetail(Guid Id)
        {
            return await _context.Contest.Where(m => m.ContestDetailId == Id).ToListAsync();
        }
    }
}
