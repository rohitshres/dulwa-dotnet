﻿using Dulwa.v1.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Dulwa.v1.Repository.Contests
{
    public interface IContestRepository:IAsyncRepository<ContestDetail>,IRepository<ContestDetail>
    {
        Task<ContestDetail> GetSingleContest();
        Task<IEnumerable<Contest>> GetContestByContestDetail(Guid Id);
    }
}
