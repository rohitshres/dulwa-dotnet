﻿using Dulwa.v1.Data;
using Dulwa.v1.Entity;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Dulwa.v1.Repository.Reviews
{
    public class ReviewRepository:Repository<Review>,IReviewRepository
    {
        public ReviewRepository(ApplicationDbContext context):base(context)
        {

        }

        public async Task<List<Review>> GetAllReviewByDulwaId(Guid Id)
        {
            return await _context.Review.Include(p=>p.Users)                
                .Where(m => m.DulwainfoId == Id)                
                .ToListAsync();
        }
        public async Task<List<Review>> GetAllReviewByPackageId(Guid Id)
        {
            return await _context.Review.Where(m => m.PackageId == Id).ToListAsync();
        }
    }
}
