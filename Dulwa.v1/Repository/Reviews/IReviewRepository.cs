﻿using Dulwa.v1.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Dulwa.v1.Repository.Reviews
{
    public interface IReviewRepository : IRepository<Review>, IAsyncRepository<Review>
    {
        Task<List<Review>> GetAllReviewByDulwaId(Guid Id);
        Task<List<Review>> GetAllReviewByPackageId(Guid Id);
    }
}
