﻿using Microsoft.AspNetCore.Identity;
using System.Threading.Tasks;

namespace Dulwa.v1.Configuration
{
    public  class UserRoleSeeder
    {
        private RoleManager<IdentityRole> _rolemanager;

        public UserRoleSeeder(RoleManager<IdentityRole> roleManager)
        {
            _rolemanager = roleManager;
        }

        public async  Task SeedRoles()
        {
            if (! await _rolemanager.RoleExistsAsync("Admin"))
            {
               await _rolemanager.CreateAsync(new IdentityRole { Name = "Admin" });
               await _rolemanager.CreateAsync(new IdentityRole { Name = "User" });
            }
        }
    }
}
