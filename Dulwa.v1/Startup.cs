﻿using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Dulwa.v1.Data;
using Dulwa.v1.Models;
using Dulwa.v1.Services;
using Microsoft.Extensions.FileProviders;
using System.IO;
using Microsoft.AspNetCore.Http;
using Dulwa.v1.Repository;
using AutoMapper;
using Dulwa.v1.Service.DulwaInfo;
using Dulwa.v1.Repository.DulwaInfos;
using Dulwa.v1.Repository.Routes;
using Dulwa.v1.Service.Routes;
using Dulwa.v1.Repository.Packages;
using Dulwa.v1.Service.Packages;
using Dulwa.v1.Repository.Storys;
using Dulwa.v1.Service.Storys;
using Dulwa.v1.Repository.Gallerys;
using Dulwa.v1.Service.Gallerys;
using Dulwa.v1.Service.Stores;
using Dulwa.v1.Repository.Stores;
using Dulwa.v1.Service.Carts;
using Dulwa.v1.Repository.Carts;
using Microsoft.AspNetCore.Razor.TagHelpers;
using CodingBlast;
using Dulwa.v1.Service.Wishlists;
using Dulwa.v1.Repository.Wishlists;
using Dulwa.v1.Service.Reviews;
using Dulwa.v1.Repository.Reviews;
using ReflectionIT.Mvc.Paging;
using Dulwa.v1.Repository.Advertisements;
using Dulwa.v1.Service.Advertisements;
using Dulwa.v1.Repository.CustomPackages;
using Dulwa.v1.Repository.Curriculums;
using Dulwa.v1.Service.Curriculums;
using Dulwa.v1.Repository.Contests;
using Dulwa.v1.Service.Contests;
using Dulwa.v1.Repository.Calendars;
using Dulwa.v1.Service.Calendars;
using Dulwa.v1.Repository.DestinationStop;
using Dulwa.v1.Service.DestinationStop;
using Dulwa.v1.Configuration;
using Dulwa.v1.Repository.KeyCategories;
using Dulwa.v1.Service.KeyCategories;
using Dulwa.v1.Service.AI;
using Dulwa.v1.Repository.Events;
using Dulwa.v1.Repository.Tradition_Cultures;
using Dulwa.v1.Repository.Cuisines;
using Dulwa.v1.Service.Events;
using Dulwa.v1.Service.Tradition_Cultures;
using Dulwa.v1.Service.Cuisines;
using Microsoft.AspNetCore.Mvc;
using Dulwa.v1.Repository.DulwaInformations;
using Dulwa.v1.Service.Dulwainfomations;
using Microsoft.AspNetCore.Antiforgery;
using Dulwa.v1.Helper;

namespace Dulwa.v1
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddDbContext<ApplicationDbContext>(options =>
                options.UseSqlServer(Configuration.GetConnectionString("DefaultConnection")));

            services.AddIdentity<ApplicationUser, IdentityRole>()
                .AddEntityFrameworkStores<ApplicationDbContext>()
                .AddDefaultTokenProviders();
            services.AddAuthentication()
            .AddFacebook(facebookOptions =>
            {
                facebookOptions.AppId = "1789308011114500";
                facebookOptions.AppSecret = "fdd758230c0a786ce3867cb5e148f2a2";
            })
            .AddGoogle(googleOptions =>
            {
                googleOptions.ClientId ="431951689924-n1sjnhf2rl4im5v87uo1875mck499iha.apps.googleusercontent.com";
                googleOptions.ClientSecret = "Md6TwYbjRF6x-XL2qua2-FvZ";
            });

            //claim
            services.AddScoped<IUserClaimsPrincipalFactory<ApplicationUser>, ClamPrincipal>();

            //repository
            services.AddScoped(typeof(IRepository<>), typeof(Repository<>));
            services.AddScoped(typeof(IAsyncRepository<>), typeof(Repository<>));
            services.AddTransient<IDulwaInfoRepository, DulwainfoRepository>();
            services.AddTransient<IRouteRepository, RouteRepository>();
            services.AddTransient<IPackageRepository, PackageRepository>();
            services.AddTransient<IStoryRepository, StoryRepository>();
            services.AddTransient<IGalleryRepository, GalleryRepository>();
            services.AddTransient<IStoreRepository, StoreRepository>();
            services.AddTransient<ICartRepository,CartRepository>();
            services.AddTransient<IWishlistRepository,WishListRepository>();
            services.AddTransient<IReviewRepository, ReviewRepository>();
            services.AddTransient<IAdvertisementRepository, AdvertisementRepository>();
            services.AddTransient<ICustomPackageRepository, CustomPackageRepository>();
            services.AddTransient<ICurriculumRepository, CurriculumRepository>();
            services.AddTransient<IContestRepository, ContestRepository>();
            services.AddTransient<ICalendarRepository, CalendarRepository>();
            services.AddTransient<IDestinationStopsRepository, DestinationStopsRepository>();
            services.AddTransient<IKeyCategoryRepository, KeyCategoryRepository>();
            services.AddTransient<IEventRepository, EventRepository>();
            services.AddTransient<ITradition_CultureRepository, TraditionRepository>();
            services.AddTransient<ICuisinesRepository, CuisineRepository>();
            services.AddTransient<IDulwainfomationRepository, DulwainfomationRepository>();


            //service
            services.AddTransient<IDulwaInfoService, DulwaInfoService>();
            services.AddTransient<IRouteService, RouteService>();
            services.AddTransient<IPackageService, PackageService>();
            services.AddTransient<IStoryService, StoryService>();
            services.AddTransient<IGalleryService, GalleryService>();
            services.AddTransient<IStoreService, StoreService>();
            services.AddTransient<ICartService, CartService>();
            services.AddTransient<IWishListService, WishListService>();
            services.AddTransient<IReviewService, ReviewService>();
            services.AddTransient<IAdvertisementService, AdvertisementService>();
            services.AddTransient<ICurriculumService,CurriculumService>();
            services.AddTransient<IContestService,ContestService>();
            services.AddTransient<ICalendarService,CalendarService>();
            services.AddTransient<IDestinationStopService,DestinationStopService>();
            services.AddTransient<IKeyCategoryService,KeyCategoryService>();
            services.AddTransient<IEventService, EventService>();
            services.AddTransient<ITradition_CultureService, Tradition_CultureService>();
            services.AddTransient<ICuisinesService, CuisinesService>();
            services.AddTransient<IDulwainfomationService, DulwainfomationService>();


            //Ai Service

            services.AddTransient<IAIService, AIService>();

            //Add Server memory
            services.AddMemoryCache();
            // Add application services.
            services.AddTransient<IEmailSender, EmailSender>();
            services.AddTransient<IFileUpload, FileUpload>();        
            services.AddAutoMapper();
            services.AddMvc(
            options =>
            {
                options.Filters.Add(new AutoValidateAntiforgeryTokenAttribute());
            }
            );
            services.AddAntiforgery(options => options.HeaderName = "X-XSRF-TOKEN");
            services.AddSession();
            services.AddSingleton<ITagHelperComponent>(new GoogleAnalyticsTagHelperComponent("UA-114669277-1"));
            services.AddPaging();
            services.AddCors();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env, IAntiforgery antiforgery)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
                app.UseBrowserLink();
                app.UseDatabaseErrorPage();
            }
            else
            {
                app.UseExceptionHandler("/Home/Error");
            }
           // app.UseStatusCodePagesWithRedirects("/error/{0}");

            app.UseStaticFiles();
            app.UseStaticFiles(new StaticFileOptions()
            {
                ServeUnknownFileTypes = true,
                FileProvider =
               new PhysicalFileProvider(
                   Path.Combine(Directory.GetCurrentDirectory(), @"Json")),
                RequestPath = new PathString("/Json")
            });

            app.UseCors(builder =>
                builder.WithOrigins("https://api.darksky.net/")
               .AllowAnyHeader()
                );
            app.UseAuthentication();
            app.UseSession();
            app.Use(async (context, next) =>
            {
                string path = context.Request.Path.Value;
                if (path != null && !path.ToLower().Contains("/api"))
                {
                    // XSRF-TOKEN used by angular in the $http if provided
                    var tokens = antiforgery.GetAndStoreTokens(context);
                    context.Response.Cookies.Append("XSRF-TOKEN", tokens.RequestToken, new CookieOptions() { HttpOnly = false });
                }
                await next();
            });
            app.UseMvc(routes =>
            {
                routes.MapRoute(
                name: "Admin",
                template: "{area:exists}/{controller=Home}/{action=Index}/{id?}");
                routes.MapRoute(
                    name: "destination",
                    template: "destination/{id}", defaults: new { controller = "Destination", action = "Detail", Id = "{id}" });
                routes.MapRoute(
                name: "storylist",
                template: "story/list/{id}", defaults: new { controller = "Story", action = "StoryDetailList", Id = "{id}" });
                routes.MapRoute(
                name: "storydetail",
                template: "story/detail/{id}", defaults: new { controller = "Story", action = "StoryDetail", Id = "{id}" });
                routes.MapRoute(
                    name: "default",
                    template: "{controller=Home}/{action=Index}/{id?}");
            });
            
        }
    }
}
