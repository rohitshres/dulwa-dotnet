﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Dulwa.v1.Models;
using Twilio;
using Twilio.Types;
using Twilio.Rest.Api.V2010.Account;

namespace Dulwa.v1.Controllers
{
    
    public class HomeController : Controller
    {
        public IActionResult Index()
        {
            return View();
        }

        public IActionResult About()
        {
            ViewData["Message"] = "Your application description page.";

            return View();
        }

        public IActionResult Contact()
        {          

            return View();
        }
        [Route("error/{code:int}")]
        public IActionResult PageError(int code)
        {
            Response.StatusCode = code;
            return View(code);
        }

        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
