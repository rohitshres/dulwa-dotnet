﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Authorization;
using Dulwa.v1.Service.Storys;
using Dulwa.v1.ViewModel;
using Dulwa.v1.Service.Packages;
using System.Security.Claims;

namespace Dulwa.v1.Controllers
{
    public class UserController : Controller
    {
        private IStoryService _storyservice;
        private IPackageService _packageservice;

        public UserController(IStoryService storyservice, IPackageService PackageService)
        {
            _storyservice = storyservice;
            _packageservice = PackageService;
        }
        public IActionResult Index()
        {
           
            return View();
        }

        public IActionResult MyTrip()
        {
            return View();
        }
        public IActionResult Purchase()
        {
            return View();
        }
        public async Task<IActionResult> Story()
        {
            return View(await _storyservice.GetStoryByUserId(this.User.FindFirstValue(ClaimTypes.NameIdentifier)));
        }
        public IActionResult Package()
        {
            return View();
        }

        public IActionResult PackageCreate()
        {
            return View();
        }
        //[HttpPost]
        //public async IActionResult PackageCreate(CustomPackageViewModel model)
        //{
          
        //    return View();
        //}
    }
}