﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Dulwa.v1.Service.Wishlists;
using System.Security.Claims;

namespace Dulwa.v1.Controllers
{
    public class WishListController : Controller
    {
        private IWishListService _wishlist;

        public WishListController(IWishListService wishlist)
        {
            _wishlist = wishlist;
        }
        public async Task<IActionResult> Index()
        {

            return View(await _wishlist.GetAllWishList(User.FindFirstValue(ClaimTypes.NameIdentifier)));
        }
        public async Task<IActionResult> Deletewishlist(Guid Id)
        {
            await _wishlist.RemoveItemFromWishList(Id, User.FindFirstValue(ClaimTypes.NameIdentifier));
            return RedirectToAction(nameof(Index));
        }

        [Route("api/WishList/AddWishList")]
        public async Task<IActionResult> AddWishList(Guid id)
        {
            await _wishlist.AddToWishlist(id, User.FindFirstValue(ClaimTypes.NameIdentifier));
            return Ok(new { response = "Success" });

        }
    }
}