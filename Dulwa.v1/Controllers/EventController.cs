﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Dulwa.v1.Service.Events;
using Microsoft.AspNetCore.Mvc;

namespace Dulwa.v1.Controllers
{
    public class EventController : Controller
    {
        private IEventService _eventservice;

        public EventController(IEventService events)
        {
            _eventservice = events;
        }
        public async Task<IActionResult> Index()
        {
            return View(await _eventservice.GetAllEvent());
        }
    }
}