﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Dulwa.v1.Service.Packages;

namespace Dulwa.v1.Controllers
{
    public class PackageController : Controller
    {
        private IPackageService _packageservice;

        public PackageController(IPackageService packageservice)
        {
            _packageservice = packageservice;
        }
        public async Task<IActionResult> Index()
        {
            return View(await _packageservice.GetAllPackage());
        }

        public async Task<IActionResult> Detail(Guid Id,Guid Dulwaid)
        {
            ViewBag.DulwaId = Dulwaid;
            return View(await _packageservice.GetPackageDetail(Id));
        }
    }
}