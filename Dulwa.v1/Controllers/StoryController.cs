﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Dulwa.v1.Service.DulwaInfo;
using Microsoft.AspNetCore.Mvc.Rendering;
using Dulwa.v1.ViewModel;
using Microsoft.AspNetCore.Identity;
using Dulwa.v1.Models;
using Dulwa.v1.Service.Storys;
using Dulwa.v1.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using ReflectionIT.Mvc.Paging;

namespace Dulwa.v1.Controllers
{
    public class StoryController : Controller
    {
        private IDulwaInfoService _dulwainfoservice;
        private UserManager<ApplicationUser> _usermanager;
        private IStoryService _storyservice;
        private IFileUpload _fileupload;

        public StoryController(IDulwaInfoService Dulwainfoservice, UserManager<ApplicationUser> userManager, IStoryService storyservice,IFileUpload file)
        {
            _dulwainfoservice = Dulwainfoservice;
            _usermanager = userManager;
            _storyservice = storyservice;
            _fileupload = file;
        }
        public async Task<IActionResult> Index(int page=1)
        {
            var data = await _dulwainfoservice.GetDulwainfoWithStory();
            var model= PagingList.Create(data, 5, page);
            return View(model);
        }

        public async Task<IActionResult> StoryDetail(string Id)
        {
            if (Id == string.Empty)
            {
                return NotFound();
            }
            return View(await _storyservice.GetStoryDetailById(Id));
        }

        public async Task<IActionResult> StoryDetailList(string Id)
        {
            if (Id == string.Empty)
            {
                return NotFound();
            }
           
            return View(await _dulwainfoservice.GetDulwainfoWithStory(Id));
        }
        public IActionResult StoryList()
        {
            return View();
        }
        [Authorize]
        public async  Task<IActionResult> Create()
        {
            if (User.IsInRole("Admin"))
            {
                return RedirectToAction("Create", "DulwaStory", new { area = "Admin" });
            }
            ViewData["DulwainfoId"] = new SelectList(await _dulwainfoservice.getDulwaInfo(), "Id", "Name");
            ViewData["StoryCateogory"] = new SelectList(await _storyservice.GetAllStoryCategory(), "Id", "Name");           
            return View();
        }
        [HttpPost]
        public async Task<IActionResult> Create(StoryViewModel Model)
        {
            if (User.IsInRole("Admin"))
            {
                return RedirectToAction("Create", "DulwaStory", new { area = "Admin" });
            }
            Model.Author = User.Identity.Name;
            Model.UserId = _usermanager.GetUserId(User);
            Model.Approval = false;
           await _storyservice.CreateStory(Model);
            return RedirectToAction("Story","User");
        }
        [HttpPost]
        [Route("api/images/upload")]
        public async Task<IActionResult> UploadImage()
        {
          return Ok(new {location= $"/content/uploaded/{await _fileupload.Upload(Request.Form.Files[0])}"});
        }
    }
}