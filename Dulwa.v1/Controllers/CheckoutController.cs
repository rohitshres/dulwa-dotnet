﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Dulwa.v1.Service.Carts;

namespace Dulwa.v1.Controllers
{
    public class CheckoutController : Controller
    {
        private ICartService _cartservice;

        public CheckoutController(ICartService cartservice)
        {
            _cartservice = cartservice;
        }
        public IActionResult BillingAddress()
        {
            return View();
        }
        public IActionResult Payment()
        {
            
            return View();
        }
        public IActionResult Confirm()
        {
            return View();
        }
        public IActionResult Complete()
        {
            return View();
        }
    }
}