﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Dulwa.v1.Service.Gallerys;
using Dulwa.v1.Service.DulwaInfo;
using Microsoft.AspNetCore.Mvc.Rendering;

namespace Dulwa.v1.Controllers
{
    public class GalleryController : Controller
    {
        private IDulwaInfoService _dulwainfoservice;
        private IGalleryService _galleryservice;

        public GalleryController(IDulwaInfoService dulwainfoservice, IGalleryService galleryservice)
        {
            _dulwainfoservice = dulwainfoservice;
            _galleryservice = galleryservice;
        }
        public async Task<IActionResult> Index()
        {
            ViewData["DulwainfoId"] = new SelectList(await _dulwainfoservice.getDulwaInfo(), "Id", "Name");
            return View();
        }
        public IActionResult GalleryView()
        {
            return View();
        }    
    }
}