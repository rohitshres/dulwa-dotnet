﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Dulwa.v1.Service.Stores;
using Microsoft.AspNetCore.Http;
using System.Security.Claims;
using Dulwa.v1.Service.Carts;

namespace Dulwa.v1.Controllers
{
     public class StoreController : Controller
    {
        public const string CartSessionKey = "CartId";
        private IStoreService _storeservice;
        private ICartService _cartservice;

        public StoreController(IStoreService storeservice, ICartService cartserice)
        {
            _storeservice = storeservice;
            _cartservice = cartserice;
        }
        public async Task<IActionResult> Index()
        {
            return View(await _storeservice.getStoreCateogory());
        }

        public IActionResult CategoryList()
        {
            return View();
        }

        public async Task<IActionResult> ProductDetail(Guid Id)
         {
            return View(await _storeservice.GetStoreById(Id));
        }
        public async Task<IActionResult> CartDetail()
        {
            return View(await _cartservice.CartItem(GetCartId()));
        }
       
        public string GetCartId()
        {
            if (HttpContext.Session.GetString(CartSessionKey) == null)
            {
                if (!string.IsNullOrWhiteSpace(HttpContext.User.Identity.Name))
                {
                    var user = this.User.FindFirstValue(ClaimTypes.NameIdentifier);
                    HttpContext.Session.SetString(CartSessionKey,user);
                }
                else
                {
                    // Generate a new random GUID using System.Guid class.     
                    Guid tempCartId = Guid.NewGuid();
                    HttpContext.Session.SetString(CartSessionKey,tempCartId.ToString());
                }
            }
            return HttpContext.Session.GetString(CartSessionKey).ToString();
        }
        [Route("api/Store/addtocart")]
        #region Cart
        public async Task<IActionResult> AddtoCart(Guid id, int Quantity=1)
        {
            await _cartservice.AddtoCart(id, Quantity, GetCartId());
            
            return Ok(new { response = "Success" });
        }
        public JsonResult CartItem()
        {
            return Json(_cartservice.GetCount(GetCartId()));
        }

        public JsonResult CartTotal()
        {
            return Json(_cartservice.Total(GetCartId()));
        }
        [Route("api/Store/cartitemdetail")]
        public JsonResult CartItemDetail()
        {
            return Json(_cartservice.CartDetail(GetCartId()));
        }

        #endregion
    }
}