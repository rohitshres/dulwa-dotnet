﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Dulwa.v1.Service.DulwaInfo;
using Dulwa.v1.Service.Curriculums;
using Dulwa.v1.Service.Contests;

namespace Dulwa.v1.Controllers
{
    public class DestinationController : Controller
    {
        private IDulwaInfoService _dulwainfoservice;
        private ICurriculumService _curriculum;
        private IContestService _contestservice;

        public DestinationController(IDulwaInfoService dulwainfoservice,ICurriculumService curriculum,IContestService contestservice)
        {
            _dulwainfoservice = dulwainfoservice;
            _curriculum = curriculum;
            _contestservice = contestservice;
        }
       
        public async Task<IActionResult> Detail(string id)
        {
            if (id==string.Empty)
            {
                return NotFound();
            }
            var destination = await _dulwainfoservice.GetDulwainfoWithStory(id);
            ViewBag.curriculum = await _curriculum.GetCurriculumById(destination.Id);
            //ViewBag.Contest = await _contestservice.GetContest(destination.Id);
            return View(destination);
        }
    }
}