﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Dulwa.v1.Service.DulwaInfo;
using Dulwa.v1.Service.Routes;
using Dulwa.v1.Service.Packages;
using Dulwa.v1.Service.Storys;
using Dulwa.v1.Service.Gallerys;
using Dulwa.v1.Service.Stores;
using Dulwa.v1.ViewModel;
using System.Net.Http;
using System.Text;
using System.Net.Http.Headers;
using Newtonsoft.Json;
using Dulwa.v1.Service.Reviews;
using Dulwa.v1.Service.Calendars;
using Dulwa.v1.Service.DestinationStop;
using Dulwa.v1.Service.Cuisines;
using Dulwa.v1.Service.Events;
using Dulwa.v1.Service.Tradition_Cultures;
using Dulwa.v1.Service.Dulwainfomations;
using System.Security.Claims;

namespace Dulwa.v1.Controllers
{
    [Produces("application/json")]
    [Route("api/[controller]/[action]")]
    public class RestController : Controller
    {
        private IDulwaInfoService _dulwaservice;
        private IRouteService _routeservice;
        private IPackageService _packageservice;
        private IStoryService _storyservice;
        private IGalleryService _galleryservice;
        private IStoreService _storeservice;
        private IReviewService _reviewservice;
        private ICalendarService _calendarservice;
        private IDestinationStopService _destinationservice;
        private ICuisinesService _cuisinesservice;
        private IEventService _eventservice;
        private ITradition_CultureService _traditionculture;
        private IDulwainfomationService _DulwaInformationService;

        public RestController(IDulwaInfoService dulwaservice,IRouteService routeService, IPackageService packageService, IStoryService StoryService,IGalleryService galleryservice, IStoreService storeservice, IReviewService reviewService,ICalendarService calendarservice, IDestinationStopService destinationstop, ICuisinesService cuisines, IEventService events, ITradition_CultureService traditionculture, IDulwainfomationService dulwainformation)
        {
            _dulwaservice = dulwaservice;
            _routeservice = routeService;
            _packageservice = packageService;
            _storyservice = StoryService;
            _galleryservice = galleryservice;
            _storeservice = storeservice;
            _reviewservice = reviewService;
            _calendarservice = calendarservice;
            _destinationservice = destinationstop;
            _cuisinesservice = cuisines;
            _eventservice = events;
            _traditionculture = traditionculture;
            _DulwaInformationService = dulwainformation;
        }
        public async Task<JsonResult> DulwaInfoList()
        {
            return Json(await _dulwaservice.getDulwaInfo());
        }

        public async Task<JsonResult> NearDistance(string lat,string lon)
        {
            return Json(await _dulwaservice.GetNearDistance(lat,lon));
        }
        public async Task<JsonResult> NearAttraction(Guid Id)
        {
            return Json(await _dulwaservice.NearAttraction(Id));
        }

        public async Task<JsonResult> Route(Guid id,string Type) {
            return Json(await _routeservice.GetDulwainfoRoute(id,Type));
        }

        public async Task<JsonResult> RouteStops(Guid id)
        {
            return Json(await _destinationservice.GetStopsList(id));
        }

        public async Task<JsonResult> SerachDulwainfo(string key)
        {
            return Json(await _dulwaservice.GetDulwaSearchInfo(key));
        }
        public JsonResult PackageDetail(Guid id)
        {
            return Json(_packageservice.GetPackage(id));
        }

        public async Task<JsonResult> PackageRoute(Guid id)
        {
            return Json(await _routeservice.GetPackageRoute(id, "route"));
        }
        public async Task<JsonResult> StoryList(int take,Guid id)
        {
            return Json(await _storyservice.GetStoryByDulwainfoId(id, take));
        }
        public async Task<JsonResult> GalleryList(int skip,int take)
        {
            return Json(await _galleryservice.GetAllGallery(skip, take));
        }

        public async Task<JsonResult> GallerySpecific(int skip,int take,string type)
        {
            if (type == "all")
            {
                return Json(await _galleryservice.GetAllGallery(skip, take));
            }
            else if (type == "image")
            {
                return Json(await _galleryservice.GetImageGallery(skip,take));
            }
            else if (type == "video")
            {
                return Json(await _galleryservice.GetVideoGallery(skip, take));
            }
            return Json("");
        }

        public async Task<JsonResult> StoreList(Guid id, int take=8, int skip=0)
        {                        
            return Json(await _storeservice.getstorelist(id, take, skip));
        }

        [HttpPost]
        public async Task<JsonResult> Reviews([FromBody]ReviewViewModel review)
        {
            string output = string.Empty;
            var client = new HttpClient();
            var requestString = "{\"documents\":[{\"language\": \"en\",\"id\": \"1\",\"text\": \""+ review.Message+"\"}";
            // Request headers  
            client.DefaultRequestHeaders.Add("Ocp-Apim-Subscription-Key", "9e52198caa2f4333b6509c174eeb74d4");
            var uri = "https://southeastasia.api.cognitive.microsoft.com/text/analytics/v2.0/sentiment?";
            HttpResponseMessage response;
            // Request body  
            byte[] byteData = Encoding.UTF8.GetBytes(requestString);
            using (var content = new ByteArrayContent(byteData))
            {
                content.Headers.ContentType = new MediaTypeHeaderValue("application/json");
                response = await client.PostAsync(uri, content);
            }
            output = await response.Content.ReadAsStringAsync();
            //if (User.Identity.IsAuthenticated)
            //{
            //    review.UserId = User.FindFirstValue(ClaimTypes.NameIdentifier).ToString();
            //}
            await _reviewservice.ReviewAnalysis(review, output);
            return Json(JsonConvert.DeserializeObject(output));
        }

        public async Task<JsonResult> Calendar()
        {
            return Json(await _calendarservice.GetPackageDate());
        }


        public async Task<JsonResult> StuffYouMightNeed(string[] Id)
        {
           return Json(await _packageservice.GetStuff(Id));
        }

        public async Task<JsonResult> Cuisine(Guid Id)
        {
            return Json(await _cuisinesservice.GetByDulwaId(Id));
        }
        public async Task<JsonResult> Event(Guid Id)
        {
            return Json(await _eventservice.GetByDulwaId(Id));
        }
        public async Task<JsonResult> TraditionCulture(Guid Id)
        {
            return Json(await _traditionculture.GetByDulwaId(Id));
        }

        public async Task<JsonResult> ReviewList(Guid Id, string Type)
        {
            return Json(await _reviewservice.GetReviewList(Id,Type));
        }
        public async Task<JsonResult> DulwaInformation()
        {
            return Json(await _DulwaInformationService.GetSingleData());
        }
    }
}