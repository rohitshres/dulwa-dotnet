﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Dulwa.v1.Service.Contests;
using Microsoft.AspNetCore.Mvc;

namespace Dulwa.v1.Controllers
{
    public class ContestController : Controller
    {
        private IContestService _contestservice;

        public ContestController(IContestService contestService)
        {
            _contestservice = contestService;
        }
        public async Task<IActionResult> Index(Guid Id)
        {

            return View(await _contestservice.GetContestByDetailId(Id));
        }
    }
}