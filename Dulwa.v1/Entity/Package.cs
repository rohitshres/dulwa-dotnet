﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace Dulwa.v1.Entity
{
    public class Package:BaseEntity
    {
        [Required]
        public string Name { get; set; }
        [Required]
        public string Types { get; set; }
        [Required]
        public int Difficulty { get; set; }
        [Required]
        public string Duration { get; set; }
        [Required]
        public decimal Price { get; set; }
        [Required]
        [Display(Name="Start Date")]
        public DateTime StartDate { get; set; }
        [Required]
        [Display(Name = "End Date")]
        public DateTime EndDate { get; set; }
        [Required]
        [Display(Name = "Description")]
        public string Description { get; set; }
        [Display(Name = "Latitude")]
        public string lat { get; set; }
        [Display(Name = "Longitude")]
        public string lon { get; set; }
        [Display(Name = "Rating")]
        public decimal rating { get; set; }
        [Display(Name = "Image")]
        public string image { get; set; }
        [ForeignKey(nameof(Dulwainfo))]
        public Guid DulwainfoId { get; set; }
        [ForeignKey(nameof(company))]
        public Guid CompanyId { get; set; }
        public string StoreCategory { get; set; }
        public Dulwainfo Dulwainfo { get; set; }
        public Company company { get; set; }
        public List<Route> Route { get; set; }
        public List<Review> Reviews { get; set; }
    }
}
