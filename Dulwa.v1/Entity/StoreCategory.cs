﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Dulwa.v1.Entity
{
    public class StoreCategory:BaseEntity
    {
        [Required]
        public string Name { get; set; }
        [Required]
        [Display(Name = "Image URL")]
        public string ImageUrl { get; set; }
        public List<Store> Store { get; set; }
    }
}
