﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Dulwa.v1.Entity
{
    public class Store:BaseEntity
    {
        [Required]
        public string Name { get; set; }
        [Required]
        public string Description { get; set; }
        [Required]
        [Display(Name="Image URL")]
        public string ImageUrl { get; set; }
        [Required]
        [Display(Name = "Product ID")]
        public string ProductId { get; set; }
        [Required]
        public decimal price { get; set; }
        [ForeignKey(nameof(StoreCategory))]
        public Guid StoreCategoryId { get; set; }
        public StoreCategory StoreCategory { get; set; }
        public StoreDetail StoreDetail { get; set; }
    }
}
