﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Dulwa.v1.Entity
{
    public class Dulwainfo:BaseEntity
    {
        [Required]
        [Display(Name = "Name")]
        public string  Name { get; set; }
        [Required]
        public decimal Rating { get; set; }
        [Required]
        public string Type { get; set; }
        [Required]
        public string Popular { get; set; }
        [Required]
        public string Season { get; set; }
        [Required]
        public string Description { get; set; }
        [Required]
        public string ShortDescription { get; set; }
        [Required]
        [Display(Name = "Image")]
        public string Image { get; set; }
        [Required]
        [Display(Name = "Latitude")]
        public string Lat { get; set; }
        [Required]
        [Display(Name = "Longitude")]
        public string lon { get; set; }
        [Display(Name = "Trending")]
        public bool trending { get; set; }    
        public IList<DulwaStory> DulwaStory { get; set; }
        public Gallery Gallery { get; set; }
        public List<Curriculum> Curriculum { get; set; }
        public List<Route> Route { get; set; }
        public List<Review> Reviews { get; set; }
    }
   
}
