﻿using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace Dulwa.v1.Entity
{
    public class PackageDate:BaseEntity
    {
        public DateTime Date { get; set; }
        [ForeignKey(nameof(Package))]
        public Guid PackageId { get; set; }
        public Package Package { get; set; }
    }
}
