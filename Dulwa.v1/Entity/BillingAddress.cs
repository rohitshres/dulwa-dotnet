﻿using System.ComponentModel.DataAnnotations;

namespace Dulwa.v1.Entity
{
    public class BillingAddress:BaseEntity
    {
        [Required]
        [Display(Name ="Company")]
        public string Company { get; set; }
        [Required]
        [Display(Name = "First Name")]
        public string FirstName { get; set; }
        [Required]
        [Display(Name = "Last Name")]
        public string LastName { get; set; }
        [Required]
        [Display(Name = "Address 1")]
        public string Addressone { get; set; }
        [Display(Name = "Address 2")]
        public string Addresstwo { get; set; }
        [Required]
        public string City { get; set; }
        [Required]
        [Display(Name = "Zip/Postal Code")]
        public string Zip_postal { get; set; }
        public string Country { get; set; }
        [Required]
        [Display(Name = "State/Province")]
        public string State_province { get; set; }
        public string Email { get; set; }
        [Required]
        [Display(Name = "Phone Number")]
        public string PhoneNumber { get; set; }
        [Display(Name = "Fax Number")]
        public string Fax_number { get; set; }
    }
}
