﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace Dulwa.v1.Entity
{
    public class Event:BaseEntity
    {
        public string Name { get; set; }
        public string Organizers { get; set; }
        public DateTime Date { get; set; }
        public string Caption { get; set; }
        public string Location { get; set; }
        public string Image { get; set; }
        public string Description { get; set; }
        public bool Ticket { get; set; }
        public string Link { get; set; }
        public string Type { get; set; }
        [ForeignKey(nameof(Dulwainfo))]
        public Guid DulwainfoId { get; set; }
        public Dulwainfo Dulwainfo { get; set; }
    }
}
