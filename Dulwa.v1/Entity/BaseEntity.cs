﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Dulwa.v1.Entity
{
    public class BaseEntity
    {
        [Key]
        public Guid Id { get; set; }
        [Display(Name ="Created Date")]
        public DateTime Created_Date { get; set; } = DateTime.Now;
        [Display(Name = "Updated Date")]
        public DateTime Updated_Date { get; set; } = DateTime.Now;
        public string slug { get; set; }

    }
}
