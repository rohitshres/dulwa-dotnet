﻿using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace Dulwa.v1.Entity
{
    public class Keys:BaseEntity
    {
        public string Name { get; set; }
        [ForeignKey(nameof(KeyCategory))]
        public Guid KeyCategoryId { get; set; }
        public KeyCategory KeyCategory { get; set; }
    }
}
