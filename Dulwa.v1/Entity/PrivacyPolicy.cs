﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Dulwa.v1.Entity
{
    public class PrivacyPolicy:BaseEntity
    {
        public string Detail { get; set; }
    }
}
