﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Dulwa.v1.Entity
{
    public class Contest:BaseEntity
    {
        [Required]
        [Display(Name = "Image URL")]
        public string ImageUrl { get; set; }
        [Display(Name = "Author name")]
        [Required]
        public string AutherName { get; set; }
        [Required]
        public string Description { get; set; }
        [Required]
        [ForeignKey(nameof(ContestDetail))]
        public Guid ContestDetailId { get; set; }
        public ContestDetail ContestDetail { get; set; }
    }
}
