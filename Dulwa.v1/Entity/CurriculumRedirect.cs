﻿using System.ComponentModel.DataAnnotations;

namespace Dulwa.v1.Entity
{
    public class CurriculumRedirect:BaseEntity
    {
        [Required]
        [Display(Name ="Browser")]
        public string Browser { get; set; }
    }
}
