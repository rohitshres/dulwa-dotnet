﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Dulwa.v1.Entity
{
    public class DulwaInformation:BaseEntity
    {
        public string TollFreeNo { get; set; }
    }
}
