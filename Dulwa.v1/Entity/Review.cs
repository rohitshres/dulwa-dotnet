﻿using Dulwa.v1.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace Dulwa.v1.Entity
{
    public class Review:BaseEntity
    {
        [Required]
        public string Name { get; set; }
        public string Message { get; set; }        
        [Display(Name ="Score")]
        public decimal score { get; set; }
        [ForeignKey(nameof(Dulwainfo))]
        public Guid? DulwainfoId { get; set; }
        [ForeignKey(nameof(Package))]
        public Guid? PackageId { get; set; }        
        public Dulwainfo Dulwainfo { get; set; }
        public Package Package { get; set; }
        [ForeignKey(nameof(Users))]
        public string UserId { get; set; }
        public ApplicationUser Users { get; set;}
    }
}
