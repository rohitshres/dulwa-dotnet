﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Dulwa.v1.Entity
{
    public class GalleryVideo:BaseEntity
    {
        [Required]
        public string Title { get; set; }
        public string Description { get; set; }
       
        [Display(Name ="Video URL")]
        public string VideoUrl { get; set; }
        [ForeignKey(nameof(Gallery))]
        public Guid GalleryId { get; set; }
        public Gallery Gallery { get; set; }
    }
}
