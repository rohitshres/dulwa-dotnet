﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Dulwa.v1.Entity
{
    public class ContestDetail:BaseEntity
    {
        [Required]
        public string Title { get; set; }
        [Required]
        public string  Description { get; set; }
        [Required]
        [Display(Name = "Start Date")]
        public DateTime StartDate { get; set; }
        [Required]
        [Display(Name = "End Date")]
        public DateTime EndDate { get; set; }
        public bool Status { get; set; }
    }
}
