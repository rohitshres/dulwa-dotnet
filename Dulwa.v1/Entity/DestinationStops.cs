﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Dulwa.v1.Entity
{
    public class DestinationStops:BaseEntity
    {
        [Required]
        [Display(Name="Name")]
        public string Name { get; set; }
        [Required]
        public string Icon { get; set; }
        [Required]
        [Display(Name = "Latitude")]
        public string lat { get; set; }
        [Required]
        [Display(Name = "Longitude")]
        public string lon { get; set; }        
        [Display(Name = "Image URL")]
        public string imageUrl { get; set; }
        [Required]
        [Display(Name = "Famous for")]
        public string famousFor { get; set; }
        [Display(Name = "Expensive")]
        public string expensive { get; set; }
        [Display(Name = "Rating")]
        public decimal rating { get; set; }
        [ForeignKey(nameof(DulwaInfo))]
        public Guid DulwainfoId { get; set; }
        public Dulwainfo DulwaInfo { get; set; }
    }
}
