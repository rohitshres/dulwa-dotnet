﻿using System.ComponentModel.DataAnnotations;

namespace Dulwa.v1.Entity
{
    public class Advertisement:BaseEntity
    {
        [Required]
        [Display(Name ="Image URL")]
        public string imageUrl { get; set; }
        [Required]
        [Display(Name = "Image URL")]
        public string name { get; set; }
        [Required]
        [Display(Name = "Redirect URL")]
        public string redirectUrl { get; set; }
    }
}
