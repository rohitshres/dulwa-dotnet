﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Dulwa.v1.Entity
{
    public class ContestImage:BaseEntity
    {
        [Required]
        [Display(Name = "Image URL")]
        public string imageUrl { get; set; }
        [ForeignKey(nameof(Contest))]
        public Guid ContestId { get; set; }
        public Contest Contest { get; set; }
    }
}
