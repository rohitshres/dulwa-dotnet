﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Dulwa.v1.Entity
{
    public class Route:BaseEntity
    {
        [Required]
        [Display(Name ="Latitude")]
        public string lat { get; set; }
        [Required]
        [Display(Name = "Longitude")]
        public string lon { get; set; }
        [Required]
        [Display(Name = "Map Type")]
        public string MapType { get; set; }
        [ForeignKey(nameof(Dulwainfo))]
        public Guid? DulwaInfoId { get; set; }
        public Dulwainfo Dulwainfo { get; set; }
        [ForeignKey(nameof(Package))]
        public Guid? PackageId { get; set; }
        public Package Package { get; set; }
        [ForeignKey(nameof(DulwaStory))]
        public Guid? DulwaStoryId { get; set; }
        public DulwaStory DulwaStory { get; set; }
        [ForeignKey(nameof(CustomPackage))]
        public Guid? CustomPackageId { get; set; }
        public CustomPackage CustomPacakage { get; set; }

    }
}
