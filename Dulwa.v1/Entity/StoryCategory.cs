﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Dulwa.v1.Entity
{
    public class StoryCategory:BaseEntity
    {
        [Required]
        public string Name { get; set; }
        public List<DulwaStory> DulwaStory { get; set; }
    }
}
