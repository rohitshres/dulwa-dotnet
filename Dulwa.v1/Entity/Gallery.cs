﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Dulwa.v1.Entity
{
    public class Gallery:BaseEntity
    {
        [Required]
        public string Name { get; set; }
        [ForeignKey(nameof(Dulwainfo))]
        public Guid DulwainfoId { get; set; }
        public Dulwainfo Dulwainfo { get; set; }
        public List<GalleryImage> GalleryImage { get; set; }
        public List<GalleryVideo> GalleryVideo { get; set; }
    }
}
