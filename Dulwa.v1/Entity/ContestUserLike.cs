﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Dulwa.v1.Entity
{
    public class ContestUserLike:BaseEntity
    {
        public bool Like { get; set; }
        [ForeignKey(nameof(Contest))]
        public Guid ContestId { get; set; }
        [Display(Name ="User ID")]
        public string UserId { get; set; }
        public Contest Contest { get; set; }
    }
}
