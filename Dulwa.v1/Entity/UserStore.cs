﻿using Dulwa.v1.Models;
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Dulwa.v1.Entity
{
    public class UserStore:BaseEntity
    {
        [Display(Name = "User ID")]
        [ForeignKey(nameof(ApplicationUser))]
        public string UserId { get; set; }
        [ForeignKey(nameof(Store))]
        [Display(Name = "Store ID")]
        public Guid StoreId { get; set; }
        public ApplicationUser ApplicationUser { get; set; }
        public Store Store { get; set; }
    }
}
