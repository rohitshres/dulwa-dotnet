﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Dulwa.v1.Entity
{
    public class GalleryImage:BaseEntity
    {
        [Display(Name ="Image URL")]
        public string ImageUrl { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        [ForeignKey(nameof(Gallery))]
        public Guid GalleryId { get; set; }
        public Gallery Gallery { get; set; }
    }
}
