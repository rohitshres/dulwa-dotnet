﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Dulwa.v1.Entity
{
    public class Cart:BaseEntity
    {
        [Required]
        [Display(Name = "Cart ID")]
        public string CartId { get; set; }       
        [Display(Name = "Order ID")]
        public string OrderId { get; set; }
        public bool Payment { get; set; }
        public List<CartItem> CartItem { get; set; }
    }
}
