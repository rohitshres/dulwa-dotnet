﻿using Dulwa.v1.Models;
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Dulwa.v1.Entity
{
    public class DulwaStory:BaseEntity
    {
        [Required]
        public string Name { get; set; }
        [Required]
        public string Author { get; set; }
        [Required]
        public DateTime Date { get; set; }
        
        public string Description { get; set; }      
        [Display(Name = "Image URL")]
        public string ImageUrl { get; set; }        
        public string Link { get; set; }
        public bool Approval { get; set; }
        public decimal Rating { get; set; }
        [ForeignKey(nameof(Dulwainfo))]
        [Display(Name ="Destination")]
        public Guid? DulwainfoId { get; set; }
        public Dulwainfo Dulwainfo { get; set; }
        [ForeignKey(nameof(StoryCategory))]
        [Display(Name="Story Category")]
        public Guid StoryCategoryId { get; set; }
        public StoryCategory StoryCategory { get; set; }
        [ForeignKey(nameof(User))]
        public string UserId { get; set; }
        public ApplicationUser User { get; set; }
    }
}
