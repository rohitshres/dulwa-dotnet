﻿using Dulwa.v1.Models;
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Dulwa.v1.Entity
{
    public class UserPackage:BaseEntity
    {
        [Display(Name ="User ID")]
        [ForeignKey(nameof(ApplicationUser))]
        public string UserId { get; set; }
        [Display(Name = "Package ID")]
        [ForeignKey(nameof(Package))]
        public Guid PackageId { get; set; }
        public ApplicationUser ApplicationUser { get; set; }
        public Package Package { get; set; }
    }
}
