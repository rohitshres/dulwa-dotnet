﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace Dulwa.v1.Entity
{
    public class Cuisine:BaseEntity
    {
        public string Name { get; set; }
        public string Image { get; set; }
        public string Community { get; set; }
        public string Caption { get; set; }
        public string Festival { get; set; }
        public string Price { get; set; }
        public string Details { get; set; }
        public string Recipe { get; set; }
        public string Video { get; set; }
        [ForeignKey(nameof(Dulwainfo))]
        public Guid DulwainfoId { get; set; }
        public Dulwainfo Dulwainfo { get; set; }

    }
}
