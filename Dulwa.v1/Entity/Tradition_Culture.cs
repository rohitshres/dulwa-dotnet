﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace Dulwa.v1.Entity
{
    public class Tradition_Culture:BaseEntity
    {
        public string Name { get; set; }
        public string Image { get; set; }
        public string Caption { get; set; }
        public string Community { get; set; }
        public string Description { get; set; }
        public string NepaliDate { get; set; }
        public string Purpose { get; set; }
        [ForeignKey(nameof(Dulwainfo))]
        public Guid DulwainfoId { get; set; }
        public Dulwainfo Dulwainfo { get; set; }
    }
}
