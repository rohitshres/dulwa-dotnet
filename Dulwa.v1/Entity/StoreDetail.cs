﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Dulwa.v1.Entity
{
    public class StoreDetail:BaseEntity
    {
        [Required]
        [Display(Name = "Size")]
        public string size { get; set; }
        [Required]
        [Display(Name = "Color")]
        public string color { get; set; }
        [Required]
        [Display(Name = "Image URL")]
        public string imageUrl { get; set; }
        [Required]
        [Display(Name = "Description")]
        public string description { get; set; }
        [Required]
        [Display(Name = "Store ID")]
        public Guid  StoreId { get; set; }
        public Store Store { get; set; }
    }
}
