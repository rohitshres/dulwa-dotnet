﻿using Dulwa.v1.Models;
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Dulwa.v1.Entity
{
    public class UserPurchaseItem:BaseEntity
    {
        [Display(Name="Store ID")]
        [ForeignKey(nameof(Store))]
        public Guid StoreId { get; set; }
        [ForeignKey(nameof(User))]
        public string UserId { get; set; }
        public Store Store { get; set; }
        public ApplicationUser User { get; set; }
    }
}
