﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Dulwa.v1.Entity
{
    public class Company:BaseEntity
    {
        [Required]
        [Display(Name = "Company Name")]
        public string name { get; set; }
        [Required]
        [Display(Name = "Address")]
        public string address { get; set; }
        [Required]
        [Display(Name = "Contact")]
        public string contact { get; set; }
        [Required]
        [Display(Name = "Image")]
        public string image { get; set; }
        [Display(Name = "PAN Number")]
        public string pan_vat_number { get; set; }
        [Required]
        [Display(Name = "Contact Person Name")]
        public string ContactPersonName { get; set; }
        [Required]
        [Display(Name = "Contact Person Designation")]
        public string ContactPersonDesignation { get; set; }
        [Display(Name = "Facebook Link")]
        public string fb_Link { get; set; }
        [Display(Name = "Google Link")]
        public string google_Link { get; set; }
        [Display(Name = "Pinterest Link")]
        public string pinterest_Link { get; set; }
        [Display(Name = "Twitter Link")]
        public string twitter_Link { get; set; }
        [Display(Name = "Instagram Link")]
        public string instragram_Link { get; set; }
        [Display(Name = "Website Link")]
        public string website_Link { get; set; }
        public List<Package> Package { get; set; }
    }
}
