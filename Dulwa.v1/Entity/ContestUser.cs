﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Dulwa.v1.Entity
{
    public class ContestUser:BaseEntity
    {
        [Display(Name = "User ID")]
        public string UserId { get; set; }
        [Display(Name ="Image Status")]
        public bool ImageStatus { get; set; }
        [ForeignKey(nameof(ContestDetail))]
        public Guid ContestDetailId { get; set; }
        public ContestDetail ContestDetail { get; set; }
    }
}
