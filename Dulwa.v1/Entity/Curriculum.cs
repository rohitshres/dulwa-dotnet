﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Dulwa.v1.Entity
{
    public class Curriculum:BaseEntity
    {
        [Required]
        [Display(Name="Image URL")]
        public string ImageUrl { get; set; }
        [Required]
        public string Name { get; set; }
        [Required]
        public string Description { get; set; }
        [Required]
        [Display(Name = "Redirect Link")]
        public string redirectLink { get; set; }
        public string Price { get; set; }
        [Display(Name = "Is Local")]
        public bool IsLocal { get; set; }
        [Display(Name = "Redirect Count")]
        public int redirectCount { get; set; }
        [ForeignKey(nameof(Dulwainfo))]
        public Guid DulwainfoId { get; set; }
        public Dulwainfo Dulwainfo { get; set; }
    }
}
