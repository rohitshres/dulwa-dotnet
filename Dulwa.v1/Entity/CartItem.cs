﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Dulwa.v1.Entity
{
    public class CartItem:BaseEntity
    {
        [Required]
        public int Quantity { get; set; }
        [ForeignKey(nameof(Store))]

        [Display(Name = "Store ID")]
        public Guid StoreId { get; set; }
        [ForeignKey(nameof(Cart))]
        [Required]
        [Display(Name = "Cart ID")]
        public Guid CartId { get; set; }
        public Store Store { get; set; }
        public Cart Cart { get; set; }
    }
}
