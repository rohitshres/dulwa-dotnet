﻿using System.ComponentModel.DataAnnotations;

namespace Dulwa.v1.Entity
{
    public class CustomPackage:BaseEntity
    {
        [Required]
        [Display(Name = "Name")]
        public string Name { get; set; }
        [Required]
        [Display(Name = "Description")]
        public string Description { get; set; }
        [Required]
        [Display(Name = "Image URL")]
        public string ImageUrl { get; set; }
    }
}
