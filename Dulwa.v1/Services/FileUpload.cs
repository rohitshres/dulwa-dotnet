﻿using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using SixLabors.ImageSharp;
using SixLabors.ImageSharp.PixelFormats;
using SixLabors.ImageSharp.Processing;
using SixLabors.ImageSharp.Processing.Filters;
using SixLabors.ImageSharp.Processing.Transforms;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace Dulwa.v1.Services
{
    public class FileUpload:IFileUpload
    {
        private IHostingEnvironment _environment;
        public FileUpload(IHostingEnvironment environment)
        {
            _environment = environment;
        }


        public async Task<string> Upload(IFormFile File)
        {
            try
            {
                if (File == null || File.Length == 0)
                    return "error";

                var targetDirectory = Path.Combine(_environment.WebRootPath, string.Format("Content\\Uploaded\\"));
                var targetDirectoryImageResize = Path.Combine(_environment.WebRootPath, string.Format("Content\\Uploaded\\ImageResize\\"));
                var fileName = $"{DateTime.Today.ToString("ddMMyyyy")}-{File.FileName}";
                var file_small= $"Small-{DateTime.Today.ToString("ddMMyyyy")}-{File.FileName}";
                var file_extra_small= $"Extra-Small-{DateTime.Today.ToString("ddMMyyyy")}-{File.FileName}";
                var savePath = Path.Combine(targetDirectory, fileName);
                var savePath_small = Path.Combine(targetDirectoryImageResize, file_small);
                var savePath_extra_small = Path.Combine(targetDirectoryImageResize, file_extra_small);

                using (var stream = new FileStream(savePath, FileMode.Create))
                {
                    await File.CopyToAsync(stream);
                }
                using (Image<Rgba32> image = Image.Load(savePath))
                {
                    image.Mutate(ctx => ctx
                         .Resize(image.Width / 2, image.Height / 2)
                         );
                    image.Save(savePath_small); // Automatic encoder selected based on extension.
                }
                using (Image<Rgba32> image = Image.Load(savePath))
                {
                    image.Mutate(ctx => ctx
                         .Resize(image.Width / 4, image.Height / 4)
                         );
                    image.Save(savePath_extra_small); // Automatic encoder selected based on extension.
                }
                return fileName;
            }
            catch(Exception e)
            {
                return "error";
            }
        }
    }
}
