﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Dulwa.v1.Services
{
    public interface IFileUpload
    {
        Task<string> Upload(IFormFile File);
    }
}
