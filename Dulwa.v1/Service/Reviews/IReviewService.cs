﻿using Dulwa.v1.Entity;
using Dulwa.v1.Repository;
using Dulwa.v1.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Dulwa.v1.Service.Reviews
{
    public interface IReviewService
    {
        Task ReviewAnalysis(ReviewViewModel model, string score);
        Task<IEnumerable<ReviewViewModel>> GetReviewList(Guid Id, string Type);
        Task DeleteReviewPacakage(Guid Id);
        Task DeleteReviewDulwainfo(Guid Id);
    }
}
