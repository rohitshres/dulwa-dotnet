﻿using AutoMapper;
using Dulwa.v1.Entity;
using Dulwa.v1.Repository.DulwaInfos;
using Dulwa.v1.Repository.Packages;
using Dulwa.v1.Repository.Reviews;
using Dulwa.v1.ViewModel;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Dulwa.v1.Service.Reviews
{
    public class ReviewService:IReviewService       
    {
        private IReviewRepository _reviewrepository;
        private IMapper _mapper;
        private IDulwaInfoRepository _dulwainforepo;
        private IPackageRepository _packagerepository;

        public ReviewService(IReviewRepository reviewReposiory, IMapper mapper, IDulwaInfoRepository dulwainforepository, IPackageRepository packagerepository)
        {
            _reviewrepository = reviewReposiory;
            _mapper = mapper;
            _dulwainforepo = dulwainforepository;
            _packagerepository = packagerepository;
        }
        public async Task Create(ReviewViewModel model)
        {            
            await  _reviewrepository.AddAsync(_mapper.Map<Review>(model));
                
        }

        public async Task ReviewAnalysis(ReviewViewModel model,string score)
        {
            var json =JsonConvert.DeserializeObject<ReviewResultViewModel>(score);
            var scores=Convert.ToDecimal(json.Documents[0].score);
            model.score = scores;            
            await Create(model);
            if (model.DulwainfoId != null)
            {
                var dulwainfo =await _dulwainforepo.GetByIdNullable(model.DulwainfoId);
                if (dulwainfo != null)
                {
                   dulwainfo.Rating=dulwainfo.Rating+scores;
                   await _dulwainforepo.UpdateAsync(dulwainfo);
                }
            }
            if (model.PackageId != null)
            {
                var package = await _packagerepository.GetPackageByIdNullable(model.PackageId);
                if (package != null)
                {
                    package.rating = package.rating + scores;
                    await _packagerepository.UpdateAsync(package);
                }
            }
        }

        public async Task<IEnumerable<ReviewViewModel>> GetReviewList(Guid Id,string Type)
        {
            if (Type == "DulwaInfo")
            {
                var reviewdata = await _reviewrepository.GetAllReviewByDulwaId(Id);
                reviewdata.Select(p => new ReviewViewModel()
                {
                    Id = p.Id,
                    Name = p.Name,
                    UserProfile = p.Users.Profile,
                    Message = p.Message
                });
                return  _mapper.Map<IEnumerable<ReviewViewModel>>(reviewdata);
            }
            else if (Type == "Package")
            {
                var reviewdata = await _reviewrepository.GetAllReviewByDulwaId(Id);
                reviewdata.Select(p => new ReviewViewModel()
                {
                    Id = p.Id,
                    Name = p.Name,
                    UserProfile = p.Users.Profile,
                    Message = p.Message
                });
                return _mapper.Map<IEnumerable<ReviewViewModel>>(reviewdata);

            }

            return null;
        }

        public async Task DeleteReviewPacakage(Guid Id)
        {
            var reviews = await _reviewrepository.GetAllReviewByPackageId(Id);
            await _reviewrepository.DeleteRangeAsync(reviews);
        }
        public async Task DeleteReviewDulwainfo(Guid Id)
        {
            var reviews = await _reviewrepository.GetAllReviewByDulwaId(Id);
            await _reviewrepository.DeleteRangeAsync(reviews);
        }
    }
}
