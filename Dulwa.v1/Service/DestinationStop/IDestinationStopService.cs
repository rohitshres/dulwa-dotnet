﻿using Dulwa.v1.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Dulwa.v1.Service.DestinationStop
{
    public interface IDestinationStopService
    {
       Task Create(DestinationStopsViewModel model);
        Task Delete(Guid Id);
        Task<IEnumerable<DestinationStopsViewModel>> GetStopsList(Guid Id);
        Task Update(DestinationStopsViewModel model);
        Task<DestinationStopsViewModel> GetSingleData(Guid? Id);
        Task<bool> CheckData(Guid Id);
        Task<IEnumerable<DestinationStopsViewModel>> GetAllList();
    }
}
