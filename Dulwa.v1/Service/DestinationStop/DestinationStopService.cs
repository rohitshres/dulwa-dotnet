﻿using AutoMapper;
using Dulwa.v1.Entity;
using Dulwa.v1.Repository.DestinationStop;
using Dulwa.v1.Services;
using Dulwa.v1.ViewModel;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Dulwa.v1.Service.DestinationStop
{
    public class DestinationStopService:IDestinationStopService
    {
        private IDestinationStopsRepository _destinationstoprepo;
        private IMapper _mapper;
        private IFileUpload _file;

        public DestinationStopService(IDestinationStopsRepository destinationstoprepository, IMapper mapper,IFileUpload file)
        {
            _destinationstoprepo = destinationstoprepository;
            _mapper = mapper;
            _file = file;
        }

        public async Task Create(DestinationStopsViewModel model)
        {
            model.imageUrl = await _file.Upload(model.File);
            model.famousFor = JsonConvert.SerializeObject(model.popular);
            await _destinationstoprepo.AddAsync(_mapper.Map<DestinationStops>(model));
        }

        public async Task Delete(Guid Id)
        {
            await _destinationstoprepo.DeleteAsync(await _destinationstoprepo.GetByIdAsync(Id));
        }

        public async Task<IEnumerable<DestinationStopsViewModel>> GetStopsList(Guid Id)
        {
            return _mapper.Map<IEnumerable<DestinationStopsViewModel>>(await _destinationstoprepo.GetStopsList(Id));
        }
        public async Task<IEnumerable<DestinationStopsViewModel>> GetAllList()
        {
            return _mapper.Map<IEnumerable<DestinationStopsViewModel>>(await _destinationstoprepo.GetallData());
        }
        public async Task Update(DestinationStopsViewModel model)
        {
            if (model.File != null)
            {
                model.imageUrl = await _file.Upload(model.File);
            }
            model.famousFor = JsonConvert.SerializeObject(model.popular);
            await _destinationstoprepo.UpdateAsync(_mapper.Map<DestinationStops>(model));
        }

        public async Task<DestinationStopsViewModel> GetSingleData(Guid? Id)
        {
            return _mapper.Map<DestinationStopsViewModel>(await _destinationstoprepo.GetByIdAsync(Id));
        }

        public async Task<bool> CheckData(Guid Id)
        {
            return await _destinationstoprepo.CheckData(Id);
        }
    }
}
