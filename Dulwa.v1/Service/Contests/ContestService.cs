﻿using AutoMapper;
using Dulwa.v1.Entity;
using Dulwa.v1.Repository.Contests;
using Dulwa.v1.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Dulwa.v1.Service.Contests
{
    public class ContestService:IContestService
    {
        private IContestRepository _contestRepository;
        private IMapper _mapper;

        public ContestService(IContestRepository contestrepository, IMapper mapper)
        {
            _contestRepository = contestrepository;
            _mapper = mapper;
        }

        public async Task CreateContestDetail(ContestDetailViewModel contest)
        {
            await _contestRepository.AddAsync(_mapper.Map<ContestDetail>(contest));
        }

        public async Task<ContestDetailViewModel> GetContest()
        {
            return _mapper.Map<ContestDetailViewModel>( await _contestRepository.GetSingleContest());
        }

        public async Task<IEnumerable<ContestViewModel>> GetContestByDetailId(Guid Id)
        {
            return _mapper.Map<IEnumerable<ContestViewModel>>(await _contestRepository.GetContestByContestDetail(Id));
        }
    }
}
