﻿using Dulwa.v1.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Dulwa.v1.Service.Contests
{
   public interface IContestService
    {
        Task CreateContestDetail(ContestDetailViewModel contest);
        Task<ContestDetailViewModel> GetContest();
        Task<IEnumerable<ContestViewModel>> GetContestByDetailId(Guid Id);
    }
}
