﻿using AutoMapper;
using Dulwa.v1.Entity;
using Dulwa.v1.Helper;
using Dulwa.v1.Repository.Stores;
using Dulwa.v1.Services;
using Dulwa.v1.ViewModel;
using Microsoft.Extensions.Caching.Memory;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Dulwa.v1.Service.Stores
{
    public class StoreService:IStoreService
    {
        private IMapper _mapper;
        private IStoreRepository _storerepository;
        private IMemoryCache _cache;
        private IFileUpload _fileupload;

        public StoreService(IMemoryCache cache, IMapper mapper, IStoreRepository storerepository, IFileUpload fileupload)
        {
            _mapper = mapper;
            _storerepository = storerepository;
            _cache = cache;
            _fileupload = fileupload;
        }

        public async Task<Store> Create(StoreViewModel model)
        {
            model.ImageUrl =await _fileupload.Upload(model.File);
            model.slug= SlugGenerator.GenerateSlug(model.Name);
            return await _storerepository.AddAsync(_mapper.Map<Store>(model));
        }

        public async Task<IEnumerable<StoreCategoryViewModel>> getStoreCateogory()
        {
            return _mapper.Map<IEnumerable<StoreCategoryViewModel>>(await _storerepository.GetStoreCategory());
        }

        public async Task<IEnumerable<StoreViewModel>> getstorelist(Guid Id, int take,int skip)
        {
           return _mapper.Map<IEnumerable<StoreViewModel>>(await _storerepository.GetStoreList(Id, take, skip));
        }

        public async Task<StoreViewModel> GetStoreById(Guid Id)
        {
            
            var storemodel=_mapper.Map<StoreViewModel>(await _storerepository.GetStoreById(Id));
            storemodel.StoreDetail = await GetStoreDetail(Id);
            return storemodel;
        }
        public async Task CreateStoreCategory(StoreCategoryViewModel model)
        {
            model.ImageUrl = await _fileupload.Upload(model.File);
            await _storerepository.CreateStoreCategory(_mapper.Map<StoreCategory>(model));
        }

        public async Task CreateStoreDetail(StoreDetailViewModel model)
        {

            var testone = new StoreDetail()
            {
                imageUrl = JsonConvert.SerializeObject(model.imageUrl),
                color = JsonConvert.SerializeObject(model.color),
                size = JsonConvert.SerializeObject(model.size),
                description = model.description,
                StoreId = model.StoreId
            };
           await _storerepository.CreateStoreDetail(testone);
          
        }
        public async Task<StoreDetailViewModel> GetStoreDetail(Guid Id)
        {
            var storedata = await _storerepository.GetStoreDetail(Id);
            var storeModel = new StoreDetailViewModel()
            {
                color = JsonConvert.DeserializeObject<List<string>>(storedata.color),
                imageUrl = JsonConvert.DeserializeObject<List<string>>(storedata.imageUrl),
                size = JsonConvert.DeserializeObject<List<string>>(storedata.size),
                description = storedata.description
            };
            return storeModel;
        }
    }
}
