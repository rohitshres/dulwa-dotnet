﻿using Dulwa.v1.Entity;
using Dulwa.v1.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Dulwa.v1.Service.Stores
{
    public interface IStoreService
    {
        Task<Store> Create(StoreViewModel model);
        Task<IEnumerable<StoreCategoryViewModel>> getStoreCateogory();
        Task<IEnumerable<StoreViewModel>> getstorelist(Guid Id, int take, int skip);
        Task<StoreViewModel> GetStoreById(Guid Id);
        Task CreateStoreCategory(StoreCategoryViewModel model);
        Task CreateStoreDetail(StoreDetailViewModel model);
        Task<StoreDetailViewModel> GetStoreDetail(Guid Id);
    }
}
