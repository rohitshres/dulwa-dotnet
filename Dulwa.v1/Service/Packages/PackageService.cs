﻿using AutoMapper;
using Dulwa.v1.Entity;
using Dulwa.v1.Repository.CustomPackages;
using Dulwa.v1.Repository.Packages;
using Dulwa.v1.Repository.Routes;
using Dulwa.v1.Repository.Stores;
using Dulwa.v1.Services;
using Dulwa.v1.ViewModel;
using Microsoft.Extensions.Caching.Memory;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Dulwa.v1.Service.Packages
{
    public class PackageService:IPackageService
    {
        private readonly IMemoryCache _cache;
        private readonly IPackageRepository _package;
        private readonly IMapper _mapper;
        private readonly IFileUpload _fileupload;
        private readonly ICustomPackageRepository _custompackage;
        private readonly IRouteRepository _routepackage;
        private readonly IStoreRepository _storerepository;
        private static readonly string _dulwalist = "Dulwalist";
        private static readonly TimeSpan _defaultCacheDuration = TimeSpan.FromSeconds(30);


        public PackageService(IMemoryCache cache, IPackageRepository package, IMapper mapper, IFileUpload fileupload, ICustomPackageRepository CustomPacakgeRepository, IRouteRepository routerepository, IStoreRepository storeRepository)
        {
            _cache = cache;
            _package = package;
            _mapper = mapper;
            _fileupload = fileupload;
            _custompackage = CustomPacakgeRepository;
            _routepackage = routerepository;
            _storerepository = storeRepository;
        }
        public async Task CreatePackage(PackageViewModel model)
        {
            model.image = await _fileupload.Upload(model.File);
            model.StoreCategory = JsonConvert.SerializeObject(model.StoreCategorys);
            await _package.AddAsync(_mapper.Map<Package>(model));
        }
        public IEnumerable<PackageViewModel> GetPackage(Guid id)
        {            
                return _mapper.Map<IEnumerable<PackageViewModel>>(_package.ListAll().Where(m=>m.DulwainfoId==id).OrderByDescending(m=>m.Created_Date).Take(20));
        
        }
        public async Task<PackageViewModel> GetPackageDetail(Guid id)
        {
            return _mapper.Map<PackageViewModel>(await _package.GetByIdAsync(id));
        }

        public async Task<IEnumerable<PackageViewModel>> GetTopPackage(int take)
        {
            return _mapper.Map<IEnumerable<PackageViewModel>>(await _package.GetTopPackage(take));
        }
        public async Task<IEnumerable<PackageViewModel>> GetAllPackage()
        {
            return _mapper.Map<IEnumerable<PackageViewModel>>(await _package.ListAllAsync());
        }

        public async Task CreatePackageDate(PackageDateViewModel model)
        {
            await _package.CreatePackageData(_mapper.Map<PackageDate>(model));
        }

        public async Task CreateCustomPackage(CustomPackageViewModel model)
        {
            var custompackage = await _custompackage.AddAsync(_mapper.Map<CustomPackage>(model));
            await _routepackage.AddRangeAsync(_mapper.Map<List<Route>>(model.Route));
            
        }
        public async Task Update(PackageViewModel model)
        {
            if (model.File != null)
            {
                model.image = await _fileupload.Upload(model.File);
            }
            model.StoreCategory = JsonConvert.SerializeObject(model.StoreCategorys);           
            await _package.UpdateAsync(_mapper.Map<Package>(model));
        }

        public async Task Delete(Guid Id)
        {
            await _package.DeleteAsync(await _package.GetByIdAsync(Id));
        }

        public async Task<PackageViewModel> GetSingleData(Guid? Id)
        {
            return _mapper.Map<PackageViewModel>(await _package.GetByIdAsync(Id));
        }

        public async Task<bool> CheckData(Guid Id)
        {
            return await _package.CheckData(Id);
        }

        public async Task<IEnumerable<StoreViewModel>> GetStuff(string[] StoreCategory)
        {
            var store = new List<StoreViewModel>();
            foreach(var data in StoreCategory)
            {
                var test= await _storerepository.GetStoreList(new Guid(data), 2, 0);
                store.AddRange(_mapper.Map<List<StoreViewModel>>(test));
            }

            return store;
        }
    }
}
