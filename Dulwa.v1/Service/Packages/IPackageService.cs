﻿using Dulwa.v1.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Dulwa.v1.Service.Packages
{
    public interface IPackageService
    {
        IEnumerable<PackageViewModel> GetPackage(Guid id);
        Task<PackageViewModel> GetPackageDetail(Guid id);
        Task CreatePackage(PackageViewModel model);
        Task<IEnumerable<PackageViewModel>> GetTopPackage(int take);
        Task<IEnumerable<PackageViewModel>> GetAllPackage();
        Task CreatePackageDate(PackageDateViewModel model);
        Task CreateCustomPackage(CustomPackageViewModel model);
        Task<IEnumerable<StoreViewModel>> GetStuff(string[] StoreCategory);
        Task Update(PackageViewModel model);
        Task Delete(Guid Id);
        Task<PackageViewModel> GetSingleData(Guid? Id);
        Task<bool> CheckData(Guid Id);
    }
}
