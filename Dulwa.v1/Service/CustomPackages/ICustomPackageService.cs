﻿using Dulwa.v1.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Dulwa.v1.Service.CustomPackages
{
    public interface ICustomPackageService
    {
        Task CreateCustomPackage(CustomPackageViewModel model);
    }
}
