﻿using AutoMapper;
using Dulwa.v1.Entity;
using Dulwa.v1.Repository.CustomPackages;
using Dulwa.v1.Repository.Routes;
using Dulwa.v1.Service.Routes;
using Dulwa.v1.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Dulwa.v1.Service.CustomPackages
{
    public class CustomPackageService:ICustomPackageService
    {
        private ICustomPackageRepository _custompackage;
        private IMapper _mapper;
        private IRouteRepository _route;

        public CustomPackageService(ICustomPackageRepository custompackage, IMapper mapper,IRouteRepository route)
        {
            _custompackage = custompackage;
            _mapper = mapper;
            _route = route;
        }

        public async Task CreateCustomPackage(CustomPackageViewModel model)
        {
           var custompackage= await _custompackage.AddAsync(_mapper.Map<CustomPackage>(model));
           //await _route.AddRangeAsync(_mapper.Map<List<Route>>(model.Route.All(m=>m.CustomPackageId=custompackage.Id)));
        }

    }
}
