﻿using AutoMapper;
using Dulwa.v1.Entity;
using Dulwa.v1.Repository.DulwaInfos;
using Dulwa.v1.Repository.Gallerys;
using Dulwa.v1.ViewModel;
using Microsoft.Extensions.Caching.Memory;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Newtonsoft.Json;
using Dulwa.v1.Helper;

namespace Dulwa.v1.Service.DulwaInfo
{
    public class DulwaInfoService:IDulwaInfoService
    {
        private readonly IMemoryCache _cache;
        private readonly IDulwaInfoRepository _dulwainfo;
        private readonly IMapper _mapper;
        private readonly IGalleryRepository _galleryrepository;
        private static readonly string _dulwalist = "Dulwalist";
        private static readonly TimeSpan _defaultCacheDuration = TimeSpan.FromSeconds(30);


        public DulwaInfoService(IMemoryCache cache, IDulwaInfoRepository dulwainfoRepo,IMapper mapper, IGalleryRepository galleryrepository)
        {
            _cache = cache;
            _dulwainfo = dulwainfoRepo;
            _mapper = mapper;
            _galleryrepository = galleryrepository;
        }

        public async Task<IEnumerable<DulwaInfoViewModel>> getDulwaInfo()
        {
            return await _cache.GetOrCreateAsync(_dulwalist, async entry =>
            {
                entry.SlidingExpiration = _defaultCacheDuration;
                return _mapper.Map<IEnumerable<DulwaInfoViewModel>>(await _dulwainfo.ListAllAsync());
            });
        }

        public async Task<DulwaInfoViewModel> createDulwaInfo(DulwaInfoViewModel model)
        {
            model.Type = JsonConvert.SerializeObject(model.Types);
            model.Season = JsonConvert.SerializeObject(model.seasons);
            model.Popular = JsonConvert.SerializeObject(model.seasons);
            model.slug = SlugGenerator.GenerateSlug(model.Name);
            return _mapper.Map<DulwaInfoViewModel>(await _dulwainfo.AddAsync(_mapper.Map<Dulwainfo>(model)));
        }  
        
        public async Task EditDulwaInfo(DulwaInfoViewModel model)
        {
            model.Type = JsonConvert.SerializeObject(model.Types);
            model.Season = JsonConvert.SerializeObject(model.seasons);
            model.Popular = JsonConvert.SerializeObject(model.seasons);
            await _dulwainfo.UpdateAsync(_mapper.Map<Dulwainfo>(model));
        }

        public async Task DeleteDulwaInfo(Guid Id)
        {
            await _dulwainfo.DeleteAsync(await _dulwainfo.GetByIdAsync(Id));
        }

        public async Task<DulwaInfoViewModel> GetSingleData(Guid? Id)
        {
            return _mapper.Map<DulwaInfoViewModel>(await _dulwainfo.GetByIdAsync(Id));
        }

        public async Task<bool> CheckData(Guid Id)
        {
            return await _dulwainfo.CheckData(Id);
        }

        public async Task<IEnumerable<DulwaInfoViewModel>> GetNearDistance(string lat,string lon)
        {
            return await _dulwainfo.NearDistance(lat, lon);
        }

        public async Task<IEnumerable<DulwaInfoViewModel>> GetDulwaSearchInfo(string key)
        {
            return _mapper.Map<IEnumerable<DulwaInfoViewModel>>(await _dulwainfo.GetDulwaSearchInfo(key));
        }

        public async Task<DulwaInfoViewModel> GetDulwainfoWithStory(string id)
        {
            var dulwainfo= _mapper.Map<DulwaInfoViewModel>(await _dulwainfo.GetDulwainfoWithStory(id));
            var list = _mapper.Map<List<GalleryDetailViewModel>>(await _galleryrepository.GetGalleryVideo(0, 2));
            list.AddRange(_mapper.Map<List<GalleryDetailViewModel>>(await _galleryrepository.GetGalleryImage(0, 5)));
            list.OrderByDescending(m => m.Id);
            dulwainfo.GalleryDetailModel = list;
            return dulwainfo;
            //test.GalleryDetailModel
        }

        public async Task<IEnumerable<DestinationStopsViewModel>> NearAttraction(Guid Id)
        {
            return _mapper.Map<IEnumerable<DestinationStopsViewModel>>(await _dulwainfo.NearAttraction(Id));
        }

        public async Task<IEnumerable<DulwaInfoViewModel>> GetDulwainfoWithStory()
        {
            return _mapper.Map<IEnumerable<DulwaInfoViewModel>>(await _dulwainfo.GetDulwainfoWithStory());
        }
    }
}
