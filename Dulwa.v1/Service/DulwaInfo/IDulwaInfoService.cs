﻿using Dulwa.v1.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Dulwa.v1.Service.DulwaInfo
{
    public interface IDulwaInfoService
    {
        Task<IEnumerable<DulwaInfoViewModel>> getDulwaInfo();
        Task<DulwaInfoViewModel> createDulwaInfo(DulwaInfoViewModel model);
        Task<IEnumerable<DulwaInfoViewModel>> GetNearDistance(string lat,string lon);
        Task<IEnumerable<DulwaInfoViewModel>> GetDulwaSearchInfo(string key);
        Task<DulwaInfoViewModel> GetDulwainfoWithStory(string id);
        Task<IEnumerable<DulwaInfoViewModel>> GetDulwainfoWithStory();
        Task<IEnumerable<DestinationStopsViewModel>> NearAttraction(Guid Id);
        Task EditDulwaInfo(DulwaInfoViewModel model);
        Task DeleteDulwaInfo(Guid Id);
        Task<DulwaInfoViewModel> GetSingleData(Guid? Id);
        Task<bool> CheckData(Guid Id);
    }
}
