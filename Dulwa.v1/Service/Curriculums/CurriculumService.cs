﻿using AutoMapper;
using Dulwa.v1.Entity;
using Dulwa.v1.Repository.Curriculums;
using Dulwa.v1.Services;
using Dulwa.v1.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Dulwa.v1.Service.Curriculums
{
    public class CurriculumService:ICurriculumService
    {
        private ICurriculumRepository _currriculumRepository;
        private IMapper _mapper;
        private IFileUpload _file;

        public CurriculumService(ICurriculumRepository curriculumRepository, IMapper mapper, IFileUpload file)
        {
            _currriculumRepository = curriculumRepository;
            _mapper = mapper;
            _file = file;
        }


        
        public async Task Create(CurriculumViewModel model)
        {
            model.ImageUrl = await _file.Upload(model.File);
            await _currriculumRepository.AddAsync(_mapper.Map<Curriculum>(model));
        }
        public async Task<IEnumerable<CurriculumViewModel>> GetCurriculumById(Guid Id)
        {
            return _mapper.Map<IEnumerable<CurriculumViewModel>>(await _currriculumRepository.GetCurriculumById(Id));
        }
        public async Task<IEnumerable<CurriculumViewModel>> GetAllList()
        {
            return _mapper.Map<IEnumerable<CurriculumViewModel>>(await _currriculumRepository.ListAllAsync());
        }
        public async Task Update(CurriculumViewModel model)
        {
            if (model.File!=null)
            {
                model.ImageUrl =await _file.Upload(model.File);
            }
            await _currriculumRepository.UpdateAsync(_mapper.Map<Curriculum>(model));
        }

        public async Task Delete(Guid Id)
        {
            await _currriculumRepository.DeleteAsync(await _currriculumRepository.GetByIdAsync(Id));
        }

        public async Task<CurriculumViewModel> GetSingleData(Guid? Id)
        {
            return _mapper.Map<CurriculumViewModel>(await _currriculumRepository.GetByIdAsync(Id));
        }

        public async Task<bool> CheckData(Guid Id)
        {
            return await _currriculumRepository.CheckData(Id);
        }
    }
}
