﻿using Dulwa.v1.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Dulwa.v1.Service.Curriculums
{
   public interface ICurriculumService
    {
        Task Create(CurriculumViewModel model);
        Task<IEnumerable<CurriculumViewModel>> GetCurriculumById(Guid Id);
        Task Update(CurriculumViewModel model);
        Task Delete(Guid Id);
        Task<CurriculumViewModel> GetSingleData(Guid? Id);
        Task<bool> CheckData(Guid Id);
        Task<IEnumerable<CurriculumViewModel>> GetAllList();
    }
}
