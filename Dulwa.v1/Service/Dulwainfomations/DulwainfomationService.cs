﻿using AutoMapper;
using Dulwa.v1.Entity;
using Dulwa.v1.Repository.DulwaInformations;
using Dulwa.v1.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Dulwa.v1.Service.Dulwainfomations
{
    public class DulwainfomationService:IDulwainfomationService
    {
        private IDulwainfomationRepository _dulwainformation;
        private IMapper _mapper;

        public DulwainfomationService(IDulwainfomationRepository dulwainfomation, IMapper mapper)
        {
            _dulwainformation = dulwainfomation;
            _mapper = mapper;
        }
        public async Task Create(DulwaInformationViewModel model)
        {
            await _dulwainformation.AddAsync(_mapper.Map<DulwaInformation>(model));
        }      
        public async Task<IEnumerable<DulwaInformationViewModel>> GetAllList()
        {
            return _mapper.Map<IEnumerable<DulwaInformationViewModel>>(await _dulwainformation.ListAllAsync());
        }
        public async Task Update(DulwaInformationViewModel model)
        {
            
            await _dulwainformation.UpdateAsync(_mapper.Map<DulwaInformation>(model));
        }

        public async Task Delete(Guid Id)
        {
            await _dulwainformation.DeleteAsync(await _dulwainformation.GetByIdAsync(Id));
        }

        public async Task<DulwaInformationViewModel> GetSingleData()
        {
            return _mapper.Map<DulwaInformationViewModel>(await _dulwainformation.GetFirstOrDefaultData());
        }

        public async Task<bool> CheckData(Guid Id)
        {
            return await _dulwainformation.CheckData(Id);
        }
    }
}
