﻿using Dulwa.v1.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Dulwa.v1.Service.Dulwainfomations
{
    public interface IDulwainfomationService
    {
        Task Create(DulwaInformationViewModel model);
        Task<IEnumerable<DulwaInformationViewModel>> GetAllList();
        Task Update(DulwaInformationViewModel model);
        Task Delete(Guid Id);
        Task<DulwaInformationViewModel> GetSingleData();
        Task<bool> CheckData(Guid Id);
    }
}
