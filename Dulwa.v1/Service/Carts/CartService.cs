﻿using AutoMapper;
using Dulwa.v1.Repository.Carts;
using Dulwa.v1.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Dulwa.v1.Service.Carts
{
    public class CartService:ICartService
    {
        private ICartRepository _cartrepository;
        private IMapper _mapper;

        public CartService(ICartRepository cartRepository, IMapper mapper)
        {
            _cartrepository = cartRepository;
            _mapper = mapper;
        }

        public async Task AddtoCart(Guid id, int quantity, string shoppingcartId)
        {
           await _cartrepository.AddToCart(id, quantity, shoppingcartId);
        }
        public decimal Total(string cartId)
        {
           return _cartrepository.GetTotal(cartId);
        }

        public void EmptyCart(string cartId)
        {
            _cartrepository.EmptyCart(cartId);
        }

        public async Task<CartViewModel> CartItem(string cardId)
        {
            return _mapper.Map<CartViewModel>(_cartrepository.GetCartItems(cardId));
        }
        public CartItemViewModel CartDetail(string cartId)
        {
            var cart = new CartItemViewModel()
            {
                Items = _cartrepository.GetCount(cartId),
                Total = _cartrepository.GetTotal(cartId)
            };
            return cart;
        }
        public int GetCount(string cardId)
        {
            return _cartrepository.GetCount(cardId);
        }
       
    }
}
