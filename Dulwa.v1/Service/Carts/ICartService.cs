﻿using Dulwa.v1.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Dulwa.v1.Service.Carts
{
    public interface ICartService
    {
        Task AddtoCart(Guid id, int quantity,string shoppingcartId);
        decimal Total(string cartId);
        void EmptyCart(string cartId);
        int GetCount(string cardId);
        CartItemViewModel CartDetail(string cartId);
        Task<CartViewModel> CartItem(string cardId);
    }
}
