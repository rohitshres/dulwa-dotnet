﻿using AutoMapper;
using Dulwa.v1.Entity;
using Dulwa.v1.Repository.Events;
using Dulwa.v1.Services;
using Dulwa.v1.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Dulwa.v1.Service.Events
{
    public class EventService:IEventService
    {
        private IEventRepository _eventrepository;
        private IMapper _mapper;
        private IFileUpload _file;

        public EventService(IEventRepository eventrepository, IMapper mapper, IFileUpload file)
        {
            _eventrepository = eventrepository;
            _mapper = mapper;
            _file = file;
        }

        public async Task<IEnumerable<EventViewModel>> GetAllEvent()
        {
            return _mapper.Map<IEnumerable<EventViewModel>>(await _eventrepository.ListAllAsync());
        }

        public async Task<IEnumerable<EventViewModel>> GetByDulwaId(Guid Id)
        {
            var data =await _eventrepository.ListAllAsync();
            return _mapper.Map<IEnumerable<EventViewModel>>(data.Where(m => m.DulwainfoId == Id));
        }

        public async Task Create(EventViewModel model)
        {
            model.Image = await _file.Upload(model.File);
            await _eventrepository.AddAsync(_mapper.Map<Event>(model));
        }
        public async Task<IEnumerable<EventViewModel>> GetAllList()
        {
            return _mapper.Map<IEnumerable<EventViewModel>>(await _eventrepository.ListAllAsync());
        }
        public async Task Update(EventViewModel model)
        {
            if (model.File != null)
            {
                model.Image = await _file.Upload(model.File);
            }
            await _eventrepository.UpdateAsync(_mapper.Map<Event>(model));
        }

        public async Task Delete(Guid Id)
        {
            await _eventrepository.DeleteAsync(await _eventrepository.GetByIdAsync(Id));
        }

        public async Task<EventViewModel> GetSingleData(Guid? Id)
        {
            return _mapper.Map<EventViewModel>(await _eventrepository.GetByIdAsync(Id));
        }

        public async Task<bool> CheckData(Guid Id)
        {
            return await _eventrepository.CheckData(Id);
        }
    }
}
