﻿using Dulwa.v1.ViewModel;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Dulwa.v1.Service.Events
{
    public interface IEventService
    {
        Task<IEnumerable<EventViewModel>> GetByDulwaId(Guid Id);
        Task Create(EventViewModel model);
        Task<IEnumerable<EventViewModel>> GetAllEvent();
        Task Update(EventViewModel model);
        Task Delete(Guid Id);
        Task<EventViewModel> GetSingleData(Guid? Id);
        Task<bool> CheckData(Guid Id);
        Task<IEnumerable<EventViewModel>> GetAllList();
    }
}