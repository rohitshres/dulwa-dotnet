﻿using Dulwa.v1.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Dulwa.v1.Service.Routes
{
    public interface IRouteService
    {
        Task Create(RouteViewModel model);
        Task<IEnumerable<RouteViewModel>> GetDulwainfoRoute(Guid id, string type);
        Task<IEnumerable<RouteViewModel>> GetPackageRoute(Guid id, string type);
    }
}
