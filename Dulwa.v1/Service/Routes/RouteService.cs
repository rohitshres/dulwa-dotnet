﻿using AutoMapper;
using Dulwa.v1.Entity;
using Dulwa.v1.Repository.Routes;
using Dulwa.v1.ViewModel;
using Microsoft.Extensions.Caching.Memory;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Dulwa.v1.Service.Routes
{
    public class RouteService:IRouteService 
    {
        private readonly IMemoryCache _cache;
        private readonly IMapper _mapper;
        private readonly IRouteRepository _route;
        private static readonly string _dulwalist = "RouteList";
        private static readonly TimeSpan _defaultCacheDuration = TimeSpan.FromSeconds(30);
        public RouteService(IMemoryCache cache, IMapper mapper, IRouteRepository route)
        {
            _cache = cache;
            _mapper = mapper;
            _route = route;
        }



        public async Task Create(RouteViewModel model)
        {
            
            var routes = JsonConvert.DeserializeObject<List<LatLongViewModel>>(model.LatLong);
            var routeslist = new List<Route>();
            foreach(var route in routes)
            {
                var routedata = new Route()
                {
                    DulwaInfoId = model.DulwaInfoId,
                    lat = route.Lat,
                    lon = route.lon,
                    MapType = model.MapType,
                    PackageId=model.PackageId                    
                };
                routeslist.Add(routedata);
            }
            await _route.AddRangeAsync(routeslist);
        }

        public async Task<IEnumerable<RouteViewModel>> GetDulwainfoRoute(Guid id, string type)
        {
          return _mapper.Map<IEnumerable<RouteViewModel>>(await _route.GetDulwainfoRoute(id, type));
        }

        public async Task<IEnumerable<RouteViewModel>> GetPackageRoute(Guid id, string type)
        {
            return _mapper.Map<IEnumerable<RouteViewModel>>(await _route.GetPackageRoute(id, type));
        }

        


    }
}
