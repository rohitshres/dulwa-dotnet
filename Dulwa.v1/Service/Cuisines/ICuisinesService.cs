﻿using Dulwa.v1.ViewModel;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Dulwa.v1.Service.Cuisines
{
    public interface ICuisinesService
    {
        Task<IEnumerable<CuisinesViewModel>> GetByDulwaId(Guid Id);
        Task Create(CuisinesViewModel model);
        Task Update(CuisinesViewModel model);
        Task Delete(Guid Id);
        Task<CuisinesViewModel> GetSingleData(Guid? Id);
        Task<bool> CheckData(Guid Id);
        Task<IEnumerable<CuisinesViewModel>> GetAllList();
    }
}