﻿using AutoMapper;
using Dulwa.v1.Entity;
using Dulwa.v1.Helper;
using Dulwa.v1.Repository.Cuisines;
using Dulwa.v1.Services;
using Dulwa.v1.ViewModel;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Dulwa.v1.Service.Cuisines
{
    public class CuisinesService:ICuisinesService
    {
        private ICuisinesRepository _cuisines;
        private IMapper _mapper;
        private IFileUpload _file;

        public CuisinesService(ICuisinesRepository cuisines, IMapper mapper, IFileUpload file)
        {
            _cuisines = cuisines;
            _mapper = mapper;
            _file = file;
        }


        public async Task<IEnumerable<CuisinesViewModel>> GetByDulwaId(Guid Id)
        {
            var data = _mapper.Map<IEnumerable<CuisinesViewModel>>(await _cuisines.GetCuisineByDulwaId(Id));
            foreach (var cuisine in data)
            {
               cuisine.Communitys = _cuisines.Keys(cuisine.Community);
            }
            return data;
        }
        
        public async Task Create(CuisinesViewModel model)
        {
            model.Image = await _file.Upload(model.File);
            model.Community =$"{{'Id':{JsonConvert.SerializeObject(model.Communitys)}}}";
            model.Festival = JsonConvert.SerializeObject(model.Festivals);
            model.slug = SlugGenerator.GenerateSlug(model.Name);
            await _cuisines.AddAsync(_mapper.Map<Cuisine>(model));
        }
        public async Task<IEnumerable<CuisinesViewModel>> GetAllList()
        {
            return _mapper.Map<IEnumerable<CuisinesViewModel>>(await _cuisines.GetallList());
        }
        public async Task Update(CuisinesViewModel model)
        {
            if (model.File != null)
            {
                model.Image = await _file.Upload(model.File);
            }
            model.Community = JsonConvert.SerializeObject(model.Communitys);
            model.Festival = JsonConvert.SerializeObject(model.Festivals);
            await _cuisines.UpdateAsync(_mapper.Map<Cuisine>(model));
        }

        public async Task Delete(Guid Id)
        {
            await _cuisines.DeleteAsync(await _cuisines.GetByIdAsync(Id));
        }

        public async Task<CuisinesViewModel> GetSingleData(Guid? Id)
        {
            return _mapper.Map<CuisinesViewModel>(await _cuisines.GetByIdAsync(Id));
        }

        public async Task<bool> CheckData(Guid Id)
        {
            return await _cuisines.CheckData(Id);
        }

        
    }
}
