﻿using Dulwa.v1.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Dulwa.v1.Service.Storys
{
    public interface IStoryService
    {
        Task CreateStory(StoryViewModel Model);
        Task<IEnumerable<StoryViewModel>> GetStoryByDulwainfoId(Guid id, int take);
        Task<StoryViewModel> GetStoryDetailById(string id);
        Task<IEnumerable<StoryViewModel>> GetTopStory(int take);
        Task<IEnumerable<StoryViewModel>> GetStoryByUserId(string Id);
        Task<IEnumerable<StoryCategoryViewModel>> GetAllStoryCategory();
        Task Update(StoryViewModel model);
        Task Delete(Guid Id);
        Task<StoryViewModel> GetSingleData(Guid? Id);
        Task<bool> CheckData(Guid Id);
        Task<IEnumerable<StoryViewModel>> GetAllList();
        Task<IEnumerable<StoryViewModel>> GetAllPendingStory();
    }
}
