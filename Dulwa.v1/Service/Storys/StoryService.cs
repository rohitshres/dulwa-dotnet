﻿using AutoMapper;
using Dulwa.v1.Entity;
using Dulwa.v1.Helper;
using Dulwa.v1.Repository.Storys;
using Dulwa.v1.Services;
using Dulwa.v1.ViewModel;
using Microsoft.Extensions.Caching.Memory;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Dulwa.v1.Service.Storys
{
    public class StoryService:IStoryService
    {
        private IMemoryCache _cache;
        private IStoryRepository _storyrepository;
        private IMapper _mapper;
        private IFileUpload _file;

        public StoryService(IMemoryCache cache, IStoryRepository storyrepository, IMapper mapper, IFileUpload file)
        {
            _cache = cache;
            _storyrepository = storyrepository;
            _mapper = mapper;
            _file = file;
        }
        public async Task CreateStory(StoryViewModel Model)
        {
            Model.ImageUrl = await _file.Upload(Model.CoverImage);
            Model.slug = SlugGenerator.GenerateSlug(Model.Name);
            var story = await _storyrepository.AddAsync(_mapper.Map<DulwaStory>(Model));            
        }

        public async Task<IEnumerable<StoryViewModel>> GetStoryByDulwainfoId(Guid id, int take)
        {
            return _mapper.Map<IEnumerable<StoryViewModel>>(await _storyrepository.GetStoryByDulwainfoId(id, take));
        }

        public async Task<StoryViewModel> GetStoryDetailById(string id)
        {    
            return _mapper.Map<StoryViewModel>(await _storyrepository.GetDulwaStoryById(id));
        }

        public async Task<IEnumerable<StoryViewModel>> GetTopStory(int take)
        {
            return _mapper.Map<IEnumerable<StoryViewModel>>(await _storyrepository.GetTopStory(take));
        }
        public async Task<IEnumerable<StoryViewModel>> GetStoryByUserId(string Id)
        {
            return _mapper.Map<IEnumerable<StoryViewModel>>(await _storyrepository.GetDulwaStoryByUserId(Id));
        }

        public async Task<IEnumerable<StoryCategoryViewModel>> GetAllStoryCategory()
        {
            return _mapper.Map<IEnumerable<StoryCategoryViewModel>>(await _storyrepository.GetAllStoryCategory());
        }
        public async Task<IEnumerable<StoryViewModel>> GetAllList()
        {
            return _mapper.Map<IEnumerable<StoryViewModel>>(await _storyrepository.GetAllList());
        }
        public async Task Update(StoryViewModel model)
        {
            if (model.CoverImage != null)
            {
                model.ImageUrl = await _file.Upload(model.CoverImage);
            }
            await _storyrepository.UpdateAsync(_mapper.Map<DulwaStory>(model));
        }

        public async Task Delete(Guid Id)
        {
            await _storyrepository.DeleteAsync(await _storyrepository.GetByIdAsync(Id));
        }

        public async Task<StoryViewModel> GetSingleData(Guid? Id)
        {
            return _mapper.Map<StoryViewModel>(await _storyrepository.GetByIdAsync(Id));
        }

        public async Task<bool> CheckData(Guid Id)
        {
            return await _storyrepository.CheckData(Id);
        }
       public async Task<IEnumerable<StoryViewModel>> GetAllPendingStory()
        {
            return  _mapper.Map<IEnumerable<StoryViewModel>>(await _storyrepository.GetAllPendingStory());
        }
    }
}
