﻿using Dulwa.v1.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Dulwa.v1.Service.Calendars
{
    public interface ICalendarService
    {
        Task<IEnumerable<PackageDateViewModel>> GetPackageDate();
    }
}
