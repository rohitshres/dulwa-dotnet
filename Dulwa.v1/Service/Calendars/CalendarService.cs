﻿using AutoMapper;
using Dulwa.v1.Repository.Calendars;
using Dulwa.v1.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Dulwa.v1.Service.Calendars
{
    public class CalendarService:ICalendarService
    {
        private ICalendarRepository _calendarrepository;
        private IMapper _mapper;

        public CalendarService(ICalendarRepository calendarrepository, IMapper mapper)
        {
            _calendarrepository = calendarrepository;
            _mapper = mapper;
        }

        public async Task<IEnumerable<PackageDateViewModel>> GetPackageDate()
        {
            return _mapper.Map<IEnumerable<PackageDateViewModel>>(await _calendarrepository.ListAllAsync()); 
        }
    }
}
