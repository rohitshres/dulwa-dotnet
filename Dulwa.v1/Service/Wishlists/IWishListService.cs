﻿using Dulwa.v1.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Dulwa.v1.Service.Wishlists
{
    public interface IWishListService
    {
        Task AddToWishlist(Guid storeId, string Userid);
        Task<IEnumerable<WishListViewModel>> GetAllWishList(string Userid);
        Task RemoveItemFromWishList(Guid storeId, string userId);
        Task RemoveAllItem(string userId);
    }
}
