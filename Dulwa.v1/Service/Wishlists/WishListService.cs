﻿using AutoMapper;
using Dulwa.v1.Entity;
using Dulwa.v1.Repository.Wishlists;
using Dulwa.v1.ViewModel;
using Microsoft.Extensions.Caching.Memory;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Dulwa.v1.Service.Wishlists
{
    public class WishListService:IWishListService
    {
        private IWishlistRepository _wishlistrepository;
        private IMemoryCache _cache;
        private IMapper _mapper;

        public WishListService(IMemoryCache cache, IMapper mapper, IWishlistRepository wishlistrepository)
        {
            _wishlistrepository = wishlistrepository;
            _cache = cache;
            _mapper = mapper;
        }

        public async Task AddToWishlist(Guid storeId,string Userid)
        {

            var wishlist =await _wishlistrepository.GetWishlistByStoreId(storeId, Userid);
            if (wishlist == null)
            {
                var wishlists = new WishListViewModel()
                {
                    StoreId = storeId,
                    UserId = Userid
                };
                await _wishlistrepository.AddAsync(_mapper.Map<Wishlist>(wishlists));
            }
            else
            {
                // If the item does exist in the cart,                  
                // then add one to the quantity.                 
               //return message
            }
        }

        public async Task<IEnumerable<WishListViewModel>> GetAllWishList(string Userid)
        {
            return _mapper.Map<IEnumerable<WishListViewModel>>(await _wishlistrepository.GetAllWishList(Userid));
        }
        public async Task RemoveItemFromWishList(Guid storeId,string userId)
        {
            var wishlist =await _wishlistrepository.GetWishlistByStoreId(storeId, userId);
           await _wishlistrepository.DeleteAsync(wishlist);
        }
        public async Task RemoveAllItem(string userId)
        {
            var wishlist = await _wishlistrepository.GetAllWishList(userId);
            await _wishlistrepository.DeleteRangeAsync(wishlist);
        }
    }
}
