﻿using AutoMapper;
using Dulwa.v1.Entity;
using Dulwa.v1.Repository.Tradition_Cultures;
using Dulwa.v1.Services;
using Dulwa.v1.ViewModel;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Dulwa.v1.Service.Tradition_Cultures
{
    public class Tradition_CultureService:ITradition_CultureService
    {
        private ITradition_CultureRepository _tradition_culture;
        private IMapper _mapper;
        private IFileUpload _file;

        public Tradition_CultureService(ITradition_CultureRepository tradition_culture, IMapper mapper, IFileUpload File)
        {
            _tradition_culture = tradition_culture;
            _mapper = mapper;
            _file = File;
        }

        public async Task Create(TraditionCultureViewModel model)
        {
            model.Image = await _file.Upload(model.File);
            model.Community = JsonConvert.SerializeObject(model.Communitys);
            await _tradition_culture.AddAsync(_mapper.Map<Tradition_Culture>(model));
        }

        public async Task<IEnumerable<TraditionCultureViewModel>> GetByDulwaId(Guid Id)
        {
            var data = _mapper.Map<IEnumerable<TraditionCultureViewModel>>(await _tradition_culture.GetTraditionDataWithDulwaId(Id)); 
            foreach(var tradition in data)
            {
                tradition.Communitys = _tradition_culture.Keys(tradition.Community);
            }
            return data;
        }

        public async Task<IEnumerable<TraditionCultureViewModel>> GetAllList()
        {
            return _mapper.Map<IEnumerable<TraditionCultureViewModel>>(await _tradition_culture.ListAllAsync());
        }
        public async Task Update(TraditionCultureViewModel model)
        {
            if (model.File != null)
            {
                model.Image = await _file.Upload(model.File);
            }
            model.Community = JsonConvert.SerializeObject(model.Communitys);
            await _tradition_culture.UpdateAsync(_mapper.Map<Tradition_Culture>(model));
        }

        public async Task Delete(Guid Id)
        {
            await _tradition_culture.DeleteAsync(await _tradition_culture.GetByIdAsync(Id));
        }

        public async Task<TraditionCultureViewModel> GetSingleData(Guid? Id)
        {
            return _mapper.Map<TraditionCultureViewModel>(await _tradition_culture.GetByIdAsync(Id));
        }

        public async Task<bool> CheckData(Guid Id)
        {
            return await _tradition_culture.CheckData(Id);
        }
    }
}
