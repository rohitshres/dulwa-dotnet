﻿using Dulwa.v1.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Dulwa.v1.Service.Tradition_Cultures
{
    public interface ITradition_CultureService
    {
        Task Create(TraditionCultureViewModel model);
        Task<IEnumerable<TraditionCultureViewModel>> GetByDulwaId(Guid Id);
        Task Update(TraditionCultureViewModel model);
        Task Delete(Guid Id);
        Task<TraditionCultureViewModel> GetSingleData(Guid? Id);
        Task<bool> CheckData(Guid Id);
        Task<IEnumerable<TraditionCultureViewModel>> GetAllList();
    }
}
