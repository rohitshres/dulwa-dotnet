﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;

namespace Dulwa.v1.Service.AI
{
    public class AIService:IAIService
    {

        public async Task<string> Service(string format,string uri,string key)
        {
            string output = string.Empty;

            var client = new HttpClient();
           // var requestString = "{\"documents\":[{\"language\": \"en\",\"id\": \"1\",\"text\": \"" + review.Message + "\"}";

            // Request headers  
            client.DefaultRequestHeaders.Add("Ocp-Apim-Subscription-Key", key);
            //var uri = "https://southeastasia.api.cognitive.microsoft.com/text/analytics/v2.0/sentiment?";
            HttpResponseMessage response;
            // Request body  
            byte[] byteData = Encoding.UTF8.GetBytes(format);
            using (var content = new ByteArrayContent(byteData))
            {
                content.Headers.ContentType = new MediaTypeHeaderValue("application/json");
                response = await client.PostAsync(uri, content);
            }

           return await response.Content.ReadAsStringAsync();
        }
    }
}
