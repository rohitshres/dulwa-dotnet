﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Dulwa.v1.Service.AI
{
   public interface IAIService
    {
        Task<string> Service(string format, string uri, string key);
    }
}
