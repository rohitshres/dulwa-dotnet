﻿using Dulwa.v1.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Dulwa.v1.Service.Gallerys
{
    public interface IGalleryService
    {
        Task<IEnumerable<DulwaInfoViewModel>> GetAllGallery();
        Task<IEnumerable<GalleryDetailViewModel>> GetAllGallery(int skip, int take);
        Task<IEnumerable<GalleryViewModel>> GetGallery(int skip, int take);
        Task Create(GalleryViewModel Model);
        Task<IEnumerable<GalleryDetailViewModel>> GetImageGallery(int skip, int take);
        Task<IEnumerable<GalleryDetailViewModel>> GetVideoGallery(int skip, int take);
        Task<GalleryViewModel> GetAllGalleryWithGalleryId(Guid Id);
        Task<GalleryImageViewModel> GetGalleryImageDetail(Guid? Id);
        Task UpdateImageDetail(GalleryImageViewModel model);
        Task CreateGalleryImage(GalleryImageViewModel model);
        Task CreateGalleryVideo(GalleryVideoViewModel model);
        Task DeleteGalleryImage(Guid Id);
        Task<GalleryVideoViewModel> GetGalleryVideoDetail(Guid? Id);
        Task UpdateVideoDetail(GalleryVideoViewModel model);
        Task DeleteGalleryVideo(Guid Id);



    }
}
