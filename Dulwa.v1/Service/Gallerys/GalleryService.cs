﻿using AutoMapper;
using Dulwa.v1.Repository.Gallerys;
using Dulwa.v1.Services;
using Dulwa.v1.ViewModel;
using Dulwa.v1.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Dulwa.v1.Service.Gallerys
{
    public class GalleryService:IGalleryService
    {
        private IGalleryRepository _galleryrepository;
        private IFileUpload _file;
        private IMapper _mapper;

        public GalleryService(IGalleryRepository galleryrepository, IFileUpload file, IMapper mapper)
        {
            _galleryrepository = galleryrepository;
            _file = file;
            _mapper = mapper;
        }

        public async Task<IEnumerable<DulwaInfoViewModel>> GetAllGallery()
        {
            return _mapper.Map<IEnumerable<DulwaInfoViewModel>>(await _galleryrepository.GetAllGallery());
        }
        public async Task<GalleryViewModel> GetAllGalleryWithGalleryId(Guid Id)
        {
            return _mapper.Map<GalleryViewModel>(await _galleryrepository.GetAllGalleryWithGalleryId(Id));
        }
        public async Task<IEnumerable<GalleryViewModel>> GetGallery(int skip,int take)
        {
            return _mapper.Map<IEnumerable<GalleryViewModel>>(await _galleryrepository.GetGallery(skip,take));
        }


        public async Task<IEnumerable<GalleryDetailViewModel>> GetAllGallery(int skip,int take)
        {
           
            var list = _mapper.Map<List<GalleryDetailViewModel>>(await _galleryrepository.GetGalleryVideo(skip, take));
            list.AddRange(_mapper.Map<List<GalleryDetailViewModel>>(await _galleryrepository.GetGalleryImage(skip, take)));
            return list.OrderByDescending(m=>m.Created_Date);
        }

        public async Task<IEnumerable<GalleryDetailViewModel>> GetImageGallery(int skip,int take)
        {
            return _mapper.Map<List<GalleryDetailViewModel>>(await _galleryrepository.GetGalleryImage(skip,take));
        }
        public async Task<IEnumerable<GalleryDetailViewModel>> GetVideoGallery(int skip,int take)
        {
            return _mapper.Map<List<GalleryDetailViewModel>>(await _galleryrepository.GetGalleryVideo(skip, take));
        }
        

        public async Task Create(GalleryViewModel Model)
        {
            var gallerydata = await _galleryrepository.GetGalleryByDulwainfoId(Model.DulwainfoId);
            var id = new Guid();
            if (gallerydata == null)
            {
                var gallery = await _galleryrepository.AddAsync(_mapper.Map<Gallery>(Model));
                id = gallery.Id;
            }
            if (Model.ImageFiles != null)
            foreach (var formFile in Model.ImageFiles)
            {
                if (formFile.Length > 0)
                {
                    var filename = await _file.Upload(formFile);
                    var Image = new GalleryImageViewModel();
                    Image.ImageUrl = filename;
                        Image.GalleryId = gallerydata == null ? id : gallerydata.Id ;
                    await _galleryrepository.CreateGalleryImage(_mapper.Map<GalleryImage>(Image));
                }
            }          
        }

        public async Task CreateGalleryImage(GalleryImageViewModel model)
        {
            if (model.Files != null)
                foreach (var formFile in model.Files)
                {
                    if (formFile.Length > 0)
                    {
                        var filename = await _file.Upload(formFile);
                        var Image = new GalleryImageViewModel();
                        Image.ImageUrl = filename;
                        Image.GalleryId = model.GalleryId;
                        await _galleryrepository.CreateGalleryImage(_mapper.Map<GalleryImage>(Image));
                    }
                }
        }
        public async Task CreateGalleryVideo(GalleryVideoViewModel model)
        {
                        await _galleryrepository.CreateGalleryVideo(_mapper.Map<GalleryVideo>(model));                  
        }

        public async Task<GalleryImageViewModel> GetGalleryImageDetail(Guid? Id)
        {
            return _mapper.Map<GalleryImageViewModel>( await _galleryrepository.GetImageDetail(Id));
        }

        public async Task DeleteGalleryImage(Guid Id)
        {
            await _galleryrepository.DeleteGallerImage(Id);
        }
        public async Task UpdateImageDetail(GalleryImageViewModel model)
        {
            if (model.File != null)
            {
                model.ImageUrl = await _file.Upload(model.File);
            }
           await _galleryrepository.UpdateImageDetail(_mapper.Map<GalleryImage>(model));
        }
        public async Task<GalleryVideoViewModel> GetGalleryVideoDetail(Guid? Id)
        {
            return _mapper.Map<GalleryVideoViewModel>(await _galleryrepository.GetVideoDetail(Id));
        }

        public async Task DeleteGalleryVideo(Guid Id)
        {
            await _galleryrepository.DeleteGallerVideo(Id);
        }
        public async Task UpdateVideoDetail(GalleryVideoViewModel model)
        {           
            await _galleryrepository.UpdateVideoDetail(_mapper.Map<GalleryVideo>(model));
        }
    }
}
