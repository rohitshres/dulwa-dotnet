﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Dulwa.v1.Areas.ViewModel;
using Dulwa.v1.Repository.Advertisements;

namespace Dulwa.v1.Service.Advertisements
{
    public class AdvertisementService : IAdvertisementService
    {
        private IAdvertisementRepository _advertisementrepository;
        private IMapper _mapper;

        public AdvertisementService(IAdvertisementRepository advertisementrepository, IMapper mapper)
        {
            _advertisementrepository = advertisementrepository;
            _mapper = mapper;
        }
        public async Task<IEnumerable<AdvertisementViewModel>> GetAdvertisementList(int take)
        {
            return _mapper.Map<IEnumerable<AdvertisementViewModel>>(await _advertisementrepository.GetAdvertisementList(take)); 
        }
    }
}
