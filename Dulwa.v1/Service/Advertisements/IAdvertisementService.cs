﻿using Dulwa.v1.Areas.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Dulwa.v1.Service.Advertisements
{
   public interface IAdvertisementService
    {
        Task<IEnumerable<AdvertisementViewModel>> GetAdvertisementList(int take);
    }
}
