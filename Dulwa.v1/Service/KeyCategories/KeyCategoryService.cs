﻿using AutoMapper;
using Dulwa.v1.Repository.KeyCategories;
using Dulwa.v1.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Dulwa.v1.Service.KeyCategories
{
    public class KeyCategoryService:IKeyCategoryService
    {
        private IKeyCategoryRepository _keyrepository;
        private IMapper _mapper;

        public KeyCategoryService(IKeyCategoryRepository keyrepository, IMapper mapper)
        {
            _keyrepository = keyrepository;
            _mapper = mapper;
        }

        public async Task<IEnumerable<KeysViewModel>> GetKeys(string name)
        {
            return _mapper.Map<IEnumerable<KeysViewModel>>(await _keyrepository.KeysList(name));
        }
    }
}
