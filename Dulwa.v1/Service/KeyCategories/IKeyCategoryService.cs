﻿using Dulwa.v1.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Dulwa.v1.Service.KeyCategories
{
    public interface IKeyCategoryService
    {
        Task<IEnumerable<KeysViewModel>> GetKeys(string name);
    }
}
