﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Dulwa.v1.Data;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Authorization;

namespace Dulwa.v1.Areas.Admin.Controllers
{
    [Area("Admin")]
    [Authorize(Roles ="Admin")]
    public class HomeController : Controller
    {
        private ApplicationDbContext _context;

        public HomeController(ApplicationDbContext context)
        {
            _context = context;
        }
        public IActionResult Index()
        {
            return View();
        }
        public IActionResult UserControl()
        {
            ViewBag.Users = _context.Users.ToList();
            ViewBag.Roles = _context.Roles.ToList();
            return View();
        }
    }
}