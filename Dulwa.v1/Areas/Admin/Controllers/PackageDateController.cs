﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using Dulwa.v1.Data;
using Dulwa.v1.Entity;
using Microsoft.AspNetCore.Authorization;
using System.Net;

namespace Dulwa.v1.Areas.Admin.Controllers
{
    [Area("Admin")]
    [Authorize(Roles = "Admin")]

    public class PackageDateController : Controller
    {
        private readonly ApplicationDbContext _context;

        public PackageDateController(ApplicationDbContext context)
        {
            _context = context;
        }

        // GET: Admin/PackageDate
        public async Task<IActionResult> Index()
        {
            var applicationDbContext = _context.PackageDate.Include(p => p.Package);
            return View(await applicationDbContext.ToListAsync());
        }

        // GET: Admin/PackageDate/Details/5
        public async Task<IActionResult> Details(Guid? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var packageDate = await _context.PackageDate
                .Include(p => p.Package)
                .SingleOrDefaultAsync(m => m.Id == id);
            if (packageDate == null)
            {
                return NotFound();
            }

            return View(packageDate);
        }

        // GET: Admin/PackageDate/Create
        public IActionResult Create()
        {
            ViewData["PackageId"] = new SelectList(_context.Package, "Id", "Name");
            return View();
        }

        // POST: Admin/PackageDate/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("Date,PackageId,Id,Created_Date,Updated_Date")] PackageDate packageDate)
        {
            if (ModelState.IsValid)
            {
                packageDate.Id = Guid.NewGuid();
                _context.Add(packageDate);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            ViewData["PackageId"] = new SelectList(_context.Package, "Id", "Id", packageDate.PackageId);
            return View(packageDate);
        }

        // GET: Admin/PackageDate/Edit/5
        public async Task<IActionResult> Edit(Guid? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var packageDate = await _context.PackageDate.SingleOrDefaultAsync(m => m.Id == id);
            if (packageDate == null)
            {
                return NotFound();
            }
            ViewData["PackageId"] = new SelectList(_context.Package, "Id", "Id", packageDate.PackageId);
            return View(packageDate);
        }

        // POST: Admin/PackageDate/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(Guid id, [Bind("Date,PackageId,Id,Created_Date,Updated_Date")] PackageDate packageDate)
        {
            if (id != packageDate.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(packageDate);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!PackageDateExists(packageDate.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            ViewData["PackageId"] = new SelectList(_context.Package, "Id", "Id", packageDate.PackageId);
            return View(packageDate);
        }

        // GET: Admin/PackageDate/Delete/5
      
        [HttpPost]
        public async Task<JsonResult> Delete(Guid id)
        {
            try
            {
                var packageDate = await _context.PackageDate.SingleOrDefaultAsync(m => m.Id == id);
                _context.PackageDate.Remove(packageDate);
                await _context.SaveChangesAsync();
                return Json(HttpStatusCode.OK);
            }
            catch (Exception ex)
            {
                return Json(HttpStatusCode.InternalServerError);
            }

        }
        private bool PackageDateExists(Guid id)
        {
            return _context.PackageDate.Any(e => e.Id == id);
        }
    }
}
