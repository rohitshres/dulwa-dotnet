﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using Dulwa.v1.Data;
using Dulwa.v1.Entity;
using Microsoft.AspNetCore.Authorization;
using System.Security.Claims;
using Dulwa.v1.Services;
using Dulwa.v1.Service.Storys;
using Dulwa.v1.ViewModel;
using System.Net;

namespace Dulwa.v1.Areas.Admin.Controllers
{
    [Area("Admin")]
    [Authorize(Roles = "Admin")]
    public class DulwaStoryController : Controller
    {
        private readonly ApplicationDbContext _context;
        private readonly IStoryService _storyservice;
        private readonly IFileUpload _file;

        public DulwaStoryController(ApplicationDbContext context, IStoryService StoryService)
        {
            _context = context;
            _storyservice = StoryService;
        }

        // GET: Admin/DulwaStory
        public async Task<IActionResult> Index()
        {
            
            return View(await _storyservice.GetAllList());
        }

        // GET: Admin/DulwaStory/Details/5
        public async Task<IActionResult> Details(Guid? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var dulwaStory = await _storyservice.GetSingleData(id);
            if (dulwaStory == null)
            {
                return NotFound();
            }

            return View(dulwaStory);
        }

        // GET: Admin/DulwaStory/Create
        public IActionResult Create()
        {
            ViewData["DulwainfoId"] = new SelectList(_context.Dulwainfo, "Id", "Name");
            ViewData["UserId"] = new SelectList(_context.Users, "Id", "UserName");
            ViewData["StoryCateogory"] = new SelectList(_context.StoryCategory, "Id", "Name");
            return View();
        }

        // POST: Admin/DulwaStory/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create(StoryViewModel dulwaStory)
        {
            if (ModelState.IsValid)
            {
                dulwaStory.Id = Guid.NewGuid();
                dulwaStory.Approval = true;
                dulwaStory.UserId = this.User.FindFirstValue(ClaimTypes.NameIdentifier);
                await _storyservice.CreateStory(dulwaStory);
                return RedirectToAction(nameof(Index));
            }
            ViewData["DulwainfoId"] = new SelectList(_context.Dulwainfo, "Id", "Name", dulwaStory.DulwainfoId);
            ViewData["UserId"] = new SelectList(_context.Users, "Id", "UserName", dulwaStory.UserId);
            ViewData["StoryCateogory"] = new SelectList(_context.StoryCategory, "Id", "Name");

            return View(dulwaStory);
        }

        // GET: Admin/DulwaStory/Edit/5
        public async Task<IActionResult> Edit(Guid? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var dulwaStory = await _storyservice.GetSingleData(id);
            if (dulwaStory == null)
            {
                return NotFound();
            }
            ViewData["DulwainfoId"] = new SelectList(_context.Dulwainfo, "Id", "Name", dulwaStory.DulwainfoId);
            ViewData["UserId"] = new SelectList(_context.Users, "Id", "UserName", dulwaStory.UserId);
            ViewData["StoryCateogory"] = new SelectList(_context.StoryCategory, "Id", "Name",dulwaStory.StoryCategoryId);

            return View(dulwaStory);
        }

        // POST: Admin/DulwaStory/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(Guid id, StoryViewModel dulwaStory)
        {
            if (id != dulwaStory.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    dulwaStory.Approval = true;
                    dulwaStory.UserId = this.User.FindFirstValue(ClaimTypes.NameIdentifier);
                    await _storyservice.Update(dulwaStory);
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!await _storyservice.CheckData(dulwaStory.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            ViewData["DulwainfoId"] = new SelectList(_context.Dulwainfo, "Id", "Name", dulwaStory.DulwainfoId);
            ViewData["UserId"] = new SelectList(_context.Users, "Id", "UserName", dulwaStory.UserId);
            ViewData["StoryCateogory"] = new SelectList(_context.StoryCategory, "Id", "Name", dulwaStory.StoryCategoryId);

            return View(dulwaStory);
        }

        // GET: Admin/DulwaStory/Delete/5
       

        [HttpPost]
        public async Task<JsonResult> Delete(Guid id)
        {
            try
            {
                await _storyservice.Delete(id);
                return Json(HttpStatusCode.OK);
            }
            catch (Exception ex)
            {
                return Json(HttpStatusCode.InternalServerError);
            }

        }



        public async Task<IActionResult> StoryApproval()
        {
            return View(await _storyservice.GetAllPendingStory());
        }

        public async Task<IActionResult> StoryDetail(Guid? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var dulwaStory = await _storyservice.GetSingleData(id);
            if (dulwaStory == null)
            {
                return NotFound();
            }

            return View(dulwaStory);
        }

        [HttpPost]
        public async Task<IActionResult> ApproveStroy(Guid Id)
        {
            if (Id == null)
            {
                return NotFound();
            }
            var dulwastory = await _storyservice.GetSingleData(Id);
            dulwastory.Approval = true;
            try
            {                
                await _storyservice.Update(dulwastory);
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!await _storyservice.CheckData(dulwastory.Id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }
            return RedirectToAction(nameof(StoryApproval));
        }


    }
}
