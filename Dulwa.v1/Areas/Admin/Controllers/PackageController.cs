﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using Dulwa.v1.Data;
using Dulwa.v1.Entity;
using Dulwa.v1.Services;
using Dulwa.v1.ViewModel;
using Dulwa.v1.Service.Packages;
using Dulwa.v1.Service.Reviews;
using Microsoft.AspNetCore.Authorization;
using System.Net;

namespace Dulwa.v1.Areas.Admin.Controllers
{
    [Area("Admin")]
    [Authorize(Roles = "Admin")]

    public class PackageController : Controller
    {
        private readonly ApplicationDbContext _context;
        private readonly IFileUpload _fileupload;
        private readonly IPackageService _packageservice;
        private readonly IReviewService _reviewService;

        public PackageController(ApplicationDbContext context, IPackageService Packageservice, IFileUpload fileupload, IReviewService reviewservice)
        {
            _context = context;
            _fileupload = fileupload;
            _packageservice = Packageservice;
            _reviewService = reviewservice;
        }

        // GET: Admin/Package
        public async Task<IActionResult> Index()
        {
            var applicationDbContext = _context.Package.Include(p => p.Dulwainfo).Include(p => p.company);
            return View(await applicationDbContext.ToListAsync());
        }

        // GET: Admin/Package/Details/5
        public async Task<IActionResult> Details(Guid? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var package = await _context.Package
                .Include(p => p.Dulwainfo)
                .Include(p => p.company)
                .SingleOrDefaultAsync(m => m.Id == id);
            if (package == null)
            {
                return NotFound();
            }

            return View(package);
        }

        // GET: Admin/Package/Create
        public IActionResult Create()
        {
            ViewData["DulwainfoId"] = new SelectList(_context.Dulwainfo, "Id", "Name");
            ViewData["CompanyId"] = new SelectList(_context.Company, "Id", "name");
            ViewData["StoreCategory"] = new SelectList(_context.StoreCategory, "Id", "Name");
            return View();
        }

        // POST: Admin/Package/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create(PackageViewModel package)
        {
            if (ModelState.IsValid)
            {
                package.Id = Guid.NewGuid();
                await _packageservice.CreatePackage(package);  
                return RedirectToAction("PackageRouteCreate", "Route",new { Id=package.Id});

                return RedirectToAction(nameof(Index));
            }
            ViewData["DulwainfoId"] = new SelectList(_context.Dulwainfo, "Id", "Name", package.DulwainfoId);
            ViewData["CompanyId"] = new SelectList(_context.Company, "Id", "name", package.CompanyId);
            ViewData["StoreCategory"] = new SelectList(_context.StoreCategory, "Id", "Name");

            return View(package);
        }


        public async Task<IActionResult> CreatePackageRoute()
        {
            return View();
        }

        public async Task<IActionResult> CreatePackageDate(Guid PackageId)
        {
            if (PackageId == Guid.Empty)
                return RedirectToAction(nameof(Index));
            ViewBag.package= await _context.Package                
                .SingleOrDefaultAsync(m => m.Id == PackageId);

            return View();
        }
        [HttpPost]
        public async Task<IActionResult> CreatePackageDate(PackageDateViewModel model)
        {
            if (ModelState.IsValid)
            {
                await _packageservice.CreatePackageDate(model);
                return RedirectToAction(nameof(Index));
            }
            return View();
        }
        // GET: Admin/Package/Edit/5
        public async Task<IActionResult> Edit(Guid? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var package = await _packageservice.GetSingleData(id);
            if (package == null)
            {
                return NotFound();
            }
            ViewData["DulwainfoId"] = new SelectList(_context.Dulwainfo, "Id", "Name",package.DulwainfoId);
            ViewData["CompanyId"] = new SelectList(_context.Company, "Id", "name",package.CompanyId);
            ViewData["StoreCategory"] = new SelectList(_context.StoreCategory, "Id", "Name");            
            return View(package);
        }

        // POST: Admin/Package/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(Guid id, PackageViewModel package)
        {
            if (id != package.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    await _packageservice.Update(package);
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!PackageExists(package.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            ViewData["DulwainfoId"] = new SelectList(_context.Dulwainfo, "Id", "Name", package.DulwainfoId);
            ViewData["CompanyId"] = new SelectList(_context.Company, "Id", "name", package.CompanyId);
            ViewData["StoreCategory"] = new SelectList(_context.StoreCategory, "Id", "Name");
            return View(package);
        }

        // GET: Admin/Package/Delete/5

        [HttpPost]
        public async Task<JsonResult> Delete(Guid id)
        {
            try
            {
                await _reviewService.DeleteReviewPacakage(id);
                await _packageservice.Delete(id);
                return Json(HttpStatusCode.OK);
            }
            catch (Exception ex)
            {
                return Json(HttpStatusCode.InternalServerError);
            }

        }

        private bool PackageExists(Guid id)
        {
            return _context.Package.Any(e => e.Id == id);
        }
    }
}
