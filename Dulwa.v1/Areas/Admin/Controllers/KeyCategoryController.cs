﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using Dulwa.v1.Data;
using Dulwa.v1.Entity;
using Microsoft.AspNetCore.Authorization;
using System.Net;

namespace Dulwa.v1.Areas.Admin.Controllers
{
    [Area("Admin")]
    [Authorize(Roles = "Admin")]

    public class KeyCategoryController : Controller
    {
        private readonly ApplicationDbContext _context;

        public KeyCategoryController(ApplicationDbContext context)
        {
            _context = context;
        }

        // GET: Admin/KeyCategory
        public async Task<IActionResult> Index()
        {
            return View(await _context.KeyCategory.ToListAsync());
        }

        // GET: Admin/KeyCategory/Details/5
        public async Task<IActionResult> Details(Guid? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var keyCategory = await _context.KeyCategory
                .SingleOrDefaultAsync(m => m.Id == id);
            if (keyCategory == null)
            {
                return NotFound();
            }

            return View(keyCategory);
        }

        // GET: Admin/KeyCategory/Create
        public IActionResult Create()
        {
            return View();
        }

        // POST: Admin/KeyCategory/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("Name,Id,Created_Date,Updated_Date")] KeyCategory keyCategory)
        {
            if (ModelState.IsValid)
            {
                keyCategory.Id = Guid.NewGuid();
                _context.Add(keyCategory);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            return View(keyCategory);
        }

        // GET: Admin/KeyCategory/Edit/5
        public async Task<IActionResult> Edit(Guid? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var keyCategory = await _context.KeyCategory.SingleOrDefaultAsync(m => m.Id == id);
            if (keyCategory == null)
            {
                return NotFound();
            }
            return View(keyCategory);
        }

        // POST: Admin/KeyCategory/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(Guid id, [Bind("Name,Id,Created_Date,Updated_Date")] KeyCategory keyCategory)
        {
            if (id != keyCategory.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(keyCategory);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!KeyCategoryExists(keyCategory.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            return View(keyCategory);
        }

        // GET: Admin/KeyCategory/Delete/5       
        [HttpPost]
        public async Task<JsonResult> Delete(Guid id)
        {
            try
            {
                var keyCategory = await _context.KeyCategory.SingleOrDefaultAsync(m => m.Id == id);
                _context.KeyCategory.Remove(keyCategory);
                await _context.SaveChangesAsync();
                return Json(HttpStatusCode.OK);
            }
            catch (Exception ex)
            {
                return Json(HttpStatusCode.InternalServerError);
            }

        }

        private bool KeyCategoryExists(Guid id)
        {
            return _context.KeyCategory.Any(e => e.Id == id);
        }
    }
}
