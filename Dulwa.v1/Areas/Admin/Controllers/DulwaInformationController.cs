﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using Dulwa.v1.Data;
using Dulwa.v1.ViewModel;
using Microsoft.AspNetCore.Authorization;
using System.Net;

namespace Dulwa.v1.Areas.Admin.Controllers
{
    [Authorize(Roles = "Admin")]

    [Area("Admin")]
    public class DulwaInformationController : Controller
    {
        private readonly ApplicationDbContext _context;

        public DulwaInformationController(ApplicationDbContext context)
        {
            _context = context;
        }

        // GET: Admin/DulwaInformation
        public async Task<IActionResult> Index()
        {
            return View(await _context.DulwaInformation.ToListAsync());
        }

        // GET: Admin/DulwaInformation/Details/5
        public async Task<IActionResult> Details(Guid? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var dulwaInformationViewModel = await _context.DulwaInformation
                .SingleOrDefaultAsync(m => m.Id == id);
            if (dulwaInformationViewModel == null)
            {
                return NotFound();
            }

            return View(dulwaInformationViewModel);
        }

        // GET: Admin/DulwaInformation/Create
        public IActionResult Create()
        {
            return View();
        }

        // POST: Admin/DulwaInformation/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("Id,TollFreeNo")] DulwaInformationViewModel dulwaInformationViewModel)
        {
            if (ModelState.IsValid)
            {
                dulwaInformationViewModel.Id = Guid.NewGuid();
                _context.Add(dulwaInformationViewModel);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            return View(dulwaInformationViewModel);
        }

        // GET: Admin/DulwaInformation/Edit/5
        public async Task<IActionResult> Edit(Guid? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var dulwaInformationViewModel = await _context.DulwaInformation.SingleOrDefaultAsync(m => m.Id == id);
            if (dulwaInformationViewModel == null)
            {
                return NotFound();
            }
            return View(dulwaInformationViewModel);
        }

        // POST: Admin/DulwaInformation/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(Guid id, [Bind("Id,TollFreeNo")] DulwaInformationViewModel dulwaInformationViewModel)
        {
            if (id != dulwaInformationViewModel.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(dulwaInformationViewModel);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!DulwaInformationViewModelExists(dulwaInformationViewModel.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            return View(dulwaInformationViewModel);
        }

        // GET: Admin/DulwaInformation/Delete/5       
        [HttpPost]
        public async Task<JsonResult> Delete(Guid id)
        {
            try
            {
                var dulwaInformationViewModel = await _context.DulwaInformation.SingleOrDefaultAsync(m => m.Id == id);
                _context.DulwaInformation.Remove(dulwaInformationViewModel);
                await _context.SaveChangesAsync();
                return Json(HttpStatusCode.OK);
            }
            catch (Exception ex)
            {
                return Json(HttpStatusCode.InternalServerError);
            }

        }
        private bool DulwaInformationViewModelExists(Guid id)
        {
            return _context.DulwaInformation.Any(e => e.Id == id);
        }
    }
}
