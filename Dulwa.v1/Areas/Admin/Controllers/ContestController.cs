﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using Dulwa.v1.Data;
using Dulwa.v1.Entity;
using Dulwa.v1.ViewModel;
using Dulwa.v1.Service.Contests;
using Microsoft.AspNetCore.Authorization;
using System.Net;

namespace Dulwa.v1.Areas.Admin.Controllers
{
    [Authorize(Roles ="Admin")]
    [Area("Admin")]
    public class ContestController : Controller
    {
        private readonly ApplicationDbContext _context;
        private readonly IContestService _contestdetail;

        public ContestController(ApplicationDbContext context, IContestService contest)
        {
            _context = context;
            _contestdetail = contest;
        }

        // GET: Admin/Contest
        public async Task<IActionResult> Index()
        {
            return View(await _context.ContestDetail.ToListAsync());
        }

        // GET: Admin/Contest/Details/5
        public async Task<IActionResult> Details(Guid? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var contestDetail = await _context.ContestDetail
                .SingleOrDefaultAsync(m => m.Id == id);
            if (contestDetail == null)
            {
                return NotFound();
            }

            return View(contestDetail);
        }

        // GET: Admin/Contest/Create
        public IActionResult Create()
        {
            return View();
        }

        // POST: Admin/Contest/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create(ContestDetailViewModel contestDetail)
        {
            if (ModelState.IsValid)
            {
                await _contestdetail.CreateContestDetail(contestDetail);
                return RedirectToAction(nameof(Index));
            }
            return View(contestDetail);
        }

        // GET: Admin/Contest/Edit/5
        public async Task<IActionResult> Edit(Guid? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var contestDetail = await _context.ContestDetail.SingleOrDefaultAsync(m => m.Id == id);
            if (contestDetail == null)
            {
                return NotFound();
            }
            return View(contestDetail);
        }

        // POST: Admin/Contest/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(Guid id, [Bind("Title,Description,StartDate,EndDate,Status,Id,Created_Date,Updated_Date")] ContestDetail contestDetail)
        {
            if (id != contestDetail.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(contestDetail);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!ContestDetailExists(contestDetail.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            return View(contestDetail);
        }

        // GET: Admin/Contest/Delete/5
       
        [HttpPost]
        public async Task<JsonResult> Delete(Guid id)
        {
            try
            {
                var contestDetail = await _context.ContestDetail.SingleOrDefaultAsync(m => m.Id == id);
                _context.ContestDetail.Remove(contestDetail);
                await _context.SaveChangesAsync();
                return Json(HttpStatusCode.OK);
            }
            catch (Exception ex)
            {
                return Json(HttpStatusCode.InternalServerError);
            }

        }

        private bool ContestDetailExists(Guid id)
        {
            return _context.ContestDetail.Any(e => e.Id == id);
        }
    }
}
