﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using Dulwa.v1.Data;
using Dulwa.v1.Entity;
using Dulwa.v1.Service.Tradition_Cultures;
using Dulwa.v1.ViewModel;
using Dulwa.v1.Service.KeyCategories;
using Dulwa.v1.Service.DulwaInfo;
using Microsoft.AspNetCore.Authorization;
using System.Net;

namespace Dulwa.v1.Areas.Admin.Controllers
{
    [Authorize(Roles = "Admin")]

    [Area("Admin")]
    public class TraditionCultureController : Controller
    {
        private readonly ApplicationDbContext _context;
        private ITradition_CultureService _traditionculture;
        private IKeyCategoryService _categoryservice;
        private IDulwaInfoService _dulwainfoservice;

        public TraditionCultureController(ITradition_CultureService traditionculture, IDulwaInfoService dulwainfoservice, IKeyCategoryService categoryservice)
        {
            _traditionculture = traditionculture;
            _categoryservice = categoryservice;
            _dulwainfoservice = dulwainfoservice;
            
        }

        // GET: Admin/TraditionCulture
        public async Task<IActionResult> Index()
        {
            return View(await _traditionculture.GetAllList());
        }

        // GET: Admin/TraditionCulture/Details/5
        public async Task<IActionResult> Details(Guid? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var tradition_Culture = await _traditionculture.GetSingleData(id);
            if (tradition_Culture == null)
            {
                return NotFound();
            }

            return View(tradition_Culture);
        }

        // GET: Admin/TraditionCulture/Create
        public async Task<IActionResult> Create()
        {
            ViewData["DulwainfoId"] = new SelectList(await _dulwainfoservice.getDulwaInfo(), "Id", "Name");
            ViewData["community"] = new SelectList(await _categoryservice.GetKeys("Community"), "Id", "Name");

            return View();
        }

        // POST: Admin/TraditionCulture/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create(TraditionCultureViewModel tradition_Culture)
        {
            if (ModelState.IsValid)
            {
                tradition_Culture.Id = Guid.NewGuid();
                await _traditionculture.Create(tradition_Culture);
                return RedirectToAction(nameof(Index));
            }
            ViewData["DulwainfoId"] = new SelectList(await _dulwainfoservice.getDulwaInfo(), "Id", "Name", tradition_Culture.DulwainfoId);
            ViewData["community"] = new SelectList(await _categoryservice.GetKeys("Community"), "Id", "Name");

            return View(tradition_Culture);
        }

        // GET: Admin/TraditionCulture/Edit/5
        public async Task<IActionResult> Edit(Guid? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var tradition_Culture = await _traditionculture.GetSingleData(id);
            if (tradition_Culture == null)
            {
                return NotFound();
            }
            ViewData["DulwainfoId"] = new SelectList(await _dulwainfoservice.getDulwaInfo(), "Id", "Name", tradition_Culture.DulwainfoId);
            ViewData["community"] = new SelectList(await _categoryservice.GetKeys("Community"), "Id", "Name");

            return View(tradition_Culture);
        }

        // POST: Admin/TraditionCulture/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(Guid id, TraditionCultureViewModel tradition_Culture)
        {
            if (id != tradition_Culture.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    await _traditionculture.Update(tradition_Culture);
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!await _traditionculture.CheckData(tradition_Culture.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            ViewData["DulwainfoId"] = new SelectList(await _dulwainfoservice.getDulwaInfo(), "Id", "Name", tradition_Culture.DulwainfoId);
            ViewData["community"] = new SelectList(await _categoryservice.GetKeys("Community"), "Id", "Name");

            return View(tradition_Culture);
        }

        // GET: Admin/TraditionCulture/Delete/5        

        [HttpPost]
        public async Task<JsonResult> Delete(Guid id)
        {
            try
            {
                await _traditionculture.Delete(id);
                return Json(HttpStatusCode.OK);
            }
            catch (Exception ex)
            {
                return Json(HttpStatusCode.InternalServerError);
            }

        }
    }
}
