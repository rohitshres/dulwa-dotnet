﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using Dulwa.v1.Data;
using Dulwa.v1.Entity;
using Dulwa.v1.ViewModel;
using Dulwa.v1.Service.Gallerys;
using Dulwa.v1.Service.DulwaInfo;
using Microsoft.AspNetCore.Authorization;

namespace Dulwa.v1.Areas.Admin.Controllers
{
    [Area("Admin")]
    [Authorize(Roles = "Admin")]
    public class GalleryController : Controller
    {
        private IDulwaInfoService _dulwainfoservice;
        private IGalleryService _galleryservice;

        public GalleryController(IDulwaInfoService dulwainfoservice, IGalleryService galleryservice)
        {
            _dulwainfoservice = dulwainfoservice;
            _galleryservice = galleryservice;
        }

       
        // GET: Admin/Gallery
        public async Task<IActionResult> Index()
        {
            return View(await _galleryservice.GetAllGallery());
        }

        //// GET: Admin/Gallery/Details/5
        //public async Task<IActionResult> Details(Guid? id)
        //{
        //    if (id == null)
        //    {
        //        return NotFound();
        //    }

        //    var gallery = await _context.Gallery
        //        .Include(g => g.Dulwainfo)
        //        .SingleOrDefaultAsync(m => m.Id == id);
        //    if (gallery == null)
        //    {
        //        return NotFound();
        //    }

        //    return View(gallery);
        //}

        //// GET: Admin/Gallery/Create
        public async Task<IActionResult> Create()
        {
            ViewData["DulwainfoId"] = new SelectList(await _dulwainfoservice.getDulwaInfo(), "Id", "Name");
            return View();
        }
        public async Task<IActionResult> GalleryDetail(Guid Id)
        {
            return View(await _galleryservice.GetAllGalleryWithGalleryId(Id));
        }

        //// POST: Admin/Gallery/Create
        //// To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        //// more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create(GalleryViewModel gallery)
        {
            if (ModelState.IsValid)
            {
                await _galleryservice.Create(gallery);

                return RedirectToAction(nameof(Index));
            }
            ViewData["DulwainfoId"] = new SelectList(await  _dulwainfoservice.getDulwaInfo(), "Id", "Name", gallery.DulwainfoId);
            return View(gallery);
        }

        public async Task<IActionResult> ImageDetail(Guid Id)
        {
            return View(await _galleryservice.GetGalleryImageDetail(Id));
        }

        public async Task<IActionResult> ImageDetailEdit(Guid Id)
        {
            return View(await _galleryservice.GetGalleryImageDetail(Id));
        }
        [HttpPost]
        public async Task<IActionResult> ImageDetailEdit(Guid id,GalleryImageViewModel model)
        {
            if (id != model.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                   await _galleryservice.UpdateImageDetail(model); ;
                }
                catch (DbUpdateConcurrencyException)
                {
                     return NotFound();
                    
                }
                return RedirectToAction(nameof(Index));
            }
            return View(model);
        }

        public async Task<IActionResult> AddImage(Guid Id)
        {
            ViewBag.GalleryId = Id;
            return View();
        }
        [HttpPost]
        public async Task<IActionResult> AddImage(GalleryImageViewModel model)
        {
            if (ModelState.IsValid)
            {
                await _galleryservice.CreateGalleryImage(model);
                return RedirectToAction(nameof(GalleryDetail), new { Id = model.GalleryId });
            }
            return View(model);
        }

        public async Task<IActionResult> DeleteImage(Guid? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var gallery = await _galleryservice.GetGalleryImageDetail(id);
            if (gallery == null)
            {
                return NotFound();
            }

            return View(gallery);
        }

        // POST: Admin/Cuisine/Delete/5
        [HttpPost, ActionName("DeleteImage")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(Guid id)
        {
            await _galleryservice.DeleteGalleryImage(id);
            return RedirectToAction(nameof(Index));
        }



        public async Task<IActionResult> AddVideo(Guid Id)
        {
            ViewBag.GalleryId = Id;
            return View();
        }
        [HttpPost]
        public async Task<IActionResult> AddVideo(GalleryVideoViewModel model)
        {
            if (ModelState.IsValid)
            {
                await _galleryservice.CreateGalleryVideo(model);
                return RedirectToAction(nameof(GalleryDetail), new { Id = model.GalleryId });
            }
            return View();
        }

        public async Task<IActionResult> VideoDetail(Guid Id)
        {
            return View(await _galleryservice.GetGalleryVideoDetail(Id));
        }
        public async Task<IActionResult> EditVideo(Guid Id)
        {
            return View(await _galleryservice.GetGalleryVideoDetail(Id));
        }
        [HttpPost]
        public async Task<IActionResult> EditVideo(GalleryVideoViewModel Model)
        {
            await _galleryservice.UpdateVideoDetail(Model);
            return View();
        }
        public async Task<IActionResult> DeleteVideo(Guid? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var gallery = await _galleryservice.GetGalleryVideoDetail(id);
            if (gallery == null)
            {
                return NotFound();
            }

            return View(gallery);
        }

        // POST: Admin/Cuisine/Delete/5
        [HttpPost, ActionName("DeleteVideo")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteVideoConfirm(Guid id)
        {
            await _galleryservice.DeleteGalleryVideo(id);
            return RedirectToAction(nameof(Index));
        }

        //// GET: Admin/Gallery/Edit/5
        //public async Task<IActionResult> Edit(Guid? id)
        //{
        //    if (id == null)
        //    {
        //        return NotFound();
        //    }

        //    var gallery = await _context.Gallery.SingleOrDefaultAsync(m => m.Id == id);
        //    if (gallery == null)
        //    {
        //        return NotFound();
        //    }
        //    ViewData["DulwainfoId"] = new SelectList(_context.Dulwainfo, "Id", "Id", gallery.DulwainfoId);
        //    return View(gallery);
        //}

        //// POST: Admin/Gallery/Edit/5
        //// To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        //// more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        //[HttpPost]
        //[ValidateAntiForgeryToken]
        //public async Task<IActionResult> Edit(Guid id, Gallery gallery)
        //{
        //    if (id != gallery.Id)
        //    {
        //        return NotFound();
        //    }

        //    if (ModelState.IsValid)
        //    {
        //        try
        //        {
        //            _context.Update(gallery);
        //            await _context.SaveChangesAsync();
        //        }
        //        catch (DbUpdateConcurrencyException)
        //        {
        //            if (!GalleryExists(gallery.Id))
        //            {
        //                return NotFound();
        //            }
        //            else
        //            {
        //                throw;
        //            }
        //        }
        //        return RedirectToAction(nameof(Index));
        //    }
        //    ViewData["DulwainfoId"] = new SelectList(_context.Dulwainfo, "Id", "Id", gallery.DulwainfoId);
        //    return View(gallery);
        //}

        //// GET: Admin/Gallery/Delete/5
        //public async Task<IActionResult> Delete(Guid? id)
        //{
        //    if (id == null)
        //    {
        //        return NotFound();
        //    }

        //    var gallery = await _context.Gallery
        //        .Include(g => g.Dulwainfo)
        //        .SingleOrDefaultAsync(m => m.Id == id);
        //    if (gallery == null)
        //    {
        //        return NotFound();
        //    }

        //    return View(gallery);
        //}

        //// POST: Admin/Gallery/Delete/5
        //[HttpPost, ActionName("Delete")]
        //[ValidateAntiForgeryToken]
        //public async Task<IActionResult> DeleteConfirmed(Guid id)
        //{
        //    var gallery = await _context.Gallery.SingleOrDefaultAsync(m => m.Id == id);
        //    _context.Gallery.Remove(gallery);
        //    await _context.SaveChangesAsync();
        //    return RedirectToAction(nameof(Index));
        //}

        //private bool GalleryExists(Guid id)
        //{
        //    return _context.Gallery.Any(e => e.Id == id);
        //}
    }
}
