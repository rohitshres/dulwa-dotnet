﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using Dulwa.v1.Data;
using Dulwa.v1.Entity;
using Dulwa.v1.Service.DestinationStop;
using Dulwa.v1.ViewModel;
using Dulwa.v1.Service.KeyCategories;
using Dulwa.v1.Service.DulwaInfo;
using Microsoft.AspNetCore.Authorization;
using System.Net;

namespace Dulwa.v1.Areas.Admin.Controllers
{
    [Area("Admin")]
    [Authorize(Roles = "Admin")]

    public class DestinationStopsController : Controller
    {      
        private readonly IDestinationStopService _DestinationStops;
        private readonly IKeyCategoryService _categoryservice;
        private readonly IDulwaInfoService _dulwainfoservice;

        public DestinationStopsController(IDestinationStopService DestinationStop, IKeyCategoryService categoryservice, IDulwaInfoService DulwainfoService)
        {           
            _DestinationStops = DestinationStop;
            _categoryservice = categoryservice;
            _dulwainfoservice = DulwainfoService;
        }

        // GET: Admin/DestinationStops
        public async Task<IActionResult> Index()
        {
            
            return View(await _DestinationStops.GetAllList());
        }

        // GET: Admin/DestinationStops/Details/5
        public async Task<IActionResult> Details(Guid? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var destinationStops = await _DestinationStops.GetSingleData(id);
            if (destinationStops == null)
            {
                return NotFound();
            }

            return View(destinationStops);
        }

        // GET: Admin/DestinationStops/Create
        public async Task<IActionResult> Create()
        {
            ViewData["DulwainfoId"] = new SelectList(await _dulwainfoservice.getDulwaInfo(), "Id", "Name");
            ViewData["popular"] = new SelectList(await _categoryservice.GetKeys("Popular"), "Id", "Name");
            return View();
        }

        // POST: Admin/DestinationStops/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create(DestinationStopsViewModel destinationStops)
        {
            if (ModelState.IsValid)
            {               
                await _DestinationStops.Create(destinationStops);
                return RedirectToAction(nameof(Index));
            }
            ViewData["DulwainfoId"] = new SelectList(await _dulwainfoservice.getDulwaInfo(), "Id", "Name", destinationStops.DulwainfoId);
            ViewData["popular"] = new SelectList(await _categoryservice.GetKeys("Popular"), "Id", "Name");
            return View(destinationStops);
        }

        // GET: Admin/DestinationStops/Edit/5
        public async Task<IActionResult> Edit(Guid? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var destinationStops = await _DestinationStops.GetSingleData(id);
            if (destinationStops == null)
            {
                return NotFound();
            }
            ViewData["DulwainfoId"] = new SelectList(await _dulwainfoservice.getDulwaInfo(), "Id", "Name", destinationStops.DulwainfoId);
            ViewData["popular"] = new SelectList(await _categoryservice.GetKeys("Popular"), "Id", "Name");
            return View(destinationStops);
        }

        // POST: Admin/DestinationStops/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(Guid id,  DestinationStopsViewModel destinationStops)
        {
            if (id != destinationStops.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    await _DestinationStops.Update(destinationStops);
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!await _DestinationStops.CheckData(destinationStops.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            ViewData["DulwainfoId"] = new SelectList(await _dulwainfoservice.getDulwaInfo(), "Id", "Name", destinationStops.DulwainfoId);
            ViewData["popular"] = new SelectList(await _categoryservice.GetKeys("Popular"), "Id", "Name");

            return View(destinationStops);
        }

        // GET: Admin/DestinationStops/Delete/5       

        [HttpPost]
        public async Task<JsonResult> Delete(Guid id)
        {
            try
            {
                await _DestinationStops.Delete(id);
                return Json(HttpStatusCode.OK);
            }
            catch (Exception ex)
            {
                return Json(HttpStatusCode.InternalServerError);
            }

        }
    }
}
