﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using Dulwa.v1.Data;
using Dulwa.v1.Entity;
using Microsoft.AspNetCore.Authorization;
using System.Net;

namespace Dulwa.v1.Areas.Admin.Controllers
{
    [Area("Admin")]
    [Authorize(Roles = "Admin")]

    public class StoryCategoryController : Controller
    {
        private readonly ApplicationDbContext _context;

        public StoryCategoryController(ApplicationDbContext context)
        {
            _context = context;
        }

        // GET: Admin/StoryCategory
        public async Task<IActionResult> Index()
        {
            return View(await _context.StoryCategory.ToListAsync());
        }

        // GET: Admin/StoryCategory/Details/5
        public async Task<IActionResult> Details(Guid? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var storyCategory = await _context.StoryCategory
                .SingleOrDefaultAsync(m => m.Id == id);
            if (storyCategory == null)
            {
                return NotFound();
            }

            return View(storyCategory);
        }

        // GET: Admin/StoryCategory/Create
        public IActionResult Create()
        {
            return View();
        }

        // POST: Admin/StoryCategory/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("Name,Id,Created_Date,Updated_Date")] StoryCategory storyCategory)
        {
            if (ModelState.IsValid)
            {
                storyCategory.Id = Guid.NewGuid();
                _context.Add(storyCategory);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            return View(storyCategory);
        }

        // GET: Admin/StoryCategory/Edit/5
        public async Task<IActionResult> Edit(Guid? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var storyCategory = await _context.StoryCategory.SingleOrDefaultAsync(m => m.Id == id);
            if (storyCategory == null)
            {
                return NotFound();
            }
            return View(storyCategory);
        }

        // POST: Admin/StoryCategory/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(Guid id, [Bind("Name,Id,Created_Date,Updated_Date")] StoryCategory storyCategory)
        {
            if (id != storyCategory.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(storyCategory);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!StoryCategoryExists(storyCategory.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            return View(storyCategory);
        }

        // GET: Admin/StoryCategory/Delete/5
       
        [HttpPost]
        public async Task<JsonResult> Delete(Guid id)
        {
            try
            {
                var storyCategory = await _context.StoryCategory.SingleOrDefaultAsync(m => m.Id == id);
                _context.StoryCategory.Remove(storyCategory);
                await _context.SaveChangesAsync();
                return Json(HttpStatusCode.OK);
            }
            catch (Exception ex)
            {
                return Json(HttpStatusCode.InternalServerError);
            }

        }

        private bool StoryCategoryExists(Guid id)
        {
            return _context.StoryCategory.Any(e => e.Id == id);
        }
    }
}
