﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using Dulwa.v1.Data;
using Dulwa.v1.Entity;
using Dulwa.v1.ViewModel;
using Dulwa.v1.Service.Cuisines;
using Dulwa.v1.Service.DulwaInfo;
using Dulwa.v1.Service.KeyCategories;
using Microsoft.AspNetCore.Authorization;
using System.Net;
using Dulwa.v1.Helper;

namespace Dulwa.v1.Areas.Admin.Controllers
{
    [Area("Admin")]
    [Authorize(Roles = "Admin")]

    public class CuisineController : Controller
    {        
        private readonly ICuisinesService _cuisinesservice;
        private readonly IDulwaInfoService _dulwainfoservice;
        private readonly IKeyCategoryService _categoryservice;

        public CuisineController(ICuisinesService Cuisines,IDulwaInfoService dulwainfoservice, IKeyCategoryService categoryservice)
        {           
            _cuisinesservice = Cuisines;
            _dulwainfoservice = dulwainfoservice;
            _categoryservice = categoryservice;
        }

        // GET: Admin/Cuisine
        public async Task<IActionResult> Index()
        {            
            return View(await _cuisinesservice.GetAllList());
        }

        // GET: Admin/Cuisine/Details/5
        public async Task<IActionResult> Details(Guid? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var cuisine = await _cuisinesservice.GetSingleData(id);
            if (cuisine == null)
            {
                return NotFound();
            }

            return View(cuisine);
        }

        // GET: Admin/Cuisine/Create
        public async Task<IActionResult> Create()
        {
            ViewData["DulwainfoId"] = new SelectList(await _dulwainfoservice.getDulwaInfo() , "Id", "Name");
            ViewData["festival"] = new SelectList(await _categoryservice.GetKeys("Festival"), "Id", "Name");
            ViewData["community"] = new SelectList(await _categoryservice.GetKeys("Community"), "Id", "Name");

            return View();
        }

        // POST: Admin/Cuisine/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create(CuisinesViewModel cuisine)
        {
            if (ModelState.IsValid)
            {
                cuisine.Id = Guid.NewGuid();                
                await _cuisinesservice.Create(cuisine);
                return RedirectToAction(nameof(Index));
            }
            ViewData["DulwainfoId"] = new SelectList(await _dulwainfoservice.getDulwaInfo(), "Id", "Name", cuisine.DulwainfoId);
            ViewData["festival"] = new SelectList(await _categoryservice.GetKeys("Festival"), "Id", "Name");
            ViewData["community"] = new SelectList(await _categoryservice.GetKeys("Community"), "Id", "Name");


            return View(cuisine);
        }

        // GET: Admin/Cuisine/Edit/5
        public async Task<IActionResult> Edit(Guid? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var cuisine = await _cuisinesservice.GetSingleData(id);
            if (cuisine == null)
            {
                return NotFound();
            }
            ViewData["DulwainfoId"] = new SelectList(await _dulwainfoservice.getDulwaInfo(), "Id", "Name", cuisine.DulwainfoId);
            ViewData["festival"] = new SelectList(await _categoryservice.GetKeys("Festival"), "Id", "Name");
            ViewData["community"] = new SelectList(await _categoryservice.GetKeys("Community"), "Id", "Name");


            return View(cuisine);
        }

        // POST: Admin/Cuisine/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(Guid id, CuisinesViewModel cuisine)
        {
            if (id != cuisine.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    await _cuisinesservice.Update(cuisine);
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!await _cuisinesservice.CheckData(cuisine.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            ViewData["DulwainfoId"] = new SelectList(await _dulwainfoservice.getDulwaInfo() , "Id", "Name", cuisine.DulwainfoId);
            ViewData["festival"] = new SelectList(await _categoryservice.GetKeys("Festival"), "Id", "Name");
            ViewData["community"] = new SelectList(await _categoryservice.GetKeys("Community"), "Id", "Name");


            return View(cuisine);
        }

        // GET: Admin/Cuisine/Delete/5
        //public async Task<IActionResult> Delete(Guid? id)
        //{
        //    if (id == null)
        //    {
        //        return NotFound();
        //    }

        //    var cuisine = await _cuisinesservice.GetSingleData(id);
        //    if (cuisine == null)
        //    {
        //        return NotFound();
        //    }

        //    return View(cuisine);
        //}

        // POST: Admin/Cuisine/Delete/5
        [HttpPost]        
        public async Task<JsonResult> Delete(Guid id)
        {
            try
            {
                await _cuisinesservice.Delete(id);
                return Json(HttpStatusCode.OK);
            }
            catch(Exception ex)
            {
                return Json(HttpStatusCode.InternalServerError);
            }
            
        }      
    }
}
