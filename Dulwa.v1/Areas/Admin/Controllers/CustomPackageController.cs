﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using Dulwa.v1.Data;
using Dulwa.v1.Entity;
using Microsoft.AspNetCore.Authorization;
using System.Net;

namespace Dulwa.v1.Areas.Admin.Controllers
{
    [Area("Admin")]
    [Authorize(Roles = "Admin")]

    public class CustomPackageController : Controller
    {
        private readonly ApplicationDbContext _context;

        public CustomPackageController(ApplicationDbContext context)
        {
            _context = context;
        }

        // GET: Admin/CustomPackage
        public async Task<IActionResult> Index()
        {
            return View(await _context.CustomPackage.ToListAsync());
        }

        // GET: Admin/CustomPackage/Details/5
        public async Task<IActionResult> Details(Guid? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var customPackage = await _context.CustomPackage
                .SingleOrDefaultAsync(m => m.Id == id);
            if (customPackage == null)
            {
                return NotFound();
            }

            return View(customPackage);
        }

        // GET: Admin/CustomPackage/Create
        public IActionResult Create()
        {
            return View();
        }

        // POST: Admin/CustomPackage/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("Name,Description,ImageUrl,Id,Created_Date,Updated_Date")] CustomPackage customPackage)
        {
            if (ModelState.IsValid)
            {
                customPackage.Id = Guid.NewGuid();
                _context.Add(customPackage);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            return View(customPackage);
        }

        // GET: Admin/CustomPackage/Edit/5
        public async Task<IActionResult> Edit(Guid? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var customPackage = await _context.CustomPackage.SingleOrDefaultAsync(m => m.Id == id);
            if (customPackage == null)
            {
                return NotFound();
            }
            return View(customPackage);
        }

        // POST: Admin/CustomPackage/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(Guid id, [Bind("Name,Description,ImageUrl,Id,Created_Date,Updated_Date")] CustomPackage customPackage)
        {
            if (id != customPackage.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(customPackage);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!CustomPackageExists(customPackage.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            return View(customPackage);
        }

        // GET: Admin/CustomPackage/Delete/5
        [HttpPost]
        public async Task<JsonResult> Delete(Guid id)
        {
            try
            {
                var customPackage = await _context.CustomPackage.SingleOrDefaultAsync(m => m.Id == id);
                _context.CustomPackage.Remove(customPackage);
                await _context.SaveChangesAsync();
                return Json(HttpStatusCode.OK);
            }
            catch (Exception ex)
            {
                return Json(HttpStatusCode.InternalServerError);
            }

        }

        private bool CustomPackageExists(Guid id)
        {
            return _context.CustomPackage.Any(e => e.Id == id);
        }
    }
}
