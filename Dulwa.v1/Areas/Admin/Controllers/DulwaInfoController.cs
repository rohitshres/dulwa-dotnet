﻿using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Dulwa.v1.Service.DulwaInfo;
using Dulwa.v1.ViewModel;
using Dulwa.v1.Services;
using Microsoft.AspNetCore.Authorization;
using Dulwa.v1.Service.KeyCategories;
using Microsoft.AspNetCore.Mvc.Rendering;
using Dulwa.v1.Data;
using System;
using Microsoft.EntityFrameworkCore;
using System.Net;

namespace Dulwa.v1.Areas.Admin.Controllers
{
    [Area("Admin")]
    [Authorize(Roles = "Admin")]
    public class DulwaInfoController : Controller
    {
        private readonly ApplicationDbContext _context;
        private IDulwaInfoService _dulwaservice;
        private IFileUpload _file;
        private IKeyCategoryService _categoryservice;

        public DulwaInfoController(IDulwaInfoService dulwaservice, IFileUpload file,IKeyCategoryService categoryservice)
        {
            _dulwaservice = dulwaservice;
            _file = file;
            _categoryservice = categoryservice;
        }
        public async Task<IActionResult> Index()
        {            
            return View(await _dulwaservice.getDulwaInfo());
        }

        // GET: Admin/Company/Details/5
        public async Task<IActionResult> Details()
        {
            return View();
        }

        public async Task<IActionResult> Create()
        {
            ViewData["type"] = new SelectList( await _categoryservice.GetKeys("Type"),"Id","Name");
            ViewData["popular"] = new SelectList( await _categoryservice.GetKeys("Popular"),"Id","Name");
            ViewData["season"] = new SelectList( await _categoryservice.GetKeys("Season"),"Id","Name");
            return View();
        }
        [HttpPost]
        public async Task<IActionResult> Create(DulwaInfoViewModel Model)
        {
            if (ModelState.IsValid)
            {
              Model.Image=await _file.Upload(Model.File);               
                
              var destination= await _dulwaservice.createDulwaInfo(Model);
                return RedirectToAction("DulwainfoRouteCreate","Route",new { Id=destination.Id});
            }
            ViewData["type"] = new SelectList(await _categoryservice.GetKeys("Type"), "Id", "Name");
            ViewData["popular"] = new SelectList(await _categoryservice.GetKeys("Popular"), "Id", "Name");
            ViewData["season"] = new SelectList(await _categoryservice.GetKeys("Season"), "Id", "Name");
            return View(Model);
        }

        public async Task<IActionResult> Edit(Guid? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var dulwainfo = await _dulwaservice.GetSingleData(id);
            ViewData["type"] = new SelectList(await _categoryservice.GetKeys("Type"), "Id", "Name");
            ViewData["popular"] = new SelectList(await _categoryservice.GetKeys("Popular"), "Id", "Name");
            ViewData["season"] = new SelectList(await _categoryservice.GetKeys("Season"), "Id", "Name");
            if (dulwainfo == null)
            {
                return NotFound();
            }
            return View(dulwainfo);
        }

        // POST: Admin/Event/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]        
        public async Task<IActionResult> Edit(Guid id, DulwaInfoViewModel Model)
        {
            if (id != Model.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    await _dulwaservice.EditDulwaInfo(Model);
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (! await _dulwaservice.CheckData(Model.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }           
            return View(Model);
        }


        [HttpPost]
        public async Task<JsonResult> Delete(Guid id)
        {
            try
            {
                await _dulwaservice.DeleteDulwaInfo(id);
                return Json(HttpStatusCode.OK);
            }
            catch (Exception ex)
            {
                return Json(HttpStatusCode.InternalServerError);
            }

        }




    }
}