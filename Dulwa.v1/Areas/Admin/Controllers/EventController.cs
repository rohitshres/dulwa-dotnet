﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using Dulwa.v1.Data;
using Dulwa.v1.Entity;
using Dulwa.v1.Service.Events;
using Dulwa.v1.ViewModel;
using Dulwa.v1.Service.DulwaInfo;
using Microsoft.AspNetCore.Authorization;
using System.Net;

namespace Dulwa.v1.Areas.Admin.Controllers
{
    [Area("Admin")]
    [Authorize(Roles = "Admin")]

    public class EventController : Controller
    {
        private readonly IEventService _eventservice;
        private readonly IDulwaInfoService _dulwainfoservice;

        public EventController(IEventService eventservice, IDulwaInfoService DulwainfoService)
        {   
            _eventservice = eventservice;
            _dulwainfoservice = DulwainfoService;
        }

        // GET: Admin/Event
        public async Task<IActionResult> Index()
        {
            return View(await _eventservice.GetAllList());
        }

        // GET: Admin/Event/Details/5
        public async Task<IActionResult> Details(Guid? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var cevent = await _eventservice.GetSingleData(id);
            if (cevent == null)
            {
                return NotFound();
            }

            return View(cevent);
        }

        // GET: Admin/Event/Create
        public async Task<IActionResult> Create()
        {
            ViewData["DulwainfoId"] = new SelectList(await _dulwainfoservice.getDulwaInfo(), "Id", "Name");
            return View();
        }

        // POST: Admin/Event/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        public async Task<IActionResult> Create(EventViewModel events)
        {
            if (ModelState.IsValid)
            {
                events.Id = Guid.NewGuid();
                await _eventservice.Create(events);
                return RedirectToAction(nameof(Index));
            }
            ViewData["DulwainfoId"] = new SelectList(await _dulwainfoservice.getDulwaInfo(), "Id", "Name", events.DulwainfoId);
            return View(events);
        }

        // GET: Admin/Event/Edit/5
        public async Task<IActionResult> Edit(Guid? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var cevent = await _eventservice.GetSingleData(id);
            if (cevent == null)
            {
                return NotFound();
            }
            ViewData["DulwainfoId"] = new SelectList(await _dulwainfoservice.getDulwaInfo(), "Id", "Name", cevent.DulwainfoId);
            return View(cevent);
        }

        // POST: Admin/Event/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(Guid id, EventViewModel cevent)
        {
            if (id != cevent.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    await _eventservice.Update(cevent);
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!await _eventservice.CheckData(cevent.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            ViewData["DulwainfoId"] = new SelectList(await _dulwainfoservice.getDulwaInfo(), "Id", "Name", cevent.DulwainfoId);

            return View(cevent);
        }

        // GET: Admin/Event/Delete/5
       

        [HttpPost]
        public async Task<JsonResult> Delete(Guid id)
        {
            try
            {
                await _eventservice.Delete(id);
                return Json(HttpStatusCode.OK);
            }
            catch (Exception ex)
            {
                return Json(HttpStatusCode.InternalServerError);
            }

        }

    }
}
