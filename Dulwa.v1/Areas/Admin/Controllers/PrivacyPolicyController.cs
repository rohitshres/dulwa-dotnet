﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using Dulwa.v1.Data;
using Dulwa.v1.Entity;

namespace Dulwa.v1.Areas.Admin.Controllers
{
    [Area("Admin")]
    public class PrivacyPolicyController : Controller
    {
        private readonly ApplicationDbContext _context;

        public PrivacyPolicyController(ApplicationDbContext context)
        {
            _context = context;
        }

        // GET: Admin/PrivacyPolicy
        public async Task<IActionResult> Index()
        {
            return View(await _context.PrivacyPolicy.ToListAsync());
        }

        // GET: Admin/PrivacyPolicy/Details/5
        public async Task<IActionResult> Details(Guid? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var privacyPolicy = await _context.PrivacyPolicy
                .SingleOrDefaultAsync(m => m.Id == id);
            if (privacyPolicy == null)
            {
                return NotFound();
            }

            return View(privacyPolicy);
        }

        // GET: Admin/PrivacyPolicy/Create
        public IActionResult Create()
        {
            return View();
        }

        // POST: Admin/PrivacyPolicy/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("Detail,Id,Created_Date,Updated_Date,slug")] PrivacyPolicy privacyPolicy)
        {
            if (ModelState.IsValid)
            {
                privacyPolicy.Id = Guid.NewGuid();
                _context.Add(privacyPolicy);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            return View(privacyPolicy);
        }

        // GET: Admin/PrivacyPolicy/Edit/5
        public async Task<IActionResult> Edit(Guid? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var privacyPolicy = await _context.PrivacyPolicy.SingleOrDefaultAsync(m => m.Id == id);
            if (privacyPolicy == null)
            {
                return NotFound();
            }
            return View(privacyPolicy);
        }

        // POST: Admin/PrivacyPolicy/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(Guid id, [Bind("Detail,Id,Created_Date,Updated_Date,slug")] PrivacyPolicy privacyPolicy)
        {
            if (id != privacyPolicy.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(privacyPolicy);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!PrivacyPolicyExists(privacyPolicy.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            return View(privacyPolicy);
        }

        // GET: Admin/PrivacyPolicy/Delete/5
        public async Task<IActionResult> Delete(Guid? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var privacyPolicy = await _context.PrivacyPolicy
                .SingleOrDefaultAsync(m => m.Id == id);
            if (privacyPolicy == null)
            {
                return NotFound();
            }

            return View(privacyPolicy);
        }

        // POST: Admin/PrivacyPolicy/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(Guid id)
        {
            var privacyPolicy = await _context.PrivacyPolicy.SingleOrDefaultAsync(m => m.Id == id);
            _context.PrivacyPolicy.Remove(privacyPolicy);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool PrivacyPolicyExists(Guid id)
        {
            return _context.PrivacyPolicy.Any(e => e.Id == id);
        }
    }
}
