﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Authorization;

namespace Dulwa.v1.Areas.Admin.Controllers
{
    [Authorize(Roles = "Admin")]

    public class UserManageController : Controller
    {
        private RoleManager<IdentityUser> _rolemanage;

        public UserManageController(RoleManager<IdentityUser> rolemanager)
        {
            _rolemanage = rolemanager;
        }
        public IActionResult Index()
        {
            return View();
        }
    }
}