﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using Dulwa.v1.Data;
using Dulwa.v1.Entity;
using Dulwa.v1.ViewModel;
using Dulwa.v1.Service.Stores;
using Microsoft.AspNetCore.Authorization;
using System.Net;

namespace Dulwa.v1.Areas.Admin.Controllers
{
    [Authorize(Roles = "Admin")]

    [Area("Admin")]
    public class StoreCategoryController : Controller
    {
        private readonly ApplicationDbContext _context;
        private readonly IStoreService _storeservice;

        public StoreCategoryController(ApplicationDbContext context, IStoreService storeservice)
        {
            _context = context;
            _storeservice = storeservice;
        }

        // GET: Admin/StoreCategory
        public async Task<IActionResult> Index()
        {
            return View(await _context.StoreCategory.ToListAsync());
        }

        // GET: Admin/StoreCategory/Details/5
        public async Task<IActionResult> Details(Guid? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var storeCategory = await _context.StoreCategory
                .SingleOrDefaultAsync(m => m.Id == id);
            if (storeCategory == null)
            {
                return NotFound();
            }

            return View(storeCategory);
        }

        // GET: Admin/StoreCategory/Create
        public IActionResult Create()
        {
            return View();
        }

        // POST: Admin/StoreCategory/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create(StoreCategoryViewModel storeCategory)
        {
            if (ModelState.IsValid)
            {
                await _storeservice.CreateStoreCategory(storeCategory);
                return RedirectToAction(nameof(Index));
            }
            return View(storeCategory);
        }

        // GET: Admin/StoreCategory/Edit/5
        public async Task<IActionResult> Edit(Guid? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var storeCategory = await _context.StoreCategory.SingleOrDefaultAsync(m => m.Id == id);
            if (storeCategory == null)
            {
                return NotFound();
            }
            return View(storeCategory);
        }

        // POST: Admin/StoreCategory/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(Guid id, [Bind("Name,ImageUrl,Id,Created_Date,Updated_Date")] StoreCategory storeCategory)
        {
            if (id != storeCategory.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(storeCategory);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!StoreCategoryExists(storeCategory.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            return View(storeCategory);
        }

        // GET: Admin/StoreCategory/Delete/5       
        [HttpPost]
        public async Task<JsonResult> Delete(Guid id)
        {
            try
            {
                var storeCategory = await _context.StoreCategory.SingleOrDefaultAsync(m => m.Id == id);
                _context.StoreCategory.Remove(storeCategory);
                await _context.SaveChangesAsync();
                return Json(HttpStatusCode.OK);
            }
            catch (Exception ex)
            {
                return Json(HttpStatusCode.InternalServerError);
            }

        }

        private bool StoreCategoryExists(Guid id)
        {
            return _context.StoreCategory.Any(e => e.Id == id);
        }
    }
}
