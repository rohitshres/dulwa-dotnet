﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using Dulwa.v1.Data;
using Dulwa.v1.Entity;
using Microsoft.AspNetCore.Authorization;
using System.Net;

namespace Dulwa.v1.Areas.Admin.Controllers
{
    [Area("Admin")]
    [Authorize(Roles = "Admin")]

    public class KeyController : Controller
    {
        private readonly ApplicationDbContext _context;

        public KeyController(ApplicationDbContext context)
        {
            _context = context;
        }

        // GET: Admin/Key
        public async Task<IActionResult> Index()
        {
            var applicationDbContext = _context.Keys.Include(k => k.KeyCategory);
            return View(await applicationDbContext.ToListAsync());
        }

        // GET: Admin/Key/Details/5
        public async Task<IActionResult> Details(Guid? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var keys = await _context.Keys
                .Include(k => k.KeyCategory)
                .SingleOrDefaultAsync(m => m.Id == id);
            if (keys == null)
            {
                return NotFound();
            }

            return View(keys);
        }

        // GET: Admin/Key/Create
        public IActionResult Create()
        {
            ViewData["KeyCategoryId"] = new SelectList(_context.KeyCategory, "Id", "Name");
            return View();
        }

        // POST: Admin/Key/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create(Keys keys)
        {
            if (ModelState.IsValid)
            {
                keys.Id = Guid.NewGuid();
                _context.Add(keys);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            ViewData["KeyCategoryId"] = new SelectList(_context.KeyCategory, "Id", "Name", keys.KeyCategoryId);
            return View(keys);
        }

        // GET: Admin/Key/Edit/5
        public async Task<IActionResult> Edit(Guid? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var keys = await _context.Keys.SingleOrDefaultAsync(m => m.Id == id);
            if (keys == null)
            {
                return NotFound();
            }
            ViewData["KeyCategoryId"] = new SelectList(_context.KeyCategory, "Id", "Id", keys.KeyCategoryId);
            return View(keys);
        }

        // POST: Admin/Key/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(Guid id, [Bind("Name,KeyCategoryId,Id,Created_Date,Updated_Date")] Keys keys)
        {
            if (id != keys.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(keys);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!KeysExists(keys.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            ViewData["KeyCategoryId"] = new SelectList(_context.KeyCategory, "Id", "Id", keys.KeyCategoryId);
            return View(keys);
        }

        // GET: Admin/Key/Delete/5
        
        [HttpPost]
        public async Task<JsonResult> Delete(Guid id)
        {
            try
            {
                var keys = await _context.Keys.SingleOrDefaultAsync(m => m.Id == id);
                _context.Keys.Remove(keys);
                await _context.SaveChangesAsync();
                return Json(HttpStatusCode.OK);
            }
            catch (Exception ex)
            {
                return Json(HttpStatusCode.InternalServerError);
            }

        }

        private bool KeysExists(Guid id)
        {
            return _context.Keys.Any(e => e.Id == id);
        }
    }
}
