﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using Dulwa.v1.Data;
using Dulwa.v1.Entity;
using Microsoft.AspNetCore.Authorization;
using Dulwa.v1.ViewModel;
using Newtonsoft.Json;
using Dulwa.v1.Service.Routes;
using System.Net;

namespace Dulwa.v1.Areas.Admin.Controllers
{
    [Area("Admin")]
    [Authorize(Roles = "Admin")]
    public class RouteController : Controller
    {
        private readonly ApplicationDbContext _context;
        private readonly IRouteService _routeservice;

        public RouteController(ApplicationDbContext context, IRouteService routeservice)
        {
            _context = context;
            _routeservice = routeservice;
        }

        // GET: Admin/Route
        public async Task<IActionResult> Index()
        {
            var applicationDbContext = _context.Route.Include(r => r.Dulwainfo).Include(r => r.Package);
            return View(await applicationDbContext.ToListAsync());
        }

        // GET: Admin/Route/Details/5
        public async Task<IActionResult> Details(Guid? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var route = await _context.Route
                .Include(r => r.Dulwainfo)
                .Include(r => r.Package)
                .SingleOrDefaultAsync(m => m.Id == id);
            if (route == null)
            {
                return NotFound();
            }

            return View(route);
        }

        public IActionResult DulwainfoRouteCreate(Guid Id)
        {
            ViewData["DulwaInfoId"] = Id;
            return View();
        }
        [HttpPost]
        public async Task<IActionResult> DulwaRouteCreate(RouteViewModel Model)
        {
            if (ModelState.IsValid)
            {
                await _routeservice.Create(Model);
                return RedirectToAction("Index", "DulwaInfo");
            }
            return View(Model);
        }

        public async Task<IActionResult> PackageRouteCreate(Guid Id)
        {
            ViewData["PackageId"] = Id;

            return View();
        }
        [HttpPost]
        public async Task<IActionResult> PackageRouteCreate(RouteViewModel Model)
        {
            if (ModelState.IsValid)
            {
                Model.MapType = "route";
                await _routeservice.Create(Model);
                return RedirectToAction("Index", "Package");
            }
            return View(Model);
        }

        // GET: Admin/Route/Create
        public IActionResult Create()
        {
            ViewData["DulwaInfoId"] = new SelectList(_context.Dulwainfo, "Id", "Name");
            ViewData["PackageId"] = new SelectList(_context.Package, "Id", "Name");
            return View();
        }



        // POST: Admin/Route/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create(Route route)
        {
            if (ModelState.IsValid)
            {
                route.Id = Guid.NewGuid();
                _context.Add(route);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            ViewData["DulwaInfoId"] = new SelectList(_context.Dulwainfo, "Id", "Id", route.DulwaInfoId);
            ViewData["PackageId"] = new SelectList(_context.Package, "Id", "Id", route.PackageId);
            return View(route);
        }

        // GET: Admin/Route/Edit/5
        public async Task<IActionResult> Edit(Guid? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var route = await _context.Route.SingleOrDefaultAsync(m => m.Id == id);
            if (route == null)
            {
                return NotFound();
            }
            ViewData["DulwaInfoId"] = new SelectList(_context.Dulwainfo, "Id", "Id", route.DulwaInfoId);
            ViewData["PackageId"] = new SelectList(_context.Package, "Id", "Id", route.PackageId);
            return View(route);
        }

        // POST: Admin/Route/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(Guid id, [Bind("lat,lon,MapType,DulwaInfoId,PackageId,Id,Created_Date,Updated_Date")] Route route)
        {
            if (id != route.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(route);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!RouteExists(route.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            ViewData["DulwaInfoId"] = new SelectList(_context.Dulwainfo, "Id", "Id", route.DulwaInfoId);
            ViewData["PackageId"] = new SelectList(_context.Package, "Id", "Id", route.PackageId);
            return View(route);
        }

        // GET: Admin/Route/Delete/5  
        [HttpPost]
        public async Task<JsonResult> Delete(Guid id)
        {
            try
            {
                var route = await _context.Route.SingleOrDefaultAsync(m => m.Id == id);
                _context.Route.Remove(route);
                await _context.SaveChangesAsync();
                return Json(HttpStatusCode.OK);
            }
            catch (Exception ex)
            {
                return Json(HttpStatusCode.InternalServerError);
            }

        }

        private bool RouteExists(Guid id)
        {
            return _context.Route.Any(e => e.Id == id);
        }
    }
}
