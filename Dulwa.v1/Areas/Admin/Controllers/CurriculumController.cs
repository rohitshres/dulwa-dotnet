﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using Dulwa.v1.Data;
using Dulwa.v1.Entity;
using Dulwa.v1.Service.Curriculums;
using Dulwa.v1.ViewModel;
using Dulwa.v1.Services;
using Dulwa.v1.Service.DulwaInfo;
using Microsoft.AspNetCore.Authorization;
using System.Net;

namespace Dulwa.v1.Areas.Admin.Controllers
{
    [Area("Admin")]
    [Authorize(Roles = "Admin")]

    public class CurriculumController : Controller
    {    
        private readonly ICurriculumService _curriculumService;
        private readonly IDulwaInfoService _dulwainfoservice;

        public CurriculumController(ICurriculumService curriculumService, IDulwaInfoService dulwainfo)
        {         
            _curriculumService = curriculumService;
            _dulwainfoservice = dulwainfo;
        }

        // GET: Admin/Curriculum
        public async Task<IActionResult> Index()
        {
            return View(await _curriculumService.GetAllList());
        }

        // GET: Admin/Curriculum/Details/5
        public async Task<IActionResult> Details(Guid? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var curriculum = await _curriculumService.GetSingleData(id);
            if (curriculum == null)
            {
                return NotFound();
            }

            return View(curriculum);
        }

        // GET: Admin/Curriculum/Create
        public async Task<IActionResult> Create()
        {
            ViewData["DulwainfoId"] = new SelectList(await _dulwainfoservice.getDulwaInfo(), "Id", "Name");
            return View();
        }

        // POST: Admin/Curriculum/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create(CurriculumViewModel curriculum)
        {
            if (ModelState.IsValid)
            {                
                await _curriculumService.Create(curriculum);
                return RedirectToAction(nameof(Index));
            }
            return View(curriculum);
        }

        // GET: Admin/Curriculum/Edit/5
        public async Task<IActionResult> Edit(Guid? id)
        {
            if (id == null)
            {
                return NotFound();
            }
            var curriculum = await _curriculumService.GetSingleData(id);
            ViewData["DulwainfoId"] = new SelectList(await _dulwainfoservice.getDulwaInfo(), "Id", "Name",curriculum.DulwainfoId);

            if (curriculum == null)
            {
                return NotFound();
            }
            return View(curriculum);
        }

        // POST: Admin/Curriculum/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost] 
        public async Task<IActionResult> Edit(Guid id, CurriculumViewModel curriculum)
        {
            if (id != curriculum.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    await _curriculumService.Update(curriculum);
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!await _curriculumService.CheckData(curriculum.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            return View(curriculum);
        }


        // POST: Admin/Curriculum/Delete/5
       
        [HttpPost]
        public async Task<JsonResult> Delete(Guid id)
        {
            try
            {
                await _curriculumService.Delete(id);

                return Json(HttpStatusCode.OK);
            }
            catch (Exception ex)
            {
                return Json(HttpStatusCode.InternalServerError);
            }

        }

    }
}
