﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using Dulwa.v1.Data;
using Dulwa.v1.Entity;
using Dulwa.v1.Service.Stores;
using Dulwa.v1.ViewModel;
using Newtonsoft.Json;
using Microsoft.AspNetCore.Authorization;
using System.Net;

namespace Dulwa.v1.Areas.Admin.Controllers
{
    [Area("Admin")]
    [Authorize(Roles = "Admin")]

    public class StoreController : Controller
    {
        private readonly ApplicationDbContext _context;
        private readonly IStoreService _storeservice;

        public StoreController(ApplicationDbContext context, IStoreService StoreService)
        {
            _context = context;
            _storeservice = StoreService;
        }

        // GET: Admin/Store
        public async Task<IActionResult> Index()
        {
            var applicationDbContext = _context.Store.Include(s => s.StoreCategory);
            return View(await applicationDbContext.ToListAsync());
        }

        // GET: Admin/Store/Details/5
        public async Task<IActionResult> Details(Guid? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var store = await _context.Store
                .Include(s => s.StoreCategory)
                .SingleOrDefaultAsync(m => m.Id == id);
            if (store == null)
            {
                return NotFound();
            }

            return View(store);
        }

        // GET: Admin/Store/Create
        public IActionResult Create()
        {
            ViewData["StoreCategoryId"] = new SelectList(_context.StoreCategory, "Id", "Name");
            return View();
        }

        // POST: Admin/Store/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create(StoreViewModel store)
        {
            if (ModelState.IsValid)
            {

                var stores=await _storeservice.Create(store);
                return RedirectToAction(nameof(StoreDetail),new { Id= stores.Id });
            }
            ViewData["StoreCategoryId"] = new SelectList(_context.StoreCategory, "Id", "Name", store.StoreCategoryId);
            return View(store);
        }


        public async Task<IActionResult> StoreDetail(string id)
        {
            ViewBag.id = id;
            return View();
        }
        [HttpPost]
        public async Task<IActionResult> StoreDetail(StoreDetailViewModel model)
        {
          await _storeservice.CreateStoreDetail(model);
            return RedirectToAction(nameof(Index));
        }
        // GET: Admin/Store/Edit/5
        public async Task<IActionResult> Edit(Guid? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var store = await _context.Store.SingleOrDefaultAsync(m => m.Id == id);
            if (store == null)
            {
                return NotFound();
            }
            ViewData["StoreCategoryId"] = new SelectList(_context.StoreCategory, "Id", "Name", store.StoreCategoryId);
            return View(store);
        }

        // POST: Admin/Store/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(Guid id, [Bind("Name,Description,ImageUrl,price,StoreCategoryId,Id,Created_Date,Updated_Date")] Store store)
        {
            if (id != store.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(store);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!StoreExists(store.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            ViewData["StoreCategoryId"] = new SelectList(_context.StoreCategory, "Id", "Name", store.StoreCategoryId);
            return View(store);
        }

        // GET: Admin/Store/Delete/5
        
        [HttpPost]
        public async Task<JsonResult> Delete(Guid id)
        {
            try
            {
                var store = await _context.Store.SingleOrDefaultAsync(m => m.Id == id);
                _context.Store.Remove(store);
                await _context.SaveChangesAsync();
                return Json(HttpStatusCode.OK);
            }
            catch (Exception ex)
            {
                return Json(HttpStatusCode.InternalServerError);
            }

        }
        private bool StoreExists(Guid id)
        {
            return _context.Store.Any(e => e.Id == id);
        }
    }
}
