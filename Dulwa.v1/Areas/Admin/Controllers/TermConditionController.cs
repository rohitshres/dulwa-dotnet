﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using Dulwa.v1.Data;
using Dulwa.v1.Entity;

namespace Dulwa.v1.Areas.Admin.Controllers
{
    [Area("Admin")]
    public class TermConditionController : Controller
    {
        private readonly ApplicationDbContext _context;

        public TermConditionController(ApplicationDbContext context)
        {
            _context = context;
        }

        // GET: Admin/TermCondition
        public async Task<IActionResult> Index()
        {
            return View(await _context.TermsCondition.ToListAsync());
        }

        // GET: Admin/TermCondition/Details/5
        public async Task<IActionResult> Details(Guid? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var termsCondition = await _context.TermsCondition
                .SingleOrDefaultAsync(m => m.Id == id);
            if (termsCondition == null)
            {
                return NotFound();
            }

            return View(termsCondition);
        }

        // GET: Admin/TermCondition/Create
        public IActionResult Create()
        {
            return View();
        }

        // POST: Admin/TermCondition/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("Detail,Id,Created_Date,Updated_Date,slug")] TermsCondition termsCondition)
        {
            if (ModelState.IsValid)
            {
                termsCondition.Id = Guid.NewGuid();
                _context.Add(termsCondition);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            return View(termsCondition);
        }

        // GET: Admin/TermCondition/Edit/5
        public async Task<IActionResult> Edit(Guid? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var termsCondition = await _context.TermsCondition.SingleOrDefaultAsync(m => m.Id == id);
            if (termsCondition == null)
            {
                return NotFound();
            }
            return View(termsCondition);
        }

        // POST: Admin/TermCondition/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(Guid id, [Bind("Detail,Id,Created_Date,Updated_Date,slug")] TermsCondition termsCondition)
        {
            if (id != termsCondition.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(termsCondition);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!TermsConditionExists(termsCondition.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            return View(termsCondition);
        }

        // GET: Admin/TermCondition/Delete/5
        public async Task<IActionResult> Delete(Guid? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var termsCondition = await _context.TermsCondition
                .SingleOrDefaultAsync(m => m.Id == id);
            if (termsCondition == null)
            {
                return NotFound();
            }

            return View(termsCondition);
        }

        // POST: Admin/TermCondition/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(Guid id)
        {
            var termsCondition = await _context.TermsCondition.SingleOrDefaultAsync(m => m.Id == id);
            _context.TermsCondition.Remove(termsCondition);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool TermsConditionExists(Guid id)
        {
            return _context.TermsCondition.Any(e => e.Id == id);
        }
    }
}
