﻿using Microsoft.AspNetCore.Http;
using System.ComponentModel.DataAnnotations;

namespace Dulwa.v1.Areas.ViewModel
{
    public class AdvertisementViewModel
    {
        [Required]
        [Display(Name ="Image")]
        public string imageUrl { get; set; }
        [Required]
        [Display(Name = "Name")]
        public string name { get; set; }
        [Required]
        [Display(Name = "Redirect URL")]
        public string redirectUrl { get; set; }
        [Display(Name = "File")]
        public IFormFile File { get; set; }
    }
}
