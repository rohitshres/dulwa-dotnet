﻿using Microsoft.AspNetCore.Http;
using System.ComponentModel.DataAnnotations;

namespace Dulwa.v1.Areas.ViewModel
{
    public class CompanyVM
    {
        [Required]
        [Display(Name ="Name")]
        public string name { get; set; }
        [Display(Name = "Address")]
        public string address { get; set; }
        [Display(Name = "Contact")]
        public string contact { get; set; }
        [Display(Name = "Image")]
        public string image { get; set; }
        [Display(Name = "PAN Number")]
        public string pan_vat_number { get; set; }
        [Display(Name = "Contact Person")]
        public string ContactPersonName { get; set; }
        [Display(Name = "Contact Person Designation")]
        public string ContactPersonDesignation { get; set; }
        [Display(Name = "Facebook Link")]
        public string fb_Link { get; set; }
        [Display(Name = "Google Link")]
        public string google_Link { get; set; }
        [Display(Name = "Pinterest Link")]
        public string pinterest_Link { get; set; }
        [Display(Name = "Twitter Link")]
        public string twitter_Link { get; set; }
        [Display(Name = "Instagram Link")]
        public string instragram_Link { get; set; }
        [Display(Name = "Website Link")]
        public string website_Link { get; set; }
        [Display(Name = "Image")]
        public IFormFile imagefile { get; set; }
    }
}
